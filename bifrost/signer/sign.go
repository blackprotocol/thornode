package signer

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tssp "gitlab.com/blackprotocol/tss/go-tss/tss"

	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients"
	"gitlab.com/blackprotocol/blacknode/bifrost/pubkeymanager"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/bifrost/tss"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/constants"
	ttypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// Signer will pull the tx out from thorchain and then forward it to chain
type Signer struct {
	logger                zerolog.Logger
	cfg                   config.BifrostSignerConfiguration
	wg                    *sync.WaitGroup
	thorchainBridge       *thorclient.Bridge
	stopChan              chan struct{}
	blockScanner          *blockscanner.BlockScanner
	thorchainBlockScanner *ThorchainBlockScan
	chains                map[common.Chain]chainclients.ChainClient
	storage               SignerStorage
	m                     *metrics.Metrics
	errCounter            *prometheus.CounterVec
	tssKeygen             *tss.KeyGen
	pubkeyMgr             pubkeymanager.PubKeyValidator
	constantsProvider     *ConstantsProvider
	localPubKey           common.PubKey
	tssKeysignMetricMgr   *metrics.TssKeysignMetricMgr
}

// NewSigner create a new instance of signer
func NewSigner(cfg config.BifrostSignerConfiguration,
	thorchainBridge *thorclient.Bridge,
	thorKeys *thorclient.Keys,
	pubkeyMgr pubkeymanager.PubKeyValidator,
	tssServer *tssp.TssServer,
	chains map[common.Chain]chainclients.ChainClient,
	m *metrics.Metrics,
	tssKeysignMetricMgr *metrics.TssKeysignMetricMgr,
) (*Signer, error) {
	storage, err := NewSignerStore(cfg.SignerDbPath, thorchainBridge.GetConfig().SignerPasswd)
	if err != nil {
		return nil, fmt.Errorf("fail to create thorchain scan storage: %w", err)
	}
	if tssKeysignMetricMgr == nil {
		return nil, fmt.Errorf("fail to create signer , tss keysign metric manager is nil")
	}
	addr, err := thorKeys.GetSignerInfo().GetAddress()
	if err != nil {
		return nil, fmt.Errorf("fail to get address: %w", err)
	}
	var na *ttypes.NodeAccount
	for i := 0; i < 300; i++ { // wait for 5 min before timing out
		var err error
		na, err = thorchainBridge.GetNodeAccount(addr.String())
		if err != nil {
			return nil, fmt.Errorf("fail to get node account from thorchain,err:%w", err)
		}

		if !na.PubKeySet.Secp256k1.IsEmpty() {
			break
		}
		time.Sleep(constants.BlackChainBlockTime)
		fmt.Println("Waiting for node account to be registered...")
	}
	for _, item := range na.GetSignerMembership() {
		pubkeyMgr.AddPubKey(item, true)
	}
	if na.PubKeySet.Secp256k1.IsEmpty() {
		return nil, fmt.Errorf("unable to find pubkey for this node account. exiting... ")
	}
	pubkeyMgr.AddNodePubKey(na.PubKeySet.Secp256k1)

	cfg.BlockScanner.ChainID = common.BLKChain // hard code to blackchain

	// Create pubkey manager and add our private key (Yggdrasil pubkey)
	thorchainBlockScanner, err := NewThorchainBlockScan(cfg.BlockScanner, storage, thorchainBridge, m, pubkeyMgr)
	if err != nil {
		return nil, fmt.Errorf("fail to create thorchain block scan: %w", err)
	}

	blockScanner, err := blockscanner.NewBlockScanner(cfg.BlockScanner, storage, m, thorchainBridge, thorchainBlockScanner)
	if err != nil {
		return nil, fmt.Errorf("fail to create block scanner: %w", err)
	}

	kg, err := tss.NewTssKeyGen(thorKeys, tssServer, thorchainBridge)
	if err != nil {
		return nil, fmt.Errorf("fail to create Tss Key gen,err:%w", err)
	}
	constantProvider := NewConstantsProvider(thorchainBridge)
	return &Signer{
		logger:                log.With().Str("module", "signer").Logger(),
		cfg:                   cfg,
		wg:                    &sync.WaitGroup{},
		stopChan:              make(chan struct{}),
		blockScanner:          blockScanner,
		thorchainBlockScanner: thorchainBlockScanner,
		chains:                chains,
		m:                     m,
		storage:               storage,
		errCounter:            m.GetCounterVec(metrics.SignerError),
		pubkeyMgr:             pubkeyMgr,
		thorchainBridge:       thorchainBridge,
		tssKeygen:             kg,
		constantsProvider:     constantProvider,
		localPubKey:           na.PubKeySet.Secp256k1,
		tssKeysignMetricMgr:   tssKeysignMetricMgr,
	}, nil
}

func (s *Signer) getChain(chainID common.Chain) (chainclients.ChainClient, error) {
	chain, ok := s.chains[chainID]
	if !ok {
		s.logger.Debug().Str("chain", chainID.String()).Msg("is not supported yet")
		return nil, errors.New("not supported")
	}
	return chain, nil
}

// Start signer process
func (s *Signer) Start() error {
	s.wg.Add(1)
	go s.processTxnOut(s.thorchainBlockScanner.GetTxOutMessages(), 1)

	s.wg.Add(1)
	go s.processKeygen(s.thorchainBlockScanner.GetKeygenMessages())

	s.wg.Add(1)
	go s.signTransactions()

	s.wg.Add(1)
	go s.processClaims(s.thorchainBlockScanner.GetBlockClaims(), 1)

	s.blockScanner.Start(nil)
	return nil
}

func (s *Signer) shouldSign(tx types.TxOutItem) bool {
	return s.pubkeyMgr.HasPubKey(tx.VaultPubKey)
}

// signTransactions - looks for work to do by getting a list of all unsigned
// transactions stored in the storage
func (s *Signer) signTransactions() {
	s.logger.Info().Msg("start to sign transactions")
	defer s.logger.Info().Msg("stop to sign transactions")
	defer s.wg.Done()
	for {
		select {
		case <-s.stopChan:
			return
		default:
			// When blacknode is catching up , bifrost might get stale data from blacknode , thus it shall pause signing
			catchingUp, err := s.thorchainBridge.IsCatchingUp()
			if err != nil {
				s.logger.Err(err).Msg("fail to get thorchain sync status")
				time.Sleep(constants.BlackChainBlockTime)
				break // this will break select
			}
			if !catchingUp {
				s.processTransactions()
			}
			time.Sleep(1 * time.Second)
		}
	}
}

func (s *Signer) runWithContext(ctx context.Context, fn func() error) error {
	ch := make(chan error, 1)
	go func() {
		ch <- fn()
	}()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-ch:
		return err
	}
}

func (s *Signer) processClaims(ch <-chan *ttypes.BlockClaims, idx int) {
	s.logger.Info().Msgf("start to process claims,index: %d", idx)
	defer s.logger.Info().Msgf("stop processing claims,index: %d", idx)
	// signer doesn't store the claim , leave to each individual chain client to store it locally
	defer s.wg.Done()
	for {
		select {
		case <-s.stopChan:
			return
		case bc, more := <-ch:
			if !more {
				return
			}
			if bc == nil {
				continue
			}
			if len(bc.Claims) == 0 {
				continue
			}
			for _, claimItem := range bc.Claims {
				chainClient, err := s.getChain(claimItem.Chain)
				if err != nil {
					s.logger.Err(err).Msgf("fail to get chain client for chain: %s", claimItem.Chain)
					continue
				}
				// retry it for 5 times, if it failed consistently for 5 times , then give up and move on
				for i := 0; i < 5; i++ {
					if err := chainClient.ValidateClaim(claimItem); err != nil {
						s.logger.Err(err).Msgf("failed to process claim item, %+q", claimItem)
						// backoff for 1 second, and then retry it
						time.Sleep(time.Second)
					} else {
						break
					}
				}
			}
		}
	}
}

func (s *Signer) processTransactions() {
	wg := &sync.WaitGroup{}
	for _, items := range s.storage.OrderedLists() {
		wg.Add(1)

		go func(items []TxOutStoreItem) {
			defer wg.Done()
			for i, item := range items {
				select {
				case <-s.stopChan:
					return
				default:
					if item.Status == TxSpent { // don't rebroadcast spent transactions
						continue
					}

					s.logger.Info().Int("num", i).Int64("height", item.Height).Int("status", int(item.Status)).Interface("tx", item.TxOutItem).Msgf("Signing transaction")
					// a single keysign should not take longer than 5 minutes , regardless TSS or local
					ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
					if err := s.runWithContext(ctx, func() error {
						return s.signAndBroadcast(item)
					}); err != nil {
						if errors.Is(err, context.DeadlineExceeded) {
							panic(fmt.Errorf("tx out item: %+v , keysign timeout : %w", item.TxOutItem, err))
						}
						s.logger.Err(err).Msg("fail to sign and broadcast tx out store item")
						if err := s.storage.Set(item); err != nil {
							s.logger.Err(err).Msg("fail to update tx out store item with retry #")
						}
						cancel()
						return
						// The 'item' for loop should not be items[0],
						// because problems which return 'nil, nil' should be skipped over instead of blocking others.
						// When signAndBroadcast returns an error (such as from a keysign timeout),
						// a 'return' and not a 'continue' should be used so that nodes can all restart the list,
						// for when the keysign failure was from a loss of list synchrony.
						// Otherwise, out-of-sync lists would cycle one timeout at a time, maybe never resynchronising.
					}
					cancel()

					// We have a successful broadcast! Remove the item from our store
					if err := s.storage.Remove(item); err != nil {
						s.logger.Err(err).Msg("fail to update tx out store item")
					}
				}
			}
		}(items)
	}
	wg.Wait()
}

// processTxnOut processes outbound TxOuts and save them to storage
func (s *Signer) processTxnOut(ch <-chan types.TxOut, idx int) {
	s.logger.Info().Int("idx", idx).Msg("start to process tx out")
	defer s.logger.Info().Int("idx", idx).Msg("stop to process tx out")
	defer s.wg.Done()
	for {
		select {
		case <-s.stopChan:
			return
		case txOut, more := <-ch:
			if !more {
				return
			}
			s.logger.Info().Msgf("Received a TxOut Array of %v from the Thorchain", txOut)
			items := make([]TxOutStoreItem, 0, len(txOut.TxArray))

			for i, tx := range txOut.TxArray {
				items = append(items, NewTxOutStoreItem(txOut.Height, tx.TxOutItem(), int64(i)))
			}
			if err := s.storage.Batch(items); err != nil {
				s.logger.Err(err).Msg("fail to save tx out items to storage")
			}
		}
	}
}

func (s *Signer) processKeygen(ch <-chan ttypes.KeygenBlock) {
	s.logger.Info().Msg("start to process keygen")
	defer s.logger.Info().Msg("stop to process keygen")
	defer s.wg.Done()
	for {
		select {
		case <-s.stopChan:
			return
		case keygenBlock, more := <-ch:
			if !more {
				return
			}
			s.logger.Info().Msgf("Received a keygen block %+v from the Thorchain", keygenBlock)

			// a node can only belong to two vaults maximum , one active asgard , and one retiring vault
			// so in terms of keygen , it should only have one keygen request in keygenBlock
			for _, keygenReq := range keygenBlock.Keygens {
				// Add pubkeys to pubkey manager for monitoring...
				// each member might become a yggdrasil pool
				for _, pk := range keygenReq.GetMembers() {
					s.pubkeyMgr.AddPubKey(pk, false)
				}
				keygenStart := time.Now()
				pubKey, blame, err := s.tssKeygen.GenerateNewKey(keygenBlock.Height, keygenReq.GetMembers())
				if !blame.IsEmpty() {
					err := fmt.Errorf("reason: %s, nodes %+v", blame.FailReason, blame.BlameNodes)
					s.logger.Err(err).Msg("Blame")
				}
				keygenTime := time.Since(keygenStart).Milliseconds()
				if err != nil {
					s.errCounter.WithLabelValues("fail_to_keygen_pubkey", "").Inc()
					s.logger.Err(err).Msg("fail to generate new pubkey")
				}
				var chainAddresses []ttypes.ChainAddress
				if !pubKey.Secp256k1.IsEmpty() {
					// only trigger other chain client to generate new address when the main tss keygen successfully.
					s.pubkeyMgr.AddPubKey(pubKey.Secp256k1, true)
					for chainID, chainClient := range s.chains {
						if !chainClient.CustomKeygen() {
							continue
						}
						currentAsgardAddress, err := s.getAsgardWalletAddress(chainID)
						if err != nil {
							s.logger.Err(err).Msg("fail to get current asgard wallet address")
						}
						chainAddr, chainBlame, err := chainClient.Keygen(keygenReq.Members, keygenBlock.Height, "0.1.0", currentAsgardAddress.String())
						if err != nil {
							s.logger.Err(err).Str("chain", chainID.String()).Msg("fail to keygen")
							blame = thorclient.GetThorchainBlame(chainBlame)
							break
						}
						chainAddresses = append(chainAddresses, chainAddr)
					}
				}

				if err := s.sendKeygenToThorchain(keygenBlock.Height, pubKey.Secp256k1, blame, keygenReq.GetMembers(), keygenReq.Type, keygenTime, chainAddresses); err != nil {
					s.errCounter.WithLabelValues("fail_to_broadcast_keygen", "").Inc()
					s.logger.Err(err).Msg("fail to broadcast keygen")
				}

			}
		}
	}
}

func (s *Signer) sendKeygenToThorchain(height int64, poolPk common.PubKey, blame ttypes.Blame, input common.PubKeys, keygenType ttypes.KeygenType, keygenTime int64, chainAddresses []ttypes.ChainAddress) error {
	// collect supported chains in the configuration
	chains := common.Chains{
		common.BLKChain,
	}
	for name, chain := range s.chains {
		if !chain.GetConfig().OptToRetire {
			chains = append(chains, name)
		}
	}

	keygenMsg, err := s.thorchainBridge.GetKeygenStdTx(poolPk, blame, input, keygenType, chains, height, keygenTime, chainAddresses)
	if err != nil {
		return fmt.Errorf("fail to get keygen id: %w", err)
	}
	strHeight := strconv.FormatInt(height, 10)

	txID, err := s.thorchainBridge.Broadcast(keygenMsg)
	if err != nil {
		s.errCounter.WithLabelValues("fail_to_send_to_thorchain", strHeight).Inc()
		return fmt.Errorf("fail to send the tx to thorchain: %w", err)
	}
	s.logger.Info().Int64("block", height).Str("thorchain hash", txID.String()).Msg("sign and send to thorchain successfully")
	return nil
}

// signAndBroadcast retry a few times before blacknode move on to the next block
func (s *Signer) signAndBroadcast(item TxOutStoreItem) error {
	height := item.Height
	tx := item.TxOutItem
	blockHeight, err := s.thorchainBridge.GetBlockHeight()
	if err != nil {
		s.logger.Err(err).Msgf("fail to get block height")
		return err
	}
	signingTransactionPeriod, err := s.constantsProvider.GetInt64Value(blockHeight, constants.SigningTransactionPeriod)
	s.logger.Debug().Msgf("signing transaction period:%d", signingTransactionPeriod)
	if err != nil {
		s.logger.Err(err).Msgf("fail to get constant value for(%s)", constants.SigningTransactionPeriod)
		return err
	}
	// rounds up to nearth 100th, then minuses signingTxPeriod. This is in an
	// effort for multi-bifrost nodes to get deterministic consensus on which
	// transaction to sign next. If we didn't round up, which transaction to
	// sign would change every 5 seconds. And with 20 sec party timeouts, luck
	// of execution time will determine if consensus is reached. Instead, we
	// have the same transaction selected for a longer period of time, making
	// it easier for the nodes to all select the same transaction, even if they
	// don't execute at the same time.
	if ((blockHeight/100*100)+100)-(signingTransactionPeriod) > height {
		s.logger.Error().Msgf("tx was created at block height(%d), now it is (%d), it is older than (%d) blocks , skip it ", height, blockHeight, signingTransactionPeriod)
		return nil
	}
	chain, err := s.getChain(tx.Chain)
	if err != nil {
		s.logger.Err(err).Msgf("not supported %s", tx.Chain.String())
		return err
	}
	mimirKey := "HALTSIGNING"
	haltSigningGlobalMimir, err := s.thorchainBridge.GetMimir(mimirKey)
	if err != nil {
		s.logger.Err(err).Msgf("fail to get %s", mimirKey)
		return err
	}
	if haltSigningGlobalMimir > 0 {
		s.logger.Info().Msg("signing has been halted globally")
		return nil
	}
	mimirKey = fmt.Sprintf("HALTSIGNING%s", tx.Chain)
	haltSigningMimir, err := s.thorchainBridge.GetMimir(mimirKey)
	if err != nil {
		s.logger.Err(err).Msgf("fail to get %s", mimirKey)
		return err
	}
	if haltSigningMimir > 0 {
		s.logger.Info().Msgf("signing for %s is halted", tx.Chain)
		return nil
	}
	if !s.shouldSign(tx) {
		s.logger.Info().Str("signer_address", chain.GetAddress(tx.VaultPubKey)).Msg("different pool address, ignore")
		return nil
	}

	if len(tx.ToAddress) == 0 {
		s.logger.Info().Msg("To address is empty, blacknode don't know where to send the fund , ignore")
		return nil // return nil and discard item
	}

	// don't sign if the block scanner is unhealthy. This is because the
	// network may not be able to detect the outbound transaction, and
	// therefore reschedule the transaction to another signer. In a disaster
	// scenario, the network could broadcast a transaction several times,
	// bleeding funds.
	if !chain.IsBlockScannerHealthy() {
		return fmt.Errorf("the block scanner for chain %s is unhealthy, not signing transactions due to it", chain.GetChain())
	}

	if tx.Coins.IsEmpty() {
		return nil
	}

	start := time.Now()
	defer func() {
		s.m.GetHistograms(metrics.SignAndBroadcastDuration(chain.GetChain())).Observe(time.Since(start).Seconds())
	}()

	if !tx.OutHash.IsEmpty() {
		s.logger.Info().Str("OutHash", tx.OutHash.String()).Msg("tx had been sent out before")
		return nil // return nil and discard item
	}

	// We get the keysign object from thorchain again to ensure it hasn't
	// been signed already, and we can skip. This helps us not get stuck on
	// a task that we'll never sign, because 2/3rds already has and will
	// never be available to sign again.
	txOut, err := s.thorchainBridge.GetKeysign(height, tx.VaultPubKey.String())
	if err != nil {
		s.logger.Err(err).Msg("fail to get keysign items")
		return err
	}
	for _, txArray := range txOut.TxArray {
		if txArray.TxOutItem().Equals(tx) && !txArray.OutHash.IsEmpty() {
			// already been signed, we can skip it
			s.logger.Info().Str("tx_id", tx.OutHash.String()).Msgf("already signed. skipping...")
			return nil
		}
	}
	startKeySign := time.Now()
	signedTx, err := chain.SignTx(tx, height)
	if err != nil {
		s.logger.Err(err).Msg("fail to sign tx")
		return err
	}
	elapse := time.Since(startKeySign)

	// looks like the transaction is already signed
	if len(signedTx) == 0 {
		s.logger.Warn().Msgf("signed transaction is empty")
		return nil
	}
	hash, err := chain.BroadcastTx(tx, signedTx)
	if err != nil {
		s.logger.Err(err).Msg("fail to broadcast tx to chain")
		return err
	}
	if s.isTssKeysign(tx.VaultPubKey) {
		s.tssKeysignMetricMgr.SetTssKeysignMetric(hash, elapse.Milliseconds())
	}

	return nil
}

func (s *Signer) isTssKeysign(pubKey common.PubKey) bool {
	return !s.localPubKey.Equals(pubKey)
}

// getAsgardWalletAddress
func (s *Signer) getAsgardWalletAddress(chain common.Chain) (common.Address, error) {
	asgardVaults, err := s.thorchainBridge.GetAsgards()
	if err != nil {
		return common.NoAddress, fmt.Errorf("fail to get current asgard vaults,err: %w", err)
	}
	for _, vault := range asgardVaults {
		if vault.Status != ttypes.VaultStatus_ActiveVault {
			continue
		}
		if !vault.Contains(s.localPubKey) {
			continue
		}
		for _, item := range vault.Addresses {
			if !item.Chain.Equals(chain) {
				continue
			}
			return item.Address, nil
		}
	}
	return common.NoAddress, nil
}

// Stop the signer process
func (s *Signer) Stop() error {
	s.logger.Info().Msg("receive request to stop signer")
	defer s.logger.Info().Msg("signer stopped successfully")
	close(s.stopChan)
	s.wg.Wait()
	if err := s.m.Stop(); err != nil {
		s.logger.Err(err).Msg("fail to stop metric server")
	}
	s.blockScanner.Stop()
	return s.storage.Close()
}
