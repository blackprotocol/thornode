package signer

import (
	"errors"
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	btypes "gitlab.com/blackprotocol/blacknode/bifrost/blockscanner/types"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/pubkeymanager"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/config"
	ttypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

type ThorchainBlockScan struct {
	logger         zerolog.Logger
	wg             *sync.WaitGroup
	stopChan       chan struct{}
	txOutChan      chan types.TxOut
	keygenChan     chan ttypes.KeygenBlock
	claimChan      chan *ttypes.BlockClaims
	cfg            config.BifrostBlockScannerConfiguration
	scannerStorage blockscanner.ScannerStorage
	thorchain      *thorclient.Bridge
	errCounter     *prometheus.CounterVec
	pubkeyMgr      pubkeymanager.PubKeyValidator
}

// NewThorchainBlockScan create a new instance of thorchain block scanner
func NewThorchainBlockScan(cfg config.BifrostBlockScannerConfiguration, scanStorage blockscanner.ScannerStorage, thorchain *thorclient.Bridge, m *metrics.Metrics, pubkeyMgr pubkeymanager.PubKeyValidator) (*ThorchainBlockScan, error) {
	if scanStorage == nil {
		return nil, errors.New("scanStorage is nil")
	}
	if m == nil {
		return nil, errors.New("metric is nil")
	}
	return &ThorchainBlockScan{
		logger:         log.With().Str("module", "blockscanner").Str("chain", "THOR").Logger(),
		wg:             &sync.WaitGroup{},
		stopChan:       make(chan struct{}),
		txOutChan:      make(chan types.TxOut),
		keygenChan:     make(chan ttypes.KeygenBlock),
		claimChan:      make(chan *ttypes.BlockClaims),
		cfg:            cfg,
		scannerStorage: scanStorage,
		thorchain:      thorchain,
		errCounter:     m.GetCounterVec(metrics.ThorchainBlockScannerError),
		pubkeyMgr:      pubkeyMgr,
	}, nil
}

// GetTxOutMessages return the channel
func (b *ThorchainBlockScan) GetTxOutMessages() <-chan types.TxOut {
	return b.txOutChan
}

func (b *ThorchainBlockScan) GetKeygenMessages() <-chan ttypes.KeygenBlock {
	return b.keygenChan
}

func (b *ThorchainBlockScan) GetBlockClaims() <-chan *ttypes.BlockClaims {
	return b.claimChan
}

func (b *ThorchainBlockScan) GetHeight() (int64, error) {
	return b.thorchain.GetBlockHeight()
}

func (b *ThorchainBlockScan) FetchMemPool(_ int64) (types.TxIn, error) {
	return types.TxIn{}, nil
}

func (b *ThorchainBlockScan) FetchTxs(height int64) (types.TxIn, error) {
	if err := b.processTxOutBlock(height); err != nil {
		return types.TxIn{}, err
	}
	if err := b.processKeygenBlock(height); err != nil {
		return types.TxIn{}, err
	}
	if err := b.processClaims(height); err != nil {
		return types.TxIn{}, err
	}
	return types.TxIn{}, nil
}

func (b *ThorchainBlockScan) processKeygenBlock(blockHeight int64) error {
	pk := b.pubkeyMgr.GetNodePubKey()
	keygen, err := b.thorchain.GetKeygenBlock(blockHeight, pk.String())
	if err != nil {
		return fmt.Errorf("fail to get keygen from thorchain: %w", err)
	}

	// custom error (to be dropped and not logged) because the block is
	// available yet
	if keygen.Height == 0 {
		return btypes.ErrUnavailableBlock
	}

	if len(keygen.Keygens) > 0 {
		b.keygenChan <- keygen
	}
	return nil
}

func (b *ThorchainBlockScan) processTxOutBlock(blockHeight int64) error {
	for _, pk := range b.pubkeyMgr.GetSignPubKeys() {
		if len(pk.String()) == 0 {
			continue
		}
		tx, err := b.thorchain.GetKeysign(blockHeight, pk.String())
		if err != nil {
			if errors.Is(err, btypes.ErrUnavailableBlock) {
				// custom error (to be dropped and not logged) because the block is
				// available yet
				return btypes.ErrUnavailableBlock
			}
			return fmt.Errorf("fail to get keysign from block scanner: %w", err)
		}

		if len(tx.TxArray) == 0 {
			b.logger.Debug().Int64("block", blockHeight).Msg("nothing to process")
			continue
		}
		b.txOutChan <- tx
	}
	return nil
}

func (b *ThorchainBlockScan) processClaims(blockHeight int64) error {
	blockClaims, err := b.thorchain.GetClaims(blockHeight)
	if err != nil {
		if errors.Is(err, btypes.ErrUnavailableBlock) {
			// custom error (to be dropped and not logged) because the block is
			// available yet
			return btypes.ErrUnavailableBlock
		}
		return fmt.Errorf("fail to get block claims from block scanner: %w", err)
	}
	if blockClaims == nil {
		return nil
	}
	if len(blockClaims.Claims) == 0 {
		b.logger.Debug().Int64("block", blockHeight).Msg("nothing to process")
		return nil
	}
	b.claimChan <- blockClaims
	return nil
}
