package chainclients

import (
	"math/big"

	tb "gitlab.com/blackprotocol/tss/go-tss/blame"

	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	stypes "gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/config"
	otypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// ChainClient is the interface that wraps basic chain client methods
//
// SignTx       signs transactions
// BroadcastTx  broadcast transactions on the chain associated with the client
// GetChain     get chain id
// SignTx       sign transaction
// GetHeight    get chain height
// GetAddress   gets address for public key pool in chain
// GetAccount   gets account from thorclient in cain
// GetConfig	gets the chain configuration
// Start
// Stop
type ChainClient interface {
	SignTx(tx stypes.TxOutItem, height int64) ([]byte, error)
	BroadcastTx(_ stypes.TxOutItem, _ []byte) (string, error)
	GetHeight() (int64, error)
	GetAddress(poolPubKey common.PubKey) string
	GetAccount(poolPubKey common.PubKey, height *big.Int) (common.Account, error)
	GetAccountByAddress(address string, height *big.Int) (common.Account, error)
	GetChain() common.Chain
	OnObservedTxIn(txIn types.TxInItem, blockHeight int64)
	Start(globalTxsQueue chan stypes.TxIn, globalErrataQueue chan stypes.ErrataBlock, globalSolvencyQueue chan stypes.Solvency)
	GetConfig() config.BifrostChainConfiguration
	GetConfirmationCount(txIn stypes.TxIn) int64
	ConfirmationCountReady(txIn stypes.TxIn) bool
	IsBlockScannerHealthy() bool
	Stop()
	// ValidateClaim just pass the claim item to chain client , chain client should store it locally ,
	// and then return the validation result when block scanner ask for the next block
	ValidateClaim(item otypes.ClaimItem) error
	// CustomKeygen to indicate whether chain client will manage its own keygen & keysign process
	// if it returns true, then when BlackChain issue keygen instruction , the chain client's Keygen method will be called
	CustomKeygen() bool
	// Keygen instruct chain client to generate a new key for asgard
	Keygen(keys []string, blockHeight int64, version string, walletAddress string) (otypes.ChainAddress, tb.Blame, error)
}
