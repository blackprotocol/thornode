package monero

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

const (
	blockClaimPrefix = "blockclaim-v1-"
	pubkeyPrefix     = "pubkey-v1-"
)

type BlockClaimItemStore struct {
	logger zerolog.Logger
	db     *leveldb.DB
}

// NewBlockClaimItemStore create a new instance of block claim
func NewBlockClaimItemStore(db *leveldb.DB) *BlockClaimItemStore {
	return &BlockClaimItemStore{
		logger: log.With().Str("module", "blockclaim-store").Logger(),
		db:     db,
	}
}

// SetBlockClaimItem save the given blockclaim into key value store
func (bcs *BlockClaimItemStore) SetBlockClaimItem(claimItem types.ClaimItem) error {
	if err := claimItem.Valid(); err != nil {
		return fmt.Errorf("invalid claim item,err: %w", err)
	}
	hash, err := claimItem.Hash()
	if err != nil {
		return fmt.Errorf("fail to get claim item hash,err: %w", err)
	}
	key := fmt.Sprintf("%s%s", blockClaimPrefix, hash)
	bcs.logger.Debug().Msgf("save claim item with key:%s,payload: %s", key, common.Jsonfy(claimItem))
	buf, err := json.Marshal(claimItem)
	if err != nil {
		return fmt.Errorf("fail to marshal blockclaim,err: %w", err)
	}
	return bcs.db.Put([]byte(key), buf, nil)
}

// RemoveClaimItem remove the given block claim from keyvalue store
func (bcs *BlockClaimItemStore) RemoveClaimItem(claimItem types.ClaimItem) error {
	if err := claimItem.Valid(); err != nil {
		return fmt.Errorf("invalid claim item,err: %w", err)
	}
	hash, err := claimItem.Hash()
	if err != nil {
		return fmt.Errorf("fail to get claim item hash,err: %w", err)
	}
	key := fmt.Sprintf("%s%s", blockClaimPrefix, hash)
	bcs.logger.Debug().Msgf("remove claim item with key:%s", key)
	return bcs.db.Delete([]byte(key), nil)
}

// GetClaimItems retrieve all the  claims from key value store
func (bcs *BlockClaimItemStore) GetClaimItems() ([]types.ClaimItem, error) {
	iter := bcs.db.NewIterator(util.BytesPrefix([]byte(blockClaimPrefix)), nil)
	defer iter.Release()
	var result []types.ClaimItem
	for iter.Next() {
		if !iter.Valid() {
			break
		}
		if len(iter.Value()) == 0 {
			continue
		}
		var claimItem types.ClaimItem
		if err := json.Unmarshal(iter.Value(), &claimItem); err != nil {
			bcs.logger.Err(err).Str("key", string(iter.Key())).Msg("fail to unmarshal blockclaim")
			continue
		}
		result = append(result, claimItem)
	}
	return result, nil
}

func (bcs *BlockClaimItemStore) SetPubKey(pubKey string, claimItem types.ClaimItem) error {
	key := fmt.Sprintf("%s-%s", pubkeyPrefix, pubKey)
	bcs.logger.Debug().Msgf("save claim item with key:%s", key)
	buf, err := json.Marshal(claimItem)
	if err != nil {
		return fmt.Errorf("fail to marshal blockclaim,err: %w", err)
	}
	return bcs.db.Put([]byte(key), buf, nil)
}

func (bcs *BlockClaimItemStore) ExistPubKey(pubKey string) (*types.ClaimItem, error) {
	key := fmt.Sprintf("%s-%s", pubkeyPrefix, pubKey)
	exist, err := bcs.db.Has([]byte(key), nil)
	if err != nil {
		if err == leveldb.ErrNotFound {
			return nil, nil
		}
		return nil, fmt.Errorf("fail to check whether pubkey exist before,err: %w", err)
	}
	if !exist {
		return nil, nil
	}
	value, err := bcs.db.Get([]byte(key), nil)
	if err != nil {
		return nil, fmt.Errorf("fail to get pubkey related claim item,err: %w", err)
	}
	var item types.ClaimItem
	if err := json.Unmarshal(value, &item); err != nil {
		return nil, fmt.Errorf("fail to unmarshal claim item,err: %w", err)
	}
	return &item, nil
}

// Close underlying db
func (bcs *BlockClaimItemStore) Close() error {
	return bcs.db.Close()
}
