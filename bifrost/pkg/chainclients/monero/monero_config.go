package monero

// MoneroCustomConfig some custom config for monero
type MoneroCustomConfig struct {
	WalletRPCA string `json:"wallet_rpc_a"`
	WalletRPCB string `json:"wallet_rpc_b"`
}

// IsEmpty return true when both wallet RPC A and wallet RPC B are empty
func (cfg MoneroCustomConfig) IsEmpty() bool {
	return cfg.WalletRPCA == "" && cfg.WalletRPCB == ""
}
