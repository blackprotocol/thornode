package monero

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/cosmos/cosmos-sdk/crypto/codec"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	monero_rpc "gitlab.com/blackprotocol/monero-rpc"
	"gitlab.com/blackprotocol/monero-rpc/daemon"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"
	common2 "gitlab.com/blackprotocol/tss/go-tss/common"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"
	mcommon "gitlab.com/blackprotocol/tss/monero-tss/common"
	mkeygen "gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig/keygen"
	mkeysign "gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig/keysign"
	storage2 "gitlab.com/blackprotocol/tss/monero-tss/storage"
	mtss2 "gitlab.com/blackprotocol/tss/monero-tss/tss"
	"go.uber.org/atomic"

	"gitlab.com/blackprotocol/blacknode/app"
	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	types2 "gitlab.com/blackprotocol/blacknode/bifrost/blockscanner/types"
	"gitlab.com/blackprotocol/blacknode/bifrost/claimscanner"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/runners"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/shared/utxo"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/signercache"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	sctypes "gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/constants"
	mem "gitlab.com/blackprotocol/blacknode/x/blackchain/memo"
	stypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

const (
	MaxAsgardAddresses = 100
)

type Client struct {
	logger                  zerolog.Logger
	cfg                     config.BifrostChainConfiguration
	m                       *metrics.Metrics
	chain                   common.Chain
	blockScanner            *claimscanner.ClaimScanner
	temporalStorage         *utxo.TemporalStorage
	bridge                  *thorclient.Bridge
	globalErrataQueue       chan<- types.ErrataBlock
	globalSolvencyQueue     chan<- types.Solvency
	nodePubKey              common.PubKey
	currentBlockHeight      *atomic.Int64
	asgardAddresses         []common.Address
	lastAsgard              time.Time
	wg                      *sync.WaitGroup
	stopchan                chan struct{}
	signerLock              *sync.Mutex
	signerCacheManager      *signercache.CacheManager
	daemonClient            daemon.Client
	walletClient            wallet.Client
	claimStorage            *BlockClaimItemStore
	mtss                    *mtss2.ServerImp
	lastSolvencyCheckHeight int64
	lastFee                 uint64
}

// NewClient create a new client for monero client
func NewClient(thorKeys *thorclient.Keys,
	cfg config.BifrostChainConfiguration,
	bridge *thorclient.Bridge,
	m *metrics.Metrics,
	communication *p2p.Communication,
) (*Client, error) {
	var mcfg MoneroCustomConfig
	if len(cfg.AdditionalConfig) == 0 {
		return nil, fmt.Errorf("need to use additional config to specify wallet rpc a and wallet rpc b")
	}
	if err := json.Unmarshal([]byte(cfg.AdditionalConfig), &mcfg); err != nil {
		return nil, fmt.Errorf("fail to json unmarshal additional config")
	}
	if mcfg.IsEmpty() {
		return nil, fmt.Errorf("monero custom config is empty")
	}

	daemonClient := daemon.New(monero_rpc.Config{
		Address:       cfg.RPCHost,
		CustomHeaders: nil,
		Transport:     http.DefaultTransport,
	})
	walletClient := wallet.New(monero_rpc.Config{
		Address:       cfg.ChainHost,
		CustomHeaders: nil,
		Transport:     http.DefaultTransport,
	})
	thorPrivateKey, err := thorKeys.GetPrivateKey()
	if err != nil {
		return nil, fmt.Errorf("fail to get BlackChain private key: %w", err)
	}
	temp, err := codec.ToTmPubKeyInterface(thorPrivateKey.PubKey())
	if err != nil {
		return nil, fmt.Errorf("fail to get tm pub key: %w", err)
	}
	nodePubKey, err := common.NewPubKeyFromCrypto(temp)
	if err != nil {
		return nil, fmt.Errorf("fail to get the node pubkey: %w", err)
	}
	tmPrivateKey := common.CosmosPrivateKeyToTMPrivateKey(thorPrivateKey)
	mtss, err := mtss2.NewTss(communication,
		tmPrivateKey, app.DefaultNodeHome,
		mcommon.TssConfig{
			KeyGenTimeout:  300 * time.Second, // must be shorter than constants.JailTimeKeygen
			KeySignTimeout: 60 * time.Second,  // must be shorter than constants.JailTimeKeysign
			PartyTimeout:   45 * time.Second,
			EnableMonitor:  true,
			WalletRPCA:     mcfg.WalletRPCA,
			WalletRPCB:     mcfg.WalletRPCB,
			WalletPassword: cfg.Password, // password set in the chain config
		}, nil, storage2.NewFileStateMgr)
	if err != nil {
		return nil, fmt.Errorf("fail to create monero tss instance,err: %w", err)
	}
	if err := mtss.Start(); err != nil {
		return nil, fmt.Errorf("fail to start monero tss instance,err: %w", err)
	}
	c := &Client{
		logger:              log.Logger.With().Str("module", "monero").Logger(),
		cfg:                 cfg,
		m:                   m,
		chain:               common.XMRChain,
		bridge:              bridge,
		globalErrataQueue:   nil,
		globalSolvencyQueue: nil,
		nodePubKey:          nodePubKey,
		currentBlockHeight:  atomic.NewInt64(0),
		asgardAddresses:     nil,
		lastAsgard:          time.Time{},
		wg:                  &sync.WaitGroup{},
		signerLock:          &sync.Mutex{},
		stopchan:            make(chan struct{}),
		daemonClient:        daemonClient,
		walletClient:        walletClient,
		mtss:                mtss,
	}
	var path string // if not set later, will in memory storage
	if len(c.cfg.BlockScanner.DBPath) > 0 {
		path = fmt.Sprintf("%s/%s", c.cfg.BlockScanner.DBPath, c.cfg.BlockScanner.ChainID)
	}
	storage, err := blockscanner.NewBlockScannerStorage(path)
	if err != nil {
		return c, fmt.Errorf("fail to create blockscanner storage: %w", err)
	}

	c.blockScanner, err = claimscanner.NewClaimScanner(c.cfg.BlockScanner, storage, bridge, c)
	if err != nil {
		return c, fmt.Errorf("fail to create block scanner: %w", err)
	}
	c.temporalStorage, err = utxo.NewTemporalStorage(storage.GetInternalDb())
	if err != nil {
		return c, fmt.Errorf("fail to create temporal storage: %w", err)
	}

	signerCacheManager, err := signercache.NewSignerCacheManager(storage.GetInternalDb())
	if err != nil {
		return nil, fmt.Errorf("fail to create signer cache manager,err: %w", err)
	}
	c.signerCacheManager = signerCacheManager
	c.claimStorage = NewBlockClaimItemStore(storage.GetInternalDb())
	return c, nil
}

// Start starts the block scanner
func (c *Client) Start(globalTxsQueue chan types.TxIn, globalErrataQueue chan types.ErrataBlock, globalSolvencyQueue chan types.Solvency) {
	c.globalErrataQueue = globalErrataQueue
	c.globalSolvencyQueue = globalSolvencyQueue

	c.blockScanner.Start(globalTxsQueue)
	c.wg.Add(1)

	go runners.SolvencyCheckRunner(c.GetChain(), c, c.bridge, c.stopchan, c.wg, constants.BlackChainBlockTime)
}

// Stop stops the block scanner
func (c *Client) Stop() {
	c.blockScanner.Stop()
	close(c.stopchan)
	// wait for consolidate utxo to exit
	c.wg.Wait()
}

// GetConfig - get the chain configuration
func (c *Client) GetConfig() config.BifrostChainConfiguration {
	return c.cfg
}

// GetChain returns BTC Chain
func (c *Client) GetChain() common.Chain {
	return common.XMRChain
}

// GetHeight returns current block height
func (c *Client) GetHeight() (int64, error) {
	resp, err := c.daemonClient.GetBlockCount()
	if err != nil {
		return 0, fmt.Errorf("fail to get block height,err: %w", err)
	}
	return int64(resp.Count) - 1, nil
}

func (c *Client) IsBlockScannerHealthy() bool {
	return c.blockScanner.IsHealthy()
}

func (c *Client) GetAccount(pkey common.PubKey, height *big.Int) (common.Account, error) {
	return common.Account{
		Sequence:      0,
		AccountNumber: 0,
		Coins:         nil,
		HasMemoFlag:   false,
	}, nil
}

func (c *Client) GetAccountByAddress(address string, _ *big.Int) (common.Account, error) {
	resp, err := c.mtss.GetBalance(address)
	if err != nil {
		return common.Account{}, fmt.Errorf("fail to get balance for address:%s ,err: %w", address, err)
	}
	c.logger.Info().Msgf("response of balance:%s", common.Jsonfy(resp))
	return common.Account{
		Sequence:      0,
		AccountNumber: 0,
		Coins: common.NewCoins(
			common.NewCoin(common.XMRAsset, cosmos.NewUint(resp.Balance/10000)),
		),
		HasMemoFlag: false,
	}, nil
}

// SignTx sign the given transaction
func (c *Client) SignTx(tx sctypes.TxOutItem, height int64) ([]byte, error) {
	if !tx.Chain.Equals(common.XMRChain) {
		return nil, errors.New("not monero chain")
	}
	if tx.Coins.IsEmpty() {
		return nil, nil
	}
	if c.signerCacheManager.HasSigned(tx.CacheHash()) {
		c.logger.Info().Msgf("transaction(%+v), signed before , ignore", tx)
		return nil, nil
	}
	xmrCoin := tx.Coins.GetCoin(common.XMRAsset)
	request := wallet.RequestTransfer{
		Destinations: []*wallet.Destination{
			{
				Amount:  xmrCoin.Amount.Uint64() * 10000, // XMR Asset is in 1E12 , however BlackChain is in 1E8
				Address: tx.ToAddress.String(),
			},
		},
	}
	buf, err := json.Marshal(request)
	if err != nil {
		return nil, fmt.Errorf("fail to marshal request to json,err: %w", err)
	}
	// get current BlackChain block height
	blockHeight, err := c.bridge.GetBlockHeight()
	if err != nil {
		return nil, fmt.Errorf("fail to get blacknode height,err: %w", err)
	}
	keysignHeight := blockHeight / 20 * 20
	// round the block height
	resp, err := c.mtss.KeySign(mkeysign.Request{
		BlockHeight:   keysignHeight,
		Version:       "0.1.0",
		WalletAddress: tx.FromAddress.String(),
		EncodedTx:     base64.StdEncoding.EncodeToString(buf),
		Memo:          tx.Memo,
	})
	if err != nil {
		return nil, fmt.Errorf("fail to sign transaction,err: %w", err)
	}
	if resp.Status == common2.Fail || len(resp.Blame.BlameNodes) > 0 || len(resp.TxID) == 0 {
		return nil, fmt.Errorf("fail to sign transaction")
	}
	c.logger.Info().Msgf("response from keysign: %s", common.Jsonfy(resp))
	respTxID, err := common.NewTxID(resp.TxID)
	if err != nil {
		c.logger.Err(err).Msg("fail to parse outbound tx id")
	} else {
		blackTxID, err := c.bridge.PostOutboundClaim(c.chain, tx.FromAddress.String(), tx.ToAddress.String(), resp.Signature, respTxID, tx.Memo)
		if err != nil {
			c.logger.Err(err).Msg("fail to post outbound claim")
		} else {
			c.logger.Info().Str("TxID", blackTxID.String()).Msg("send outbound claim to blackchain successfully")
		}

	}
	c.logger.Info().Str("TxID", resp.TxID).Msgf("keysign successfully")
	if err := c.signerCacheManager.SetSigned(tx.CacheHash(), resp.TxID); err != nil {
		c.logger.Err(err).Msgf("fail to mark tx out item (%+v) as signed", tx)
	}
	return nil, nil
}

func (c *Client) BroadcastTx(_ sctypes.TxOutItem, _ []byte) (string, error) {
	// monero transaction will be broadcast as part of keysign
	return "", nil
}

func (c *Client) GetAddress(poolPubKey common.PubKey) string {
	c.logger.Error().Msgf("don't know the address for poolPubKey: %s", poolPubKey)
	return ""
}

func (c *Client) OnObservedTxIn(txIn types.TxInItem, blockHeight int64) {
	m, err := mem.ParseMemo(common.LatestVersion, txIn.Memo)
	if err != nil {
		c.logger.Err(err).Msgf("fail to parse memo: %s", txIn.Memo)
		return
	}
	if !m.IsOutbound() {
		return
	}
	if m.GetTxID().IsEmpty() {
		return
	}
	if err := c.signerCacheManager.SetSigned(txIn.CacheHash(c.GetChain(), m.GetTxID().String()), txIn.Tx); err != nil {
		c.logger.Err(err).Msg("fail to update signer cache")
	}
}

func (c *Client) getAsgardAddress() ([]common.Address, error) {
	if time.Since(c.lastAsgard) < constants.BlackChainBlockTime && c.asgardAddresses != nil {
		return c.asgardAddresses, nil
	}
	vaults, err := c.bridge.GetAsgards()
	if err != nil {
		return nil, fmt.Errorf("fail to get asgards : %w", err)
	}

	for _, v := range vaults {
		var addr common.Address
		for _, item := range v.Addresses {
			if item.Chain.Equals(c.chain) {
				addr = item.Address
				break
			}
		}

		found := false
		for _, item := range c.asgardAddresses {
			if item.Equals(addr) {
				found = true
				break
			}
		}
		if !found {
			c.asgardAddresses = append(c.asgardAddresses, addr)
		}
	}
	if len(c.asgardAddresses) > MaxAsgardAddresses {
		startIdx := len(c.asgardAddresses) - MaxAsgardAddresses
		c.asgardAddresses = c.asgardAddresses[startIdx:]
	}
	c.lastAsgard = time.Now()
	return c.asgardAddresses, nil
}

// GetConfirmationCount figure out how many blocks to confirm the transaction
func (c *Client) GetConfirmationCount(txIn sctypes.TxIn) int64 {
	if len(txIn.TxArray) == 0 {
		return 0
	}
	if txIn.MemPool {
		return 0
	}
	addresses, err := c.getAsgardAddress()
	if err != nil {
		c.logger.Err(err).Msg("fail to get asgard address")
		addresses = c.asgardAddresses
	}
	totalValue := txIn.GetTotalTransactionValue(common.XMRAsset, addresses)
	confirm := totalValue.QuoUint64(defaultCoinbaseValue).Uint64()
	c.logger.Info().Msgf("totalTxValue:%s,total fee and Subsidy:%d,confirmation:%d", totalValue, defaultCoinbaseValue, confirm)
	return int64(confirm)
}

func (c *Client) ConfirmationCountReady(txIn sctypes.TxIn) bool {
	if len(txIn.TxArray) == 0 {
		return true
	}
	// MemPool items doesn't need confirmation and also monero doesn't scan mempool
	if txIn.MemPool {
		return true
	}
	blockHeight := txIn.TxArray[0].BlockHeight
	confirm := txIn.ConfirmationRequired
	c.logger.Info().Msgf("confirmation required: %d", confirm)
	// every tx in txIn already have at least 1 confirmation
	return (c.currentBlockHeight.Load() - blockHeight) >= confirm
}

// FetchClaims scan block with the given height
func (c *Client) FetchClaims(height int64) ([]types.TxIn, error) {
	c.logger.Info().Msgf("start to fetch claims,height: %d", height)
	// only validate claims when a valid block is available
	blockResp, err := c.daemonClient.GetBlock(&daemon.RequestGetBlock{
		Height: uint64(height),
		Hash:   "",
	})
	if err != nil {
		if strings.Contains(err.Error(), "greater than current top block height:") {
			return nil, types2.ErrUnavailableBlock
		}
		return nil, fmt.Errorf("fail to get block,err: %w", err)
	}

	// send network fee , calculated average network fee
	totalTxs := len(blockResp.BlockDetail.TxHashes)
	if totalTxs > 0 {
		averageFee := (blockResp.BlockHeader.Reward - defaultCoinbaseValue) / uint64(totalTxs) / 10000
		if averageFee > 0 {
			networkFeeTxID, err := c.bridge.PostNetworkFee(height, c.GetChain(), 1, averageFee)
			if err != nil {
				c.logger.Err(err).Msg("fail to send network fee")
			}
			c.lastFee = averageFee
			c.logger.Info().Str("tx_id", networkFeeTxID.String()).Msg("network fee")
		}
	}
	if c.IsBlockScannerHealthy() {
		if err := c.ReportSolvency(height); err != nil {
			c.logger.Err(err).Msg("fail to send solvency info to blackchain")
		}
	}
	items, err := c.claimStorage.GetClaimItems()
	if err != nil {
		return nil, fmt.Errorf("fail to get claim items,err: %w", err)
	}
	c.logger.Info().Msgf("there are %d items need to scan", len(items))
	// nothing need to scan
	if len(items) == 0 {
		return nil, nil
	}
	heightToTxInMap := make(map[uint64]*types.TxIn)

	for _, item := range items {
		resp, err := c.walletClient.CheckTxProof(&wallet.RequestCheckTxProof{
			TxID:      item.TxID.String(),
			Address:   item.To,
			Message:   item.Message,
			Signature: item.Signature,
		})
		if err != nil {
			c.logger.Err(err).
				Str("TxID", item.TxID.String()).
				Str("Address", item.To).
				Str("Message", item.Message).
				Str("Signature", item.Signature).
				Msg("fail to check tx proof")
			continue
		}
		c.logger.Debug().Msgf("response for check tx proof,%s", common.Jsonfy(resp))
		if !resp.Good {
			c.logger.Err(err).
				Str("TxID", item.TxID.String()).
				Str("Address", item.To).
				Str("Message", item.Message).
				Str("Signature", item.Signature).
				Msg("not valid, skip it")
			// claim is not good , remove it from key value store and move on
			if err := c.claimStorage.RemoveClaimItem(item); err != nil {
				// fail to remove a claim item from storage is not critical , the worst is bifrost will observe the inbound again, which cause slash points
				c.logger.Err(err).Msg("fail to remove claim item from storage")
			}
			continue
		}
		if resp.InPool && resp.Confirmations == 0 {
			c.logger.Info().Msgf("resp: %s", common.Jsonfy(resp))
			// even the claim is valid , but it is still in mempool , thus skip it for now , and retry it later.
			continue
		}
		// amount in monero is 1E12, need to convert to 1E8
		amount := cosmos.NewUint(resp.Received / 10000)
		respTrans, err := c.daemonClient.GetTransactions(&daemon.RequestGetTransactions{
			TxsHashes: []string{
				item.TxID.String(),
			},
			DecodeAsJson: true,
			Prune:        false,
		})
		if err != nil {
			c.logger.Err(err).Str("TxID", item.TxID.String()).Msg("fail to get transaction")
			continue
		}
		if len(respTrans.Txs) == 0 {
			c.logger.Info().Str("TxID", item.TxID.String()).Msg("failed to get transaction,transaction in response is empty")
			continue
		}

		// bifrost only retrieve one transaction at a time
		tx := respTrans.Txs[0]
		pubKey, err := extractPubKeysFromExtra(tx.Detail.Extra)
		if err != nil {
			c.logger.Err(err).Msg("fail to extract pubkey from extra field")
			continue
		}
		ci, err := c.claimStorage.ExistPubKey(pubKey)
		if err != nil {
			c.logger.Err(err).Msg("fail to check whether the pubkey has been observed before")
			continue
		}
		if ci != nil {
			c.logger.Error().
				Str("TxID", ci.TxID.String()).
				Str("Address", ci.To).
				Str("Message", ci.Message).
				Str("Signature", ci.Signature).
				Msgf("pubkey(%s) has been observed before", pubKey)
			continue
		}
		// save pubkey and claim item into key value store is unlikely to fail
		// but if it failed for some reasons, then we should not observe ,otherwise it might be hit with burning bug.
		if err := c.claimStorage.SetPubKey(pubKey, item); err != nil {
			c.logger.Err(err).Msg("fail to save pubkey")
			continue
		}
		// monero amount is in 1E12 , convert it to 1E 8
		gas := cosmos.NewUint(tx.Detail.RCTSignatures.TxnFee / 10000)
		txIn, ok := heightToTxInMap[tx.BlockHeight]
		if !ok {
			txIn = &types.TxIn{
				Chain:                common.XMRChain,
				TxArray:              nil,
				ConfirmationRequired: 0,
			}
			heightToTxInMap[tx.BlockHeight] = txIn
		}
		// construct types.TxIn
		txIn.TxArray = append(txIn.TxArray, types.TxInItem{
			BlockHeight: int64(tx.BlockHeight),
			Tx:          item.TxID.String(),
			Memo:        item.Message,
			Sender:      item.From,
			To:          item.To,
			Coins: common.Coins{
				common.NewCoin(common.XMRAsset, amount),
			},
			Gas: common.Gas{
				common.NewCoin(common.XMRAsset, gas),
			},
		})
		if err := c.claimStorage.RemoveClaimItem(item); err != nil {
			// fail to remove a claim item from storage is not critical , the worst is bifrost will observe the inbound again, which cause slash points
			c.logger.Err(err).Msg("fail to remove claim item from storage")
			continue
		}
	}
	c.currentBlockHeight.Store(height)
	var result []types.TxIn
	for _, item := range heightToTxInMap {
		item.Count = strconv.Itoa(len(item.TxArray))
		result = append(result, *item)
	}
	return result, nil
}

// ValidateClaim validate the given claim, if the claim is true then return true, and populate correct types.TxIn
// if it is not valid , then return error
func (c *Client) ValidateClaim(item stypes.ClaimItem) error {
	// save the given claim into local key value store
	if err := item.Valid(); err != nil {
		return fmt.Errorf("invalid claim item,err: %w", err)
	}
	return c.claimStorage.SetBlockClaimItem(item)
}
func (c *Client) CustomKeygen() bool { return true }

// Keygen instruct chain client to generate a new key for asgard
func (c *Client) Keygen(keys []string, blockHeight int64, version string, walletAddress string) (stypes.ChainAddress, tb.Blame, error) {
	// kick off keygen process , and generate a new vault
	// walletAddress need to be the address of current asgard , which local node is a member of
	// monero wallet is stateful , only one wallet can be open at the sametime, if we open one wallet, the other get closed
	// if a wallet get closed for a while , upon opening it again , it needs to catch up with the block scanning, which takes time.
	// and it will cause keysign to timeout , with two wallets daemon ,  both retiring wallet & active wallet can stay open at the same time
	// by passing current walletAddress , monero-tss will not to close it
	// if walletAddress is empty, means the nodes doesn't belong to any retiring vault , thus it will choose a wallet daemon
	resp, err := c.mtss.Keygen(mkeygen.Request{
		Keys:          keys,
		BlockHeight:   blockHeight,
		Version:       version,
		WalletAddress: walletAddress,
	})
	if err != nil {
		return stypes.ChainAddress{}, resp.Blame, fmt.Errorf("fail to generate a new wallet,err: %w", err)
	}
	addr, err := common.NewAddress(resp.PoolAddress)
	if err != nil {
		return stypes.ChainAddress{}, resp.Blame, fmt.Errorf("invalid address,err: %w", err)
	}
	return stypes.ChainAddress{
		Chain:   c.chain,
		Address: addr,
	}, resp.Blame, nil
}

// extractPubKeysFromExtra monero will pact the pubkey of the random secret key into the extra field, we need to extract it , and then make sure
// the same pubkey has not been used before , otherwise it means user is trying to send to the same stealth address.
func extractPubKeysFromExtra(extra []byte) (string, error) {
	totalLen := len(extra)
	if totalLen == 0 || totalLen < 33 {
		return "", errors.New("invalid input")
	}
	for i := 0; i < totalLen; {
		switch extra[i] {
		case 0:
			i++
			i += int(extra[i])
		case 1:
			if totalLen-i <= 32 {
				return "", errors.New("tx pubkey has insufficient length")
			}
			pubkey := hex.EncodeToString(extra[i+1 : i+33])
			return pubkey, nil
		case 2:
			i++
			i += int(extra[i])
		case 3:
			i += 40
		case 4: // additional pubkeys
			i++
			i += 32 * int(extra[i])
		case 0xde:
			i++
			i += int(extra[i])
		case 0x17:
			i++
			i += int(extra[i])
		default:
			return "", errors.New("unknown tx extra tag")
		}
	}
	return "", errors.New("fail to extra pubkey")
}

func (c *Client) ShouldReportSolvency(height int64) bool {
	c.logger.Info().Msgf("should report solvency,height:%d , last check: %d", height, c.lastSolvencyCheckHeight)
	return height-c.lastSolvencyCheckHeight > 1
}

func (c *Client) ReportSolvency(height int64) error {
	if !c.ShouldReportSolvency(height) {
		return nil
	}
	c.logger.Info().Msg("solvency check..")
	asgardVaults, err := c.bridge.GetAsgards()
	if err != nil {
		return fmt.Errorf("fail to get asgards,err: %w", err)
	}
	for _, asgard := range asgardVaults {
		hasMembership := false
		for _, member := range asgard.Membership {
			if member == c.nodePubKey.String() {
				hasMembership = true
				break
			}
		}
		// current bifrost doesn't belong to this asgard, so no need to report solvency
		// even it want to , it doesn't know the balance of another address.
		if !hasMembership {
			continue
		}
		var addr common.Address
		for _, item := range asgard.Addresses {
			if item.Chain == common.XMRChain {
				addr = item.Address
			}
		}
		if addr.IsEmpty() {
			continue
		}

		acct, err := c.GetAccountByAddress(addr.String(), nil)
		if err != nil {
			c.logger.Err(err).Msgf("fail to get account balance")
			continue
		}

		if runners.IsVaultSolvent(acct, asgard, cosmos.NewUint(3*c.lastFee)) && c.IsBlockScannerHealthy() {
			// when vault is solvent , don't need to report solvency
			continue
		}

		select {
		case c.globalSolvencyQueue <- types.Solvency{
			Height: height,
			Chain:  common.XMRChain,
			PubKey: asgard.PubKey,
			Coins:  acct.Coins,
		}:
		case <-time.After(constants.BlackChainBlockTime):
			c.logger.Info().Msgf("fail to send solvency info to BlackChain, timeout")
		}
	}
	c.lastSolvencyCheckHeight = height
	return nil
}
