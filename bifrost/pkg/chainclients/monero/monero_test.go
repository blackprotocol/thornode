package monero

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClient_extractPubKeysFromExtra(t *testing.T) {
	// happy path
	input := []byte{1, 22, 136, 120, 231, 87, 127, 148, 128, 174, 165, 96, 126, 56, 163, 136, 67, 186, 28, 197, 252, 149, 8, 159, 68, 118, 65, 43, 71, 233, 174, 209, 159, 2, 1, 0}
	pubKey, err := extractPubKeysFromExtra(input)
	assert.Nil(t, err)
	assert.Equal(t, pubKey, "168878e7577f9480aea5607e38a38843ba1cc5fc95089f4476412b47e9aed19f")
	input = []byte{1, 2, 3, 4}
	pubKey, err = extractPubKeysFromExtra(input)
	assert.Empty(t, pubKey)
	assert.NotNil(t, err)
	input = []byte{2, 22, 136, 120, 231, 87, 127, 148, 128, 174, 165, 96, 126, 56, 163, 136, 67, 186, 28, 197, 252, 149, 8, 159, 68, 118, 65, 43, 71, 233, 174, 209, 159, 2, 1, 0}
	pubKey, err = extractPubKeysFromExtra(input)
	assert.Empty(t, pubKey)
	assert.NotNil(t, err)
}
