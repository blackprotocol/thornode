package avalanche

import (
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/aggregators"
)

func LatestAggregatorContracts() []common.Address {
	var addrs []common.Address
	for _, agg := range aggregators.DexAggregators(common.LatestVersion) {
		if agg.Chain.Equals(common.AVAXChain) {
			addrs = append(addrs, common.Address(agg.Address))
		}
	}
	return addrs
}
