package chainclients

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/blackprotocol/tss/go-tss/tss"

	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/binance"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/monero"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/thorchain"
	"gitlab.com/blackprotocol/blacknode/bifrost/pubkeymanager"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/config"
)

// LoadChains returns chain clients from chain configuration
func LoadChains(thorKeys *thorclient.Keys,
	cfg map[common.Chain]config.BifrostChainConfiguration,
	tssServer *tss.TssServer,
	thorchainBridge *thorclient.Bridge,
	m *metrics.Metrics,
	pubKeyValidator pubkeymanager.PubKeyValidator,
	poolMgr thorclient.PoolManager,
) map[common.Chain]ChainClient {
	logger := log.Logger.With().Str("module", "bifrost").Logger()
	chains := make(map[common.Chain]ChainClient)

	for _, chain := range cfg {
		if chain.Disabled {
			logger.Info().Msgf("%s chain is disabled by configure", chain.ChainID)
			continue
		}
		switch chain.ChainID {
		case common.BNBChain:
			bnb, err := binance.NewBinance(thorKeys, chain, tssServer, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.BNBChain] = bnb
		case common.XMRChain:
			// create XMR chain client
			m, err := monero.NewClient(thorKeys, chain, thorchainBridge, m, tssServer.GetCommunication())
			if err != nil {
				log.Fatal().Err(err).Msg("fail to create monero client")
				continue
			}
			chains[common.XMRChain] = m
		case common.THORChain:
			thor, err := thorchain.NewClient(thorKeys, chain, tssServer, thorchainBridge, m)
			if err != nil {
				logger.Fatal().Err(err).Str("chain_id", chain.ChainID.String()).Msg("fail to load chain")
				continue
			}
			chains[common.THORChain] = thor
		default:
			continue
		}
	}

	return chains
}
