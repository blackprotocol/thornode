package thorchain

import (
	"context"
	"errors"
	"fmt"
	"math/big"
	"sync"
	"time"

	cerrors "cosmossdk.io/errors"
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	cryptocodec "github.com/cosmos/cosmos-sdk/crypto/codec"
	ctypes "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	txtypes "github.com/cosmos/cosmos-sdk/types/tx"
	signingtypes "github.com/cosmos/cosmos-sdk/types/tx/signing"
	"github.com/cosmos/cosmos-sdk/x/auth/signing"
	"github.com/cosmos/cosmos-sdk/x/auth/tx"
	atypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	btypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tendermint/tendermint/crypto"
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"
	tssp "gitlab.com/blackprotocol/tss/go-tss/tss"

	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/runners"
	"gitlab.com/blackprotocol/blacknode/bifrost/pkg/chainclients/signercache"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	stypes "gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/bifrost/tss"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/memo"
	tcTypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// CosmosSuccessCodes a transaction is considered successful if it returns 0
// or if tx is unauthorized or already in the mempool (another Bifrost already sent it)
var CosmosSuccessCodes = map[uint32]bool{
	cerrors.SuccessABCICode:                  true,
	sdkerrors.ErrTxInMempoolCache.ABCICode(): true,
	sdkerrors.ErrWrongSequence.ABCICode():    true,
}

type Client struct {
	logger              zerolog.Logger
	cfg                 config.BifrostChainConfiguration
	chainID             string
	txConfig            client.TxConfig
	txClient            txtypes.ServiceClient
	bankClient          btypes.QueryClient
	accountClient       atypes.QueryClient
	accts               *CosmosMetaDataStore
	tssKeyManager       *tss.KeySign
	localKeyManager     *keyManager
	bridge              *thorclient.Bridge
	storage             *blockscanner.BlockScannerStorage
	blockScanner        *blockscanner.BlockScanner
	signerCacheManager  *signercache.CacheManager
	cosmosScanner       *ThorchainBlockScanner
	globalSolvencyQueue chan stypes.Solvency
	wg                  *sync.WaitGroup
	stopchan            chan struct{}
}

// NewClient create a new instance of thorchain chain client
func NewClient(thorKeys *thorclient.Keys,
	cfg config.BifrostChainConfiguration,
	server *tssp.TssServer,
	bridge *thorclient.Bridge,
	m *metrics.Metrics,
) (*Client, error) {
	logger := log.With().Str("module", cfg.ChainID.String()).Logger()
	logger.Info().Msgf("config: %s", common.Jsonfy(cfg))
	tssKm, err := tss.NewKeySign(server, bridge)
	if err != nil {
		return nil, fmt.Errorf("fail to create tss signer: %w", err)
	}

	priv, err := thorKeys.GetPrivateKey()
	if err != nil {
		return nil, fmt.Errorf("fail to get private key: %w", err)
	}

	temp, err := cryptocodec.ToTmPubKeyInterface(priv.PubKey())
	if err != nil {
		return nil, fmt.Errorf("fail to get tm pub key: %w", err)
	}
	pk, err := common.NewPubKeyFromCrypto(temp)
	if err != nil {
		return nil, fmt.Errorf("fail to get pub key: %w", err)
	}
	if bridge == nil {
		return nil, fmt.Errorf("blackchain bridge is nil")
	}

	localKm := &keyManager{
		privKey: priv,
		addr:    ctypes.AccAddress(priv.PubKey().Address()),
		pubkey:  pk,
	}
	grpcConn, err := getGRPCConn(cfg.CosmosGRPCHost, cfg.CosmosGRPCTLS)
	if err != nil {
		return nil, fmt.Errorf("fail to create grpc connection,err: %w", err)
	}

	interfaceRegistry := codectypes.NewInterfaceRegistry()
	// The definition of MsgSend is the same between thorchain and blackchain
	interfaceRegistry.RegisterImplementations((*ctypes.Msg)(nil), &tcTypes.MsgSend{})
	marshaler := codec.NewProtoCodec(interfaceRegistry)
	txConfig := tx.NewTxConfig(marshaler, []signingtypes.SignMode{signingtypes.SignMode_SIGN_MODE_DIRECT})

	c := &Client{
		chainID:         chain_id,
		logger:          logger,
		cfg:             cfg,
		txConfig:        txConfig,
		txClient:        txtypes.NewServiceClient(grpcConn),
		bankClient:      btypes.NewQueryClient(grpcConn),
		accountClient:   atypes.NewQueryClient(grpcConn),
		accts:           NewCosmosMetaDataStore(),
		tssKeyManager:   tssKm,
		localKeyManager: localKm,
		bridge:          bridge,
		wg:              &sync.WaitGroup{},
		stopchan:        make(chan struct{}),
	}

	var path string // if not set later, will in memory storage
	if len(c.cfg.BlockScanner.DBPath) > 0 {
		path = fmt.Sprintf("%s/%s", c.cfg.BlockScanner.DBPath, c.cfg.BlockScanner.ChainID)
	}
	c.storage, err = blockscanner.NewBlockScannerStorage(path)
	if err != nil {
		return nil, fmt.Errorf("fail to create scan storage: %w", err)
	}

	c.cosmosScanner, err = NewCosmosBlockScanner(
		c.cfg.BlockScanner,
		c.storage,
		c.bridge,
		m,
		c.ReportSolvency,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create cosmos scanner: %w", err)
	}

	c.blockScanner, err = blockscanner.NewBlockScanner(c.cfg.BlockScanner, c.storage, m, c.bridge, c.cosmosScanner)
	if err != nil {
		return nil, fmt.Errorf("failed to create block scanner: %w", err)
	}

	signerCacheManager, err := signercache.NewSignerCacheManager(c.storage.GetInternalDb())
	if err != nil {
		return nil, fmt.Errorf("fail to create signer cache manager")
	}
	c.signerCacheManager = signerCacheManager

	return c, nil
}

// Start Cosmos chain client
func (c *Client) Start(globalTxsQueue chan stypes.TxIn, _ chan stypes.ErrataBlock, globalSolvencyQueue chan stypes.Solvency) {
	c.globalSolvencyQueue = globalSolvencyQueue
	c.tssKeyManager.Start()
	c.blockScanner.Start(globalTxsQueue)
	c.wg.Add(1)
	go runners.SolvencyCheckRunner(c.GetChain(), c, c.bridge, c.stopchan, c.wg, constants.BlackChainBlockTime)
}

// Stop Cosmos chain client
func (c *Client) Stop() {
	c.tssKeyManager.Stop()
	c.blockScanner.Stop()
	if err := c.cosmosScanner.grpc.Close(); err != nil {
		c.logger.Err(err).Msg("fail to close cosmos grpc connection")
	}
	close(c.stopchan)
	c.wg.Wait()
}

// GetConfig return the configuration used by Cosmos chain client
func (c *Client) GetConfig() config.BifrostChainConfiguration {
	return c.cfg
}

func (c *Client) IsBlockScannerHealthy() bool {
	return c.blockScanner.IsHealthy()
}

func (c *Client) GetChain() common.Chain {
	return c.cfg.ChainID
}

func (c *Client) GetHeight() (int64, error) {
	return c.blockScanner.FetchLastHeight()
}

// GetAddress return current signer address, it will be bech32 encoded address
func (c *Client) GetAddress(poolPubKey common.PubKey) string {
	addr, err := poolPubKey.GetAddress(c.GetChain())
	if err != nil {
		c.logger.Err(err).Str("pool_pub_key", poolPubKey.String()).Msg("fail to get pool address")
		return ""
	}
	return addr.String()
}

func (c *Client) GetAccount(pkey common.PubKey, _ *big.Int) (common.Account, error) {
	addr, err := pkey.GetAddress(c.GetChain())
	if err != nil {
		return common.Account{}, fmt.Errorf("failed to convert address (%s) from bech32: %w", pkey, err)
	}
	return c.GetAccountByAddress(addr.String(), big.NewInt(0))
}

func (c *Client) GetAccountByAddress(address string, _ *big.Int) (common.Account, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	bankReq := &btypes.QueryAllBalancesRequest{
		Address: address,
	}
	balances, err := c.bankClient.AllBalances(ctx, bankReq)
	if err != nil {
		return common.Account{}, err
	}
	nativeCoins := make([]common.Coin, 0)
	for _, balance := range balances.Balances {
		coin, err := fromThorchainToBlackchain(balance)
		if err != nil {
			c.logger.Err(err).Interface("balances", balances.Balances).Msg("wasn't able to convert coins that passed whitelist")
			continue
		}
		nativeCoins = append(nativeCoins, coin)
	}

	authReq := &atypes.QueryAccountRequest{
		Address: address,
	}

	acc, err := c.accountClient.Account(ctx, authReq)
	if err != nil {
		return common.Account{}, err
	}
	ba := new(atypes.BaseAccount)
	err = ba.Unmarshal(acc.GetAccount().Value)
	if err != nil {
		return common.Account{}, err
	}
	return common.Account{
		Sequence:      int64(ba.Sequence),
		AccountNumber: int64(ba.AccountNumber),
		Coins:         nativeCoins,
	}, nil
}

func (c *Client) processOutboundTx(tx stypes.TxOutItem, _ int64) (*tcTypes.MsgSend, error) {
	fromAddr, err := tx.VaultPubKey.GetAddress(c.GetChain())
	if err != nil {
		return nil, fmt.Errorf("failed to convert address (%s) to bech32: %w", tx.VaultPubKey.String(), err)
	}
	if len(tx.Coins) > 1 {
		return nil, fmt.Errorf("doesn't support to send multiple coins in one transaction")
	}
	var coins ctypes.Coins
	for _, coin := range tx.Coins {
		// convert to cosmos coin
		cosmosCoin, err := fromBlackchainToThorchain(coin)
		if err != nil {
			c.logger.Err(err).Interface("tx", tx).Msg("unable to convert coin fromBlackchainToThorchain")
			continue
		}
		coins = append(coins, cosmosCoin)
	}
	buf, err := cosmos.GetFromBech32(fromAddr.String(), c.GetChain().AddressPrefix(common.GetCurrentChainNetwork()))
	if err != nil {
		return nil, fmt.Errorf("fail to parse bech32 address(%s), err: %w ", fromAddr.String(), err)
	}
	toBuf, err := cosmos.GetFromBech32(tx.ToAddress.String(), c.GetChain().AddressPrefix(common.GetCurrentChainNetwork()))
	if err != nil {
		return nil, fmt.Errorf("fail to parse bech32 to address(%s), err: %w", tx.ToAddress.String(), err)
	}
	return tcTypes.NewMsgSend(
		ctypes.AccAddress(buf),
		ctypes.AccAddress(toBuf),
		coins.Sort()), nil
}

// SignTx sign the given TxArrayItem
func (c *Client) SignTx(tx stypes.TxOutItem, thorchainHeight int64) (signedTx []byte, err error) {
	if c.signerCacheManager.HasSigned(tx.CacheHash()) {
		c.logger.Info().Interface("tx", tx).Msg("transaction already signed, ignoring...")
		return nil, nil
	}

	msg, err := c.processOutboundTx(tx, thorchainHeight)
	if err != nil {
		return nil, fmt.Errorf("fail to create MsgSend,err: %w", err)
	}

	currentHeight, err := c.cosmosScanner.GetHeight()
	if err != nil {
		return nil, fmt.Errorf("fail to get current block height,err: %w", err)
	}

	// Check if we have CosmosMetadata for the current block height
	// before fetching it from the GRPC server
	meta := c.accts.Get(tx.VaultPubKey)
	if currentHeight > meta.BlockHeight {
		acc, err := c.GetAccount(tx.VaultPubKey, nil)
		if err != nil {
			return nil, fmt.Errorf("fail to get account info: %w", err)
		}
		// Only update local sequence # if it is less than what is on chain
		// When local sequence # is larger than on chain , that could be there are transactions in mempool not commit yet
		if meta.SeqNumber <= acc.Sequence {
			meta = CosmosMetadata{
				AccountNumber: acc.AccountNumber,
				SeqNumber:     acc.Sequence,
				BlockHeight:   currentHeight,
			}
			c.accts.Set(tx.VaultPubKey, meta)
		}
	}
	// THORChain will directly deduct fee from sending account
	fee := ctypes.Coins{}
	txBuilder, err := buildUnsigned(
		c.txConfig,
		msg,
		tx.VaultPubKey,
		tx.Memo,
		fee,
		uint64(meta.SeqNumber),
	)
	if err != nil {
		return nil, fmt.Errorf("unable to build unsigned tx: %w", err)
	}

	txBytes, err := c.signMsg(
		txBuilder,
		tx.VaultPubKey,
		uint64(meta.AccountNumber),
		uint64(meta.SeqNumber),
		tx.FromAddress.String(),
	)
	if err != nil {
		// if the keysign failure is caused by TSS, then post keysign failure to blackchain
		var keysignError tss.KeysignError
		if errors.As(err, &keysignError) {
			if len(keysignError.Blame.BlameNodes) == 0 {
				c.logger.Err(err).Msg("TSS doesn't know which node to blame")
			} else {
				// key sign error forward the keysign blame to thorchain
				txID, err := c.bridge.PostKeysignFailure(keysignError.Blame, thorchainHeight, tx.Memo, tx.Coins, tx.VaultPubKey)
				if err != nil {
					c.logger.Err(err).Msg("fail to post keysign failure to blackchain")
				} else {
					c.logger.Info().Str("tx_id", txID.String()).Msgf("post keysign failure to blackchain")
				}
			}
		}
		return nil, fmt.Errorf("failed to sign message: %w", err)
	}
	return txBytes, nil
}

// signMsg takes an unsigned msg in a txBuilder and signs it using either private key or TSS.
func (c *Client) signMsg(
	txBuilder client.TxBuilder,
	pubkey common.PubKey,
	account uint64,
	sequence uint64,
	fromAddr string,
) ([]byte, error) {
	cpk, err := cosmos.GetPubKeyFromBech32(cosmos.Bech32PubKeyTypeAccPub, pubkey.String())
	if err != nil {
		return nil, fmt.Errorf("unable to GetPubKeyFromBech32: %w", err)
	}

	modeHandler := c.txConfig.SignModeHandler()
	signingData := signing.SignerData{
		Address:       fromAddr,
		ChainID:       c.chainID,
		AccountNumber: account,
		Sequence:      sequence,
		PubKey:        cpk,
	}

	signBytes, err := modeHandler.GetSignBytes(signingtypes.SignMode_SIGN_MODE_DIRECT, signingData, txBuilder.GetTx())
	if err != nil {
		return nil, fmt.Errorf("unable to GetSignBytes on modeHandler: %w", err)
	}

	sigData := &signingtypes.SingleSignatureData{
		SignMode: signingtypes.SignMode_SIGN_MODE_DIRECT,
	}
	sig := signingtypes.SignatureV2{
		PubKey:   cpk,
		Data:     sigData,
		Sequence: sequence,
	}

	if c.localKeyManager.Pubkey().Equals(pubkey) {
		sigData.Signature, err = c.localKeyManager.Sign(signBytes)
		if err != nil {
			return nil, fmt.Errorf("unable to sign using localKeyManager: %w", err)
		}
	} else {
		hashedMsg := crypto.Sha256(signBytes)
		sigData.Signature, _, err = c.tssKeyManager.RemoteSign(hashedMsg, pubkey.String())
		if err != nil {
			return nil, err
		}
	}

	// Ensure the signature is valid
	if !cpk.VerifySignature(signBytes, sigData.Signature) {
		return nil, fmt.Errorf("unable to verify signature with secpPubKey")
	}

	err = txBuilder.SetSignatures(sig)
	if err != nil {
		return nil, fmt.Errorf("unable to final SetSignatures on txBuilder: %w", err)
	}

	txBytes, err := c.txConfig.TxEncoder()(txBuilder.GetTx())
	if err != nil {
		return nil, fmt.Errorf("unable to encode tx: %w", err)
	}

	return txBytes, nil
}

// BroadcastTx is to broadcast the tx to cosmos chain
func (c *Client) BroadcastTx(tx stypes.TxOutItem, txBytes []byte) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	req := &txtypes.BroadcastTxRequest{
		TxBytes: txBytes,
		Mode:    txtypes.BroadcastMode_BROADCAST_MODE_SYNC,
	}

	broadcastRes, err := c.txClient.BroadcastTx(ctx, req)
	if err != nil {
		return "", err
	}
	c.logger.Info().Interface("response", broadcastRes).Msg("transaction broadcast result")
	success := false
	if len(broadcastRes.TxResponse.TxHash) > 0 &&
		(broadcastRes.TxResponse.Code == 19 || broadcastRes.TxResponse.Code == 32 || broadcastRes.TxResponse.Code == 0) {
		success = true
	}
	if !success {
		c.logger.Error().Interface("response", broadcastRes).Msg("unsuccessful error code in transaction broadcast")
		return "", errors.New("broadcast msg failed")
	}

	c.accts.SeqInc(tx.VaultPubKey)
	// Only add the transaction to signer cache when it is certain the transaction has been broadcast successfully.
	// So for other scenario , like transaction already in mempool , invalid account sequence # , the transaction can be rescheduled , and retried
	if success {
		if err := c.signerCacheManager.SetSigned(tx.CacheHash(), broadcastRes.TxResponse.TxHash); err != nil {
			c.logger.Err(err).Msg("fail to set signer cache")
		}
	}
	c.logger.Info().Msgf("successfully broadcast transaction, tx_hash: %s", broadcastRes.TxResponse.TxHash)
	return broadcastRes.TxResponse.TxHash, nil
}

// ConfirmationCountReady cosmos chain has almost instant finality, so doesn't need to wait for confirmation
func (c *Client) ConfirmationCountReady(_ stypes.TxIn) bool {
	return true
}

// GetConfirmationCount determine how many confirmations are required
// NOTE: Cosmos chains are instant finality, so confirmations are not needed.
// If the transaction was successful, we know it is included in a block and thus immutable.
func (c *Client) GetConfirmationCount(_ stypes.TxIn) int64 {
	return 0
}

func (c *Client) ReportSolvency(blockHeight int64) error {
	if !c.ShouldReportSolvency(blockHeight) {
		return nil
	}
	asgardVaults, err := c.bridge.GetAsgards()
	if err != nil {
		return fmt.Errorf("fail to get asgards,err: %w", err)
	}
	for _, asgard := range asgardVaults {
		acct, err := c.GetAccount(asgard.PubKey, big.NewInt(0))
		if err != nil {
			c.logger.Err(err).Msgf("fail to get account balance")
			continue
		}
		if runners.IsVaultSolvent(acct, asgard, cosmos.NewUint(NativeRUNETransactionFee)) && c.IsBlockScannerHealthy() {
			continue
		}
		select {
		case c.globalSolvencyQueue <- stypes.Solvency{
			Height: blockHeight,
			Chain:  c.cfg.ChainID,
			PubKey: asgard.PubKey,
			Coins:  acct.Coins,
		}:
		case <-time.After(constants.BlackChainBlockTime):
			c.logger.Info().Msgf("fail to send solvency info to THORChain, timeout")
		}
	}
	return nil
}

func (c *Client) ShouldReportSolvency(height int64) bool {
	// Block time on Cosmos-based chains generally hovers around 6 seconds (10
	// blocks/min). Since the last fee is used as a buffer we also want to ensure that is
	// non-zero (enough blocks have been seen) before checking insolvency to avoid false
	// positives.
	return height%10 == 0
}

// OnObservedTxIn update the signer cache (in case we haven't already)
func (c *Client) OnObservedTxIn(txIn stypes.TxInItem, _ int64) {
	m, err := memo.ParseMemo(common.LatestVersion, txIn.Memo)
	c.logger.Info().Str("memo", m.String())
	if err != nil {
		c.logger.Err(err).Msgf("fail to parse memo: %s", txIn.Memo)
		return
	}
	if !m.IsOutbound() {
		return
	}
	if m.GetTxID().IsEmpty() {
		return
	}
	if err := c.signerCacheManager.SetSigned(txIn.CacheHash(c.GetChain(), m.GetTxID().String()), txIn.Tx); err != nil {
		c.logger.Err(err).Msg("fail to update signer cache")
	}
}

// ValidateClaim thorchain doesn't need to validate claim
func (c *Client) ValidateClaim(_ tcTypes.ClaimItem) error {
	return nil
}

// CustomKeygen thorchain will just use TSS key , no custom keygen
func (c *Client) CustomKeygen() bool {
	return false
}

// Keygen keygen method will not be used
func (c *Client) Keygen(_ []string, _ int64, _ string, _ string) (tcTypes.ChainAddress, tb.Blame, error) {
	return tcTypes.ChainAddress{}, tb.Blame{}, nil
}
