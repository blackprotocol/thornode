package thorchain

import "strings"

type AssetMapping struct {
	Denom           string
	Decimals        int
	THORChainSymbol string
}

// THORChainAssetMappings maps a  denom to a THORChain symbol and provides the asset decimals
var THORChainAssetMappings = []AssetMapping{
	{
		Denom:           "rune",
		Decimals:        8,
		THORChainSymbol: "RUNE",
	},
}

// GetAssetByThorchainDenom given the thorchain denom , it returns asset mapping
func GetAssetByThorchainDenom(denom string) (AssetMapping, bool) {
	for _, asset := range THORChainAssetMappings {
		if strings.EqualFold(asset.Denom, denom) {
			return asset, true
		}
	}
	return AssetMapping{}, false
}

// GetAssetByThorchainSymbol given the THORChain symbol , it return the asset mapping
func GetAssetByThorchainSymbol(symbol string) (AssetMapping, bool) {
	for _, asset := range THORChainAssetMappings {
		if strings.EqualFold(asset.THORChainSymbol, symbol) {
			return asset, true
		}
	}
	return AssetMapping{}, false
}
