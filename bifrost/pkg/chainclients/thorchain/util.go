package thorchain

import (
	"crypto/x509"
	"fmt"

	"github.com/cosmos/cosmos-sdk/client"
	ctypes "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32"
	signingtypes "github.com/cosmos/cosmos-sdk/types/tx/signing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	tcTypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// buildUnsigned takes a MsgSend and other parameters and returns a txBuilder
// It can be used to simulateTx or as the input to signMsg before BraodcastTx
func buildUnsigned(
	txConfig client.TxConfig,
	msg *tcTypes.MsgSend,
	pubkey common.PubKey,
	memo string,
	fee ctypes.Coins,
	sequence uint64,
) (client.TxBuilder, error) {
	cpk, err := cosmos.GetPubKeyFromBech32(cosmos.Bech32PubKeyTypeAccPub, pubkey.String())
	if err != nil {
		return nil, fmt.Errorf("unable to GetPubKeyFromBech32 from cosmos: %w", err)
	}
	txBuilder := txConfig.NewTxBuilder()

	err = txBuilder.SetMsgs(msg)
	if err != nil {
		return nil, fmt.Errorf("unable to SetMsgs on txBuilder: %w", err)
	}

	txBuilder.SetMemo(memo)
	txBuilder.SetFeeAmount(fee)
	txBuilder.SetGasLimit(10000000)

	sigData := &signingtypes.SingleSignatureData{
		SignMode: signingtypes.SignMode_SIGN_MODE_DIRECT,
	}
	sig := signingtypes.SignatureV2{
		PubKey:   cpk,
		Data:     sigData,
		Sequence: sequence,
	}

	err = txBuilder.SetSignatures(sig)
	if err != nil {
		return nil, fmt.Errorf("unable to initial SetSignatures on txBuilder: %w", err)
	}

	return txBuilder, nil
}

func fromThorchainToBlackchain(c cosmos.Coin) (common.Coin, error) {
	cosmosAsset, exists := GetAssetByThorchainDenom(c.Denom)
	if !exists {
		return common.NoCoin, fmt.Errorf("asset does not exist / not whitelisted by client")
	}

	thorAsset, err := common.NewAsset(fmt.Sprintf("%s.%s", common.THORChain.String(), cosmosAsset.THORChainSymbol))
	if err != nil {
		return common.NoCoin, fmt.Errorf("invalid thorchain asset: %w", err)
	}

	return common.Coin{
		Asset:    thorAsset,
		Amount:   cosmos.NewUint(c.Amount.Uint64()),
		Decimals: int64(cosmosAsset.Decimals),
	}, nil
}

func fromBlackchainToThorchain(coin common.Coin) (cosmos.Coin, error) {
	asset, exists := GetAssetByThorchainSymbol(coin.Asset.Symbol.String())
	if !exists {
		return cosmos.Coin{}, fmt.Errorf("asset does not exist / not whitelisted by client")
	}

	amount := coin.Amount.BigInt()
	return cosmos.NewCoin(asset.Denom, ctypes.NewIntFromBigInt(amount)), nil
}

// Bifrost only supports an "RPCHost" in its configuration.
// We also need to access GRPC for Cosmos chains
func getGRPCConn(host string, tls bool) (*grpc.ClientConn, error) {
	// load system certificates or proceed with insecure if tls disabled
	var creds credentials.TransportCredentials
	if tls {
		certs, err := x509.SystemCertPool()
		if err != nil {
			return &grpc.ClientConn{}, fmt.Errorf("unable to load system certs: %w", err)
		}
		creds = credentials.NewClientTLSFromCert(certs, "")
	} else {
		creds = insecure.NewCredentials()
	}

	return grpc.Dial(host, grpc.WithTransportCredentials(creds))
}

func accAddressToString(acc ctypes.AccAddress, prefix string) (string, error) {
	if acc.Empty() {
		return "", fmt.Errorf("empty account address")
	}

	bech32Addr, err := bech32.ConvertAndEncode(prefix, acc)
	if err != nil {
		return "", fmt.Errorf("unable to encode AccAddress to bech32: %w", err)
	}

	return bech32Addr, nil
}
