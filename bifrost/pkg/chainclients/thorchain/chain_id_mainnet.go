//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package thorchain

var chain_id = `thorchain-mainnet-v1`
