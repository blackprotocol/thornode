package thorchain

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/codec"
	ctypes "github.com/cosmos/cosmos-sdk/types"
	signingtypes "github.com/cosmos/cosmos-sdk/types/tx/signing"
	"github.com/cosmos/cosmos-sdk/x/auth/tx"
	btypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tendermint/tendermint/crypto/tmhash"
	tmtypes "github.com/tendermint/tendermint/proto/tendermint/types"
	rpcclient "github.com/tendermint/tendermint/rpc/client/http"
	"google.golang.org/grpc"

	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	tcTypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

const (
	NativeRUNETransactionFee = 2000000
)

// SolvencyReporter is to report solvency info to BlackNode
type SolvencyReporter func(int64) error

var _ ctypes.Msg = &tcTypes.MsgSend{}

// ThorchainBlockScanner is to scan the blocks
type ThorchainBlockScanner struct {
	cfg              config.BifrostBlockScannerConfiguration
	logger           zerolog.Logger
	db               blockscanner.ScannerStorage
	cdc              *codec.ProtoCodec
	txConfig         client.TxConfig
	txService        *rpcclient.HTTP
	tmService        tmservice.ServiceClient
	grpc             *grpc.ClientConn
	bridge           *thorclient.Bridge
	solvencyReporter SolvencyReporter
	thorchainRPC     http.Client
	lastFee          int64
}

// NewCosmosBlockScanner create a new instance of BlockScan
func NewCosmosBlockScanner(cfg config.BifrostBlockScannerConfiguration,
	scanStorage blockscanner.ScannerStorage,
	bridge *thorclient.Bridge,
	m *metrics.Metrics,
	solvencyReporter SolvencyReporter,
) (*ThorchainBlockScanner, error) {
	if scanStorage == nil {
		return nil, errors.New("scanStorage is nil")
	}
	if m == nil {
		return nil, errors.New("metrics is nil")
	}

	logger := log.Logger.With().Str("module", "blockscanner").Str("chain", cfg.ChainID.String()).Logger()

	// Bifrost only supports an "RPCHost" in its configuration.
	// We also need to access GRPC for Cosmos chains

	// Registry for decoding txs
	ctx := bridge.GetContext()
	registry := ctx.InterfaceRegistry

	// Thorchain's MsgSend can be decoded as a ctypes.Msg,
	// Necessary when using the TxDecoder to decode the transaction bytes from Tendermint.
	btypes.RegisterInterfaces(registry)
	registry.RegisterImplementations((*ctypes.Msg)(nil), &tcTypes.MsgSend{})

	cdc := codec.NewProtoCodec(registry)

	grpcConn, err := getGRPCConn(cfg.CosmosGRPCHost, cfg.CosmosGRPCTLS)
	if err != nil {
		logger.Fatal().Err(err).Msg("fail to create grpc connection")
	}

	// Registry for encoding txs
	marshaler := codec.NewProtoCodec(registry)
	txConfig := tx.NewTxConfig(marshaler, []signingtypes.SignMode{signingtypes.SignMode_SIGN_MODE_DIRECT})
	tmService := tmservice.NewServiceClient(grpcConn)
	rpcClient, err := rpcclient.New(cfg.RPCHost, "/websocket")
	if err != nil {
		logger.Fatal().Err(err).Msg("fail to create tendermint rpcclient")
	}

	return &ThorchainBlockScanner{
		cfg:              cfg,
		logger:           logger,
		db:               scanStorage,
		cdc:              cdc,
		txConfig:         txConfig,
		txService:        rpcClient,
		tmService:        tmService,
		grpc:             grpcConn,
		bridge:           bridge,
		solvencyReporter: solvencyReporter,
		thorchainRPC: http.Client{
			Timeout: time.Second * 10,
		},
	}, nil
}

// GetHeight returns the height from the lastest block minus 1
// NOTE: we must lag by one block due to a race condition fetching the block results
// Since the GetLatestBlockRequests tells what transactions will be in the block at T+1
func (c *ThorchainBlockScanner) GetHeight() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	resultHeight, err := c.tmService.GetLatestBlock(
		ctx,
		&tmservice.GetLatestBlockRequest{})
	if err != nil {
		return 0, err
	}
	return resultHeight.GetBlock().Header.Height - 1, nil
}

// FetchMemPool returns nothing since we are only concerned about finalized transactions in Cosmos
func (c *ThorchainBlockScanner) FetchMemPool(height int64) (types.TxIn, error) {
	return types.TxIn{}, nil
}

// GetBlock returns a Tendermint block as a reference to a ResultBlock for a
// given height. As noted above, this is not necessarily the final state of transactions
// and must be checked again for success by getting the BlockResults in FetchTxs
func (c *ThorchainBlockScanner) GetBlock(height int64) (*tmtypes.Block, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	resultBlock, err := c.tmService.GetBlockByHeight(
		ctx,
		&tmservice.GetBlockByHeightRequest{Height: height})
	if err != nil {
		c.logger.Error().Int64("height", height).Msgf("failed to get block: %v", err)
		return nil, fmt.Errorf("failed to get block: %w", err)
	}

	return resultBlock.GetBlock(), nil
}

func (c *ThorchainBlockScanner) processTxs(height int64, rawTxs [][]byte) ([]types.TxInItem, error) {
	// Proto types for Cosmos chains that we are transacting with may not be included in this repo.
	// Therefore, it is necessary to include them in the "proto" directory and register them in
	// the cdc (codec) that is passed below. Registry occurs in the NewCosmosBlockScanner function.
	decoder := tx.DefaultTxDecoder(c.cdc)

	// Fetch the block results so that we can ensure the transaction was successful before processing a TxInItem
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	blockResults, err := c.txService.BlockResults(ctx, &height)
	if err != nil {
		return []types.TxInItem{}, fmt.Errorf("unable to get BlockResults: %w", err)
	}

	var txIn []types.TxInItem
	for i, rawTx := range rawTxs {
		hash := hex.EncodeToString(tmhash.Sum(rawTx))
		decodedTx, err := decoder(rawTx)
		if err != nil {
			c.logger.Info().Str("tx", string(rawTx)).Err(err).Msg("unable to decode msg")
			if strings.Contains(err.Error(), "unable to resolve type URL") {
				// One of the transaction message contains an unknown type
				// Though the transaction may contain valid MsgSend, we only support transactions
				// containing MsgSend.
				// Check for these in the error before discarding the transaction.
				if strings.Contains(err.Error(), "MsgSend") {
					// double check to make sure MsgSend isn't mentioned
					c.logger.Error().Str("tx", string(rawTx)).Err(err).Msg("unable to decode msg")
				}
			}
			continue
		}

		mem, _ := decodedTx.(ctypes.TxWithMemo)
		memo := mem.GetMemo()

		for _, msg := range decodedTx.GetMsgs() {
			if msg, isMsgSend := msg.(*tcTypes.MsgSend); isMsgSend {
				// Transaction contains a relevant MsgSend, check if the transaction was successful...
				if blockResults.TxsResults[i].Code != 0 {
					c.logger.Warn().Str("txhash", hash).Int64("height", height).Msg("inbound decodedTx has non-zero response code, ignoring...")
					continue
				}

				// Convert cosmos coins to thorchain coins (taking into account asset decimal precision)
				coins := common.Coins{}
				for _, coin := range msg.Amount {
					cCoin, err := fromThorchainToBlackchain(coin)
					if err != nil {
						c.logger.Debug().Err(err).Interface("coins", c).Msg("unable to convert coin, not whitelisted. skipping...")
						continue
					}
					coins = append(coins, cCoin)
				}

				// Ignore the decodedTx when no coins exist
				if coins.IsEmpty() {
					continue
				}

				// Change AccAddress to strings
				// Can't use AccAddress.String() because uses cosmos config with Maya prefixes
				fromAddr, err := accAddressToString(msg.FromAddress, c.cfg.ChainID.AddressPrefix(common.GetCurrentChainNetwork()))
				if err != nil {
					c.logger.Err(err).Msg("unable to convert from address")
					continue
				}

				toAddr, err := accAddressToString(msg.ToAddress, c.cfg.ChainID.AddressPrefix(common.GetCurrentChainNetwork()))
				if err != nil {
					c.logger.Err(err).Msg("unable to convert to address")
					continue
				}

				txIn = append(txIn, types.TxInItem{
					Tx:          hash,
					BlockHeight: height,
					Memo:        memo,
					Sender:      fromAddr,
					To:          toAddr,
					Coins:       coins,
					Gas: common.Gas{
						common.NewCoin(common.RuneAsset(), cosmos.NewUint(NativeRUNETransactionFee)),
					},
				})

				// If there are more than one TxIn item per transaction hash,
				// will fail to process any after the first.
				// Therefore, limit to 1 MsgSend per transaction.
				break
			}
		}

	}

	return txIn, nil
}

func (c *ThorchainBlockScanner) FetchTxs(height int64) (types.TxIn, error) {
	block, err := c.GetBlock(height)
	if err != nil {
		c.logger.Debug().AnErr("could not get block", err)
		return types.TxIn{}, err
	}
	txs, err := c.processTxs(height, block.Data.Txs)
	if err != nil {
		c.logger.Debug().Err(err).Msg("error processing txs")
		return types.TxIn{}, err
	}

	txIn := types.TxIn{
		Count:    strconv.Itoa(len(txs)),
		Chain:    c.cfg.ChainID,
		TxArray:  txs,
		Filtered: false,
		MemPool:  false,
	}

	if err := c.solvencyReporter(height); err != nil {
		c.logger.Err(err).Msg("fail to send solvency to THORChain")
	}
	if err := c.ProcessNetworkFee(height); err != nil {
		c.logger.Err(err).Msg("fail to process network fee")
	}
	return txIn, nil
}

func (c *ThorchainBlockScanner) GetTHORChainRPChost(rpcHost string) string {
	u, err := url.Parse(rpcHost)
	if err != nil {
		c.logger.Err(err).Msgf("fail to parse rpc host(%s)", rpcHost)
		return ""
	}
	return fmt.Sprintf("%s:%d", u.Hostname(), 1317)
}

func (c *ThorchainBlockScanner) ProcessNetworkFee(height int64) error {
	rpcHost := c.GetTHORChainRPChost(c.cfg.RPCHost)
	if len(rpcHost) == 0 {
		return fmt.Errorf("fail to get RPC Host")
	}
	networkFeeUrl := fmt.Sprintf("http://%s/thorchain/mimir/key/NativeTransactionFee", rpcHost)
	resp, err := c.thorchainRPC.Get(networkFeeUrl)
	if err != nil {
		return fmt.Errorf("fail to process network fee,err: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %s", resp.Status)
	}
	var value int64
	buf, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("fail to read from response body,err: %w", err)
	}
	if err := json.Unmarshal(buf, &value); err != nil {
		return fmt.Errorf("fail to unmarshal mimir: %w", err)
	}
	if value <= 0 {
		value = NativeRUNETransactionFee
	}
	if value != c.lastFee {
		txID, err := c.bridge.PostNetworkFee(height, common.THORChain, 1, uint64(value))
		if err != nil {
			c.logger.Err(err).Msg("fail to post network fee")
		} else {
			c.logger.Info().Msgf("post network fee successfully, tx_id:%s", txID)
		}
		c.lastFee = value
	}
	return nil
}
