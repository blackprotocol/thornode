package gaia

import "strings"

type CosmosAssetMapping struct {
	CosmosDenom      string
	CosmosDecimals   int
	BlackChainSymbol string
}

// CosmosAssetMappings maps a Cosmos denom to a blackchain symbol and provides the asset decimals
// CHANGEME: define assets that should be observed by blackchain here. This also acts a whitelist.
var CosmosAssetMappings = []CosmosAssetMapping{
	{
		CosmosDenom:      "uatom",
		CosmosDecimals:   6,
		BlackChainSymbol: "ATOM",
	},
}

func GetAssetByCosmosDenom(denom string) (CosmosAssetMapping, bool) {
	for _, asset := range CosmosAssetMappings {
		if strings.EqualFold(asset.CosmosDenom, denom) {
			return asset, true
		}
	}
	return CosmosAssetMapping{}, false
}

func GetAssetByBlackchainSymbol(symbol string) (CosmosAssetMapping, bool) {
	for _, asset := range CosmosAssetMappings {
		if strings.EqualFold(asset.BlackChainSymbol, symbol) {
			return asset, true
		}
	}
	return CosmosAssetMapping{}, false
}
