package thorclient

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/blang/semver"
	"github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/crypto/hd"
	cKeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	ckeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	"github.com/cosmos/cosmos-sdk/simapp"
	sdk "github.com/cosmos/cosmos-sdk/types"
	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	cosmos2 "gitlab.com/blackprotocol/blacknode/bifrost/mock/cosmos"
	"gitlab.com/blackprotocol/blacknode/cmd"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/x/blackchain"
	stypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

type ThorchainBridgeTestSuite struct {
	suite.Suite
	cfg    config.BifrostClientConfiguration
	kb     ckeys.Keyring
	bridge *Bridge
}

func (s *ThorchainBridgeTestSuite) SetupTest() {
	cfg2 := cosmos.GetConfig()
	cfg2.SetBech32PrefixForAccount(cmd.Bech32PrefixAccAddr, cmd.Bech32PrefixAccAddr)
	blackchain.SetupConfigForTest()
	s.cfg = config.BifrostClientConfiguration{
		ChainID:         "thorchain",
		ChainHost:       "localhost",
		ChainRPC:        "localhost",
		ChainGRPC:       "localhost:9090",
		SignerName:      "thorchain",
		SignerPasswd:    "password",
		ChainHomeFolder: "",
	}
	s.kb = cKeys.NewInMemory(simapp.MakeTestEncodingConfig().Codec)

	params := *hd.NewFundraiserParams(0, sdk.CoinType, 0)
	hdPath := params.String()

	// create a consistent user
	info, err := s.kb.NewAccount(s.cfg.SignerName, "industry segment educate height inject hover bargain offer employ select speak outer video tornado story slow chief object junk vapor venue large shove behave", s.cfg.SignerPasswd, hdPath, hd.Secp256k1)
	s.Nil(err)
	s.NotNil(info)
	bridge, err := NewBridge(s.cfg, GetMetricForTest(), NewKeysWithKeybase(s.kb, s.cfg.SignerName, s.cfg.SignerPasswd))
	s.Nil(err)
	s.bridge = bridge
}

func (s *ThorchainBridgeTestSuite) Test_getAccountNumberAndSequenceNumber() {
	mockAuthQc := new(cosmos2.MockAuthQueryClient)
	s.bridge.authQueryClient = mockAuthQc
	// when failed to get auth accounts , it should result in an error
	mockAccount := mockAuthQc.On("Account", mock.Anything, mock.Anything).Return(&authtypes.QueryAccountResponse{}, errors.New("fail to get account"))
	accountNumber, seqNo, err := s.bridge.getAccountNumberAndSequenceNumber()
	s.NotNil(err)
	s.Zero(accountNumber)
	s.Zero(seqNo)

	// fail to decode account should result in an error
	mockAccount.Unset()
	mockAccount = mockAuthQc.On("Account", mock.Anything, mock.Anything).Return(&authtypes.QueryAccountResponse{
		Account: &types.Any{
			TypeUrl: "whatever",
			Value:   []byte{1, 2, 4},
		},
	}, nil)
	accountNumber, seqNo, err = s.bridge.getAccountNumberAndSequenceNumber()
	s.NotNil(err)
	s.Zero(accountNumber)
	s.Zero(seqNo)

	// happy path
	mockAccount.Unset()
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	ba := authtypes.NewBaseAccount(stypes.GetRandomBech32Addr(), pubKey, 10, 10)
	acct, err := types.NewAnyWithValue(ba)
	s.Nil(err)
	mockAuthQc.On("Account", mock.Anything, mock.Anything).Return(&authtypes.QueryAccountResponse{
		Account: acct,
	}, nil)
	accountNumber, seqNo, err = s.bridge.getAccountNumberAndSequenceNumber()
	s.Nil(err)
	s.Equal(uint64(10), accountNumber)
	s.Equal(uint64(10), seqNo)
}

func (s *ThorchainBridgeTestSuite) TestNewThorchainBridge() {
	cfg := config.BifrostClientConfiguration{
		ChainID:         "",
		ChainHost:       "localhost",
		ChainHomeFolder: "~/.thorcli",
		SignerName:      "signer",
		SignerPasswd:    "signerpassword",
	}
	cdc := simapp.MakeTestEncodingConfig().Codec
	kb := ckeys.NewInMemory(cdc)
	_, _, err := kb.NewMnemonic(cfg.SignerName, cKeys.English, cmd.THORChainHDPath, cfg.SignerPasswd, hd.Secp256k1)
	require.Nil(s.T(), err)

	sb, err := NewBridge(cfg, m, NewKeysWithKeybase(kb, cfg.SignerName, cfg.SignerPasswd))
	require.Nil(s.T(), sb)
	require.NotNil(s.T(), err)
	cfg = config.BifrostClientConfiguration{
		ChainID:         "chainid",
		ChainHost:       "",
		ChainHomeFolder: "~/.thorcli",
		SignerName:      "signer",
		SignerPasswd:    "signerpassword",
	}
	sb, err = NewBridge(cfg, m, NewKeysWithKeybase(kb, cfg.SignerName, cfg.SignerPasswd))
	require.Nil(s.T(), sb)
	require.NotNil(s.T(), err)
}

func (s *ThorchainBridgeTestSuite) TestGetObservationStdTx_OutboundShouldHaveNotConfirmationCounting() {
	pk := stypes.GetRandomPubKey()
	vaultAddr, err := pk.GetAddress(common.BNBChain)
	require.Nil(s.T(), err)
	tx := stypes.NewObservedTx(
		common.Tx{
			Coins: common.Coins{
				common.NewCoin(common.BNBAsset, cosmos.NewUint(123400000)),
			},
			Memo:        "This is my memo!",
			FromAddress: vaultAddr,
			ToAddress:   "bnb1ntqj0v0sv62ut0ehxt7jqh7lenfrd3hmfws0aq",
		},
		1,
		pk,
		100,
	)

	signedMsg, err := s.bridge.GetObservationsStdTx(stypes.ObservedTxs{tx}, func(chain common.Chain, pubkey common.PubKey) (common.Address, error) {
		return pubkey.GetAddress(chain)
	})
	require.NotNil(s.T(), signedMsg)
	require.Nil(s.T(), err)
	m, ok := signedMsg[0].(*stypes.MsgObservedTxOut)
	require.True(s.T(), ok)
	require.True(s.T(), m.Txs[0].FinaliseHeight == m.Txs[0].BlockHeight)
}

func (s *ThorchainBridgeTestSuite) TestSign() {
	pk := stypes.GetRandomPubKey()
	vaultAddr, err := pk.GetAddress(common.BTCChain)
	require.Nil(s.T(), err)
	tx := stypes.NewObservedTx(
		common.Tx{
			Coins: common.Coins{
				common.NewCoin(common.BTCAsset, cosmos.NewUint(123400000)),
			},
			Memo:        "This is my memo!",
			FromAddress: vaultAddr,
			ToAddress:   common.Address("btc1ntqj0v0sv62ut0ehxt7jqh7lenfrd3hmfws0aq"),
		},
		1,
		pk,
		1,
	)

	signedMsg, err := s.bridge.GetObservationsStdTx(stypes.ObservedTxs{tx}, func(chain common.Chain, pubkey common.PubKey) (common.Address, error) {
		return pubkey.GetAddress(chain)
	})
	require.NotNil(s.T(), signedMsg)
	require.Nil(s.T(), err)
}

func (s *ThorchainBridgeTestSuite) TestEnsureNodeWhitelisted() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	nodeAddr, err := s.bridge.keys.GetSignerInfo().GetAddress()
	require.Nil(s.T(), err)
	resp := stypes.QueryNodeResponse{
		Node: &stypes.QueryNodeAccount{
			NodeAddress: nodeAddr.String(),
			Status:      blackchain.NodeActive,
			PubKeySet: &common.PubKeySet{
				Secp256k1: stypes.GetRandomPubKey(),
				Ed25519:   stypes.GetRandomPubKey(),
			},
			ValidatorConsPubKey: stypes.GetRandomBech32ConsensusPubKey(),
			Bond:                cosmos.NewUint(1000 * common.One),
			ActiveBlockHeight:   100,
			BondAddress:         stypes.GetRandomXBXAddress(),
			StatusSince:         1,
			SignerMembership: []string{
				stypes.GetRandomPubKey().String(),
			},
			RequestedToLeave: false,
			ForcedToLeave:    false,
			LeaveScore:       0,
			IPAddress:        "127.0.0.1",
			Version:          "1.100.0",
			SlashPoints:      0,
			Jail:             nil,
			CurrentAward:     cosmos.NewUint(1000),
			ObserveChains:    nil,
			PreflightStatus:  nil,
			BondProviders:    nil,
		},
	}
	mockNode := mockQuery.On("Node", mock.Anything, mock.Anything).Return(&stypes.QueryNodeResponse{Node: nil}, errors.New("fail to get node"))
	err = s.bridge.EnsureNodeWhitelisted()
	require.NotNil(s.T(), err)
	mockNode.Unset()

	mockQuery.On("Node", mock.Anything, mock.Anything).Return(&resp, nil)
	err = s.bridge.EnsureNodeWhitelisted()
	require.Nil(s.T(), err)
}

func (s *ThorchainBridgeTestSuite) TestIsCatchingUp() {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if req.RequestURI == "/status" {
			content, err := os.ReadFile("../../test/fixtures/endpoints/status/status.json")
			require.Nil(s.T(), err)
			rw.Header().Set("Content-Type", "application/json")
			_, err = rw.Write(content)
			require.Nil(s.T(), err)
		}
	}))
	defer server.Close()
	ok, err := s.bridge.IsCatchingUp()
	require.NotNil(s.T(), err)
	require.False(s.T(), ok)

	s.bridge.cfg.ChainRPC = server.Listener.Addr().String()
	ok, err = s.bridge.IsCatchingUp()
	require.Nil(s.T(), err)
	require.False(s.T(), ok)
}

func (s *ThorchainBridgeTestSuite) TestGetAsgards() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// when fail to get asgard from grpc , it should result in an error
	mockAsgard := mockQuery.On("Asgards", mock.Anything, mock.Anything).Return(&stypes.QueryAsgardsResponse{}, errors.New("fail to get asgards"))
	vaults, err := s.bridge.GetAsgards()
	require.NotNil(s.T(), err)
	require.Len(s.T(), vaults, 0)
	// happy path
	mockAsgard.Unset()
	v1 := stypes.GetRandomVault()
	resp := stypes.QueryAsgardsResponse{
		Vaults: []*stypes.QueryVault{
			{
				BlockHeight:           v1.BlockHeight,
				PubKey:                v1.PubKey,
				Coins:                 v1.Coins,
				Type:                  v1.Type,
				Status:                v1.Status,
				StatusSince:           v1.StatusSince,
				Membership:            v1.Membership,
				Chains:                v1.Chains,
				InboundTxCount:        v1.InboundTxCount,
				OutboundTxCount:       v1.OutboundTxCount,
				PendingTxBlockHeights: v1.PendingTxBlockHeights,
				Routers:               v1.Routers,
				Addresses:             []stypes.ChainAddress{},
			},
		},
	}
	mockQuery.On("Asgards", mock.Anything, mock.Anything).Return(&resp, nil)
	vaults, err = s.bridge.GetAsgards()
	require.Nil(s.T(), err)
	require.Len(s.T(), vaults, 1)
}

func (s *ThorchainBridgeTestSuite) TestGetPubKeys() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// fail to get pubkeys should result in an error
	mockVaultPubKeys := mockQuery.On("VaultPubkeys", mock.Anything, mock.Anything).Return(&stypes.QueryVaultPubKeysResponse{}, errors.New("fail to get vault pub keys"))
	pks, err := s.bridge.GetPubKeys()
	require.NotNil(s.T(), err)
	require.Nil(s.T(), pks)

	// happy path
	mockVaultPubKeys.Unset()
	resp := &stypes.QueryVaultPubKeysResponse{
		Asgards: []*stypes.QueryVaultPubKeyContract{
			{
				PubKey: stypes.GetRandomPubKey().String(),
				Routers: []stypes.ChainContract{
					{
						Chain:  common.BNBChain,
						Router: stypes.GetRandomBNBAddress(),
					},
				},
			},
		},
	}
	mockQuery.On("VaultPubkeys", mock.Anything, mock.Anything).Return(resp, nil)
	pks, err = s.bridge.GetPubKeys()
	require.Nil(s.T(), err)
	require.NotNil(s.T(), pks)
}

func (s *ThorchainBridgeTestSuite) TestGetConstants() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// when failed to get constants from grpc should result in an error
	mockConstants := mockQuery.On("Constants", mock.Anything, mock.Anything).Return(&stypes.QueryConstantsResponse{}, errors.New("fail to get constants"))
	result, err := s.bridge.GetConstants()
	require.NotNil(s.T(), err)
	require.Nil(s.T(), result)
	// happy path
	mockConstants.Unset()
	mockQuery.On("Constants", mock.Anything, mock.Anything).Return(&stypes.QueryConstantsResponse{
		Int_64Values: map[string]int64{
			"Hello": 1,
		},
		BoolValues:   nil,
		StringValues: nil,
	}, nil)
	result, err = s.bridge.GetConstants()
	require.Nil(s.T(), err)
	require.NotNil(s.T(), result)
	require.Len(s.T(), result, 1)
	require.Equal(s.T(), result["Hello"], int64(1))
}

func (s *ThorchainBridgeTestSuite) TestGetRagnarok() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// when failed to get ragnarok should result in an error
	mockRagnarok := mockQuery.On("Ragnarok", mock.Anything, mock.Anything).Return(&stypes.QueryRagnarokResponse{RagnarokInProgress: false}, errors.New("fail to get ragnarok"))
	result, err := s.bridge.RagnarokInProgress()
	require.NotNil(s.T(), err)
	require.False(s.T(), result)

	// happy path
	mockRagnarok.Unset()
	mockQuery.On("Ragnarok", mock.Anything, mock.Anything).Return(&stypes.QueryRagnarokResponse{RagnarokInProgress: true}, nil)
	result, err = s.bridge.RagnarokInProgress()
	require.Nil(s.T(), err)
	require.True(s.T(), result)
}

func (s *ThorchainBridgeTestSuite) TestGetThorchainVersion() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// fail to get version should result in an error
	mockVersion := mockQuery.On("Version", mock.Anything, mock.Anything).Return(&stypes.QueryVersionResponse{}, errors.New("fail to get version"))
	result, err := s.bridge.GetThorchainVersion()
	require.True(s.T(), result.EQ(semver.Version{}))
	require.NotNil(s.T(), err)

	// happy path
	mockVersion.Unset()
	mockQuery.On("Version", mock.Anything, mock.Anything).Return(&stypes.QueryVersionResponse{
		Current: "1.100.0",
		Next:    "1.100.0",
		Querier: "1.100.0",
	}, nil)
	result, err = s.bridge.GetThorchainVersion()
	require.Nil(s.T(), err)
	require.NotNil(s.T(), result)
}

func (s *ThorchainBridgeTestSuite) TestGetMimir() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery
	// fail to get mimir should result in an error
	mockMimir := mockQuery.On("MimirWithKey", mock.Anything, mock.Anything).Return(&stypes.QueryMimirWithKeyResponse{Value: -1}, errors.New("fail to get mimir"))
	result, err := s.bridge.GetMimir("HaltBTCChain")
	require.NotNil(s.T(), err)
	require.Equal(s.T(), int64(0), result)

	// happy path
	mockMimir.Unset()
	mockQuery.On("MimirWithKey", mock.Anything, mock.Anything).Return(&stypes.QueryMimirWithKeyResponse{Value: 100}, nil)
	result, err = s.bridge.GetMimir("HaltBTCChain")
	require.Nil(s.T(), err)
	require.Equal(s.T(), int64(100), result)
}

func (s *ThorchainBridgeTestSuite) TestTHORName() {
	mockQuery := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockQuery

	// when fail to get thorname it should result in an error
	mockThorName := mockQuery.On("Thorname", mock.Anything, mock.Anything).Return(&stypes.QueryThornameResponse{}, errors.New("fail to get thorname"))
	result, err := s.bridge.GetTHORName("test1")
	require.NotNil(s.T(), err)
	require.True(s.T(), len(result.Name) == 0)

	// happy path
	mockThorName.Unset()
	mockQuery.On("Thorname", mock.Anything, mock.Anything).Return(
		&stypes.QueryThornameResponse{
			Name:              "test1",
			ExpireBlockHeight: 1000,
			Owner:             stypes.GetRandomBlackAddress().String(),
			PreferredAsset:    common.BNBAsset.String(),
			Aliases: []*stypes.QueryThornameAlias{
				{
					Chain:   common.BNBChain,
					Address: stypes.GetRandomBNBAddress(),
				},
			},
		},
		nil,
	)
	result, err = s.bridge.GetTHORName("test1")
	require.Nil(s.T(), err)
	require.True(s.T(), len(result.Aliases) == 1)
}

func (s *ThorchainBridgeTestSuite) TestPostNetworkFee() {
	mockAuthQc := new(cosmos2.MockAuthQueryClient)
	s.bridge.authQueryClient = mockAuthQc
	// when failed to get auth accounts , it should result in an error
	mockAccount := mockAuthQc.On("Account", mock.Anything, mock.Anything).Return(&authtypes.QueryAccountResponse{}, errors.New("fail to get account"))
	mockThorchainQc := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockThorchainQc
	node := stypes.GetRandomValidatorNode(blackchain.NodeActive)
	mockThorchainQc.On("Node", mock.Anything, mock.Anything).Return(
		&stypes.QueryNodeResponse{
			Node: &stypes.QueryNodeAccount{
				NodeAddress:         node.NodeAddress.String(),
				Status:              node.Status,
				PubKeySet:           &node.PubKeySet,
				ValidatorConsPubKey: node.ValidatorConsPubKey,
				Bond:                node.Bond,
				ActiveBlockHeight:   node.ActiveBlockHeight,
				BondAddress:         node.BondAddress,
				StatusSince:         node.StatusSince,
				SignerMembership:    node.SignerMembership,
				RequestedToLeave:    node.RequestedToLeave,
				ForcedToLeave:       node.ForcedToLeave,
				LeaveScore:          node.LeaveScore,
				IPAddress:           node.IPAddress,
				Version:             node.Version,
				SlashPoints:         0,
				Jail:                nil,
				CurrentAward:        sdk.NewUint(0),
				ObserveChains:       nil,
				PreflightStatus:     nil,
				BondProviders:       nil,
			},
		}, nil)
	mockThorchainQc.On("Heights", mock.Anything, mock.Anything).Return(&stypes.QueryHeightsResponse{
		BlockHeights: []*stypes.QueryLastBlockHeight{
			{
				Chain:          common.BTCChain,
				LastObservedIn: 100,
				LastSignedOut:  100,
				Blackchain:     100,
			},
		},
	}, nil)
	txid, err := s.bridge.PostNetworkFee(1024, common.BTCChain, 100, 100)
	require.NotNil(s.T(), err)

	require.True(s.T(), txid.IsEmpty())

	// happy path
	mockAccount.Unset()
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	ba := authtypes.NewBaseAccount(stypes.GetRandomBech32Addr(), pubKey, 10, 10)
	acct, err := types.NewAnyWithValue(ba)
	s.Nil(err)
	mockAuthQc.On("Account", mock.Anything, mock.Anything).Return(&authtypes.QueryAccountResponse{
		Account: acct,
	}, nil)
	txid, err = s.bridge.PostNetworkFee(1024, common.BTCChain, 100, 100)
	require.NotNil(s.T(), err)
	require.True(s.T(), txid.IsEmpty())
}

func (s *ThorchainBridgeTestSuite) TestGetKeygenBlock() {
	mockThorchainQc := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockThorchainQc
	mockKeygenByPubKey := mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{}, errors.New("fail to get keygen block"))
	// when fail to get keygen block by pubkey , it should result in an error
	block, err := s.bridge.GetKeygenBlock(1024, "whatever")
	s.NotNil(err)
	s.True(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// when signature is empty , it should return an error
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: nil,
		Signature:   "",
	}, nil)
	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.NotNil(err)
	s.True(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// when keygen block is nil , it should return empty
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: nil,
		Signature:   "whatever",
	}, nil)
	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.Nil(err)
	s.True(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// when keygen is empty , it should return empty
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: &stypes.KeygenBlock{
			Height:  1024,
			Keygens: nil,
		},
		Signature: "whatever",
	}, nil)
	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.Nil(err)
	s.False(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// when fail to decode signature it should result in an error
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: &stypes.KeygenBlock{
			Height: 1024,
			Keygens: []stypes.Keygen{
				{
					ID:   stypes.GetRandomTxHash(),
					Type: stypes.KeygenType_AsgardKeygen,
					Members: []string{
						"whatever-1",
					},
				},
			},
		},
		Signature: "wha_tever",
	}, nil)

	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.NotNil(err)
	s.True(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// when fail to decode signature it should result in an error
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: &stypes.KeygenBlock{
			Height: 1024,
			Keygens: []stypes.Keygen{
				{
					ID:   stypes.GetRandomTxHash(),
					Type: stypes.KeygenType_AsgardKeygen,
					Members: []string{
						"whatever-1",
					},
				},
			},
		},
		Signature: "whatever",
	}, nil)

	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.NotNil(err)
	s.True(block.IsEmpty())
	mockKeygenByPubKey.Unset()

	// happy path
	keygenBlock := &stypes.KeygenBlock{
		Height: 1024,
		Keygens: []stypes.Keygen{
			{
				ID:   stypes.GetRandomTxHash(),
				Type: stypes.KeygenType_AsgardKeygen,
				Members: []string{
					"whatever-1",
				},
			},
		},
	}
	buf, err := json.Marshal(keygenBlock)
	s.Nil(err)
	privKey, err := s.bridge.keys.GetPrivateKey()
	s.Nil(err)
	signature, err := privKey.Sign(buf)
	s.Nil(err)
	mockKeygenByPubKey = mockThorchainQc.On("KeygensByPubKey", mock.Anything, mock.Anything).Return(&stypes.QueryKeygenByPubKeyResponse{
		KeygenBlock: keygenBlock,
		Signature:   base64.StdEncoding.EncodeToString(signature),
	}, nil)

	block, err = s.bridge.GetKeygenBlock(1024, "whatever")
	s.Nil(err)
	s.False(block.IsEmpty())
	mockKeygenByPubKey.Unset()
}

func (s *ThorchainBridgeTestSuite) TestGetClaims() {
	mockThorchainQc := new(cosmos2.MockThorchainQueryClient)
	s.bridge.qc = mockThorchainQc

	// when fail to get claims by height , it should result in an error
	mockGetClaim := mockThorchainQc.On("ClaimsByHeight", mock.Anything, mock.Anything).Return(&stypes.QueryClaimsByHeightResponse{
		Blockclaims: nil,
		Signature:   "",
	}, errors.New("fail to get claim by height"))
	blockClaim, err := s.bridge.GetClaims(1024)
	s.NotNil(err)
	s.Nil(blockClaim)
	mockGetClaim.Unset()

	// when claim is nil, then it should return nil
	mockGetClaim = mockThorchainQc.On("ClaimsByHeight", mock.Anything, mock.Anything).Return(&stypes.QueryClaimsByHeightResponse{
		Blockclaims: nil,
		Signature:   "",
	}, nil)
	blockClaim, err = s.bridge.GetClaims(1024)
	s.Nil(err)
	s.Nil(blockClaim)
	mockGetClaim.Unset()

	// when block claim is not empty, but signature is empty it should return error
	srcBlockClaims := &stypes.BlockClaims{
		Height: 1024,
		Claims: []stypes.ClaimItem{
			{
				From:      "from",
				To:        "to",
				Signature: "whatever",
				TxID:      stypes.GetRandomTxHash(),
				Message:   "whatever",
				Signer:    stypes.GetRandomBech32Addr().String(),
				Chain:     common.XMRChain,
			},
		},
	}
	mockGetClaim = mockThorchainQc.On("ClaimsByHeight", mock.Anything, mock.Anything).Return(&stypes.QueryClaimsByHeightResponse{
		Blockclaims: srcBlockClaims,
		Signature:   "",
	}, nil)
	blockClaim, err = s.bridge.GetClaims(1024)
	s.NotNil(err)
	s.Nil(blockClaim)
	mockGetClaim.Unset()

	// fail to signature verification should result in an error
	mockGetClaim = mockThorchainQc.On("ClaimsByHeight", mock.Anything, mock.Anything).Return(&stypes.QueryClaimsByHeightResponse{
		Blockclaims: srcBlockClaims,
		Signature:   "whatever",
	}, nil)
	blockClaim, err = s.bridge.GetClaims(1024)
	s.NotNil(err)
	s.Nil(blockClaim)
	mockGetClaim.Unset()

	// happy path
	buf, err := json.Marshal(srcBlockClaims)
	s.Nil(err)
	priKey, err := s.bridge.keys.GetPrivateKey()
	s.Nil(err)
	s.NotNil(priKey)
	sig, err := priKey.Sign(buf)
	s.Nil(err)

	mockGetClaim = mockThorchainQc.On("ClaimsByHeight", mock.Anything, mock.Anything).Return(&stypes.QueryClaimsByHeightResponse{
		Blockclaims: srcBlockClaims,
		Signature:   base64.StdEncoding.EncodeToString(sig),
	}, nil)
	blockClaim, err = s.bridge.GetClaims(1024)
	s.Nil(err)
	s.NotNil(blockClaim)
	s.True(len(blockClaim.Claims) == 1)
	mockGetClaim.Unset()
}

func TestThorchainBridge(t *testing.T) {
	suite.Run(t, new(ThorchainBridgeTestSuite))
}
