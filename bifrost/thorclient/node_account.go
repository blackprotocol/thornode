package thorclient

import (
	"fmt"

	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// GetNodeAccount retrieves node account for this address from thorchain
func (b *Bridge) GetNodeAccount(thorAddr string) (*types.NodeAccount, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Node(ctx, &types.QueryNodeRequest{Address: thorAddr})
	if err != nil {
		return &types.NodeAccount{}, fmt.Errorf("failed to get node account: %w", err)
	}
	addr, err := cosmos.AccAddressFromBech32(resp.Node.NodeAddress)
	if err != nil {
		return &types.NodeAccount{}, fmt.Errorf("fail to parse node address(%s): %w", resp.Node.NodeAddress, err)
	}
	return &types.NodeAccount{
		NodeAddress:         addr,
		Status:              resp.Node.Status,
		PubKeySet:           *resp.Node.PubKeySet,
		ValidatorConsPubKey: resp.Node.ValidatorConsPubKey,
		Bond:                resp.Node.Bond,
		ActiveBlockHeight:   resp.Node.ActiveBlockHeight,
		BondAddress:         resp.Node.BondAddress,
		StatusSince:         resp.Node.StatusSince,
		SignerMembership:    resp.Node.SignerMembership,
		RequestedToLeave:    resp.Node.RequestedToLeave,
		ForcedToLeave:       resp.Node.ForcedToLeave,
		LeaveScore:          resp.Node.LeaveScore,
		IPAddress:           resp.Node.IPAddress,
		Version:             resp.Node.Version,
		Type:                types.NodeType_TypeValidator,
	}, nil
}
