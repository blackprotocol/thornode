package types

import (
	"gitlab.com/blackprotocol/blacknode/common"
)

// Solvency structure is to hold all the information necessary to report solvency to blacknode
type Solvency struct {
	Height int64
	Chain  common.Chain
	PubKey common.PubKey
	Coins  common.Coins
}
