package types

import (
	stypes "gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

type Msg struct {
	Type  string                 `json:"type"`
	Value stypes.MsgObservedTxIn `json:"value"`
}
