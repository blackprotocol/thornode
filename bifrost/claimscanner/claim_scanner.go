package claimscanner

import (
	"errors"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/blackprotocol/blacknode/bifrost/blockscanner"
	btypes "gitlab.com/blackprotocol/blacknode/bifrost/blockscanner/types"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/constants"
)

// Fetcher define the methods a claim based scanner need to implement
// claim based scanner will not scan mempool
type Fetcher interface {
	// FetchClaims scan block with the given height
	FetchClaims(height int64) ([]types.TxIn, error)
	// GetHeight return current block height
	GetHeight() (int64, error)
}

// ClaimScanner is used to discover block height
type ClaimScanner struct {
	cfg             config.BifrostBlockScannerConfiguration
	logger          zerolog.Logger
	wg              *sync.WaitGroup
	scanChan        chan int64
	stopChan        chan struct{}
	scannerStorage  blockscanner.ScannerStorage
	metrics         *metrics.Metrics
	previousBlock   int64
	globalTxsQueue  chan types.TxIn
	thorchainBridge *thorclient.Bridge
	chainScanner    Fetcher
	healthy         bool // status of scanner, if last attempt to scan a block was successful or not
}

// NewClaimScanner create a new instance of BlockScanner
func NewClaimScanner(cfg config.BifrostBlockScannerConfiguration,
	scannerStorage blockscanner.ScannerStorage,
	thorchainBridge *thorclient.Bridge,
	chainScanner Fetcher,
) (*ClaimScanner, error) {
	var err error
	if scannerStorage == nil {
		return nil, errors.New("scannerStorage is nil")
	}

	if thorchainBridge == nil {
		return nil, errors.New("thorchain bridge is nil")
	}
	logger := log.Logger.With().Str("module", "claim_scanner").Str("chain", cfg.ChainID.String()).Logger()
	scanner := &ClaimScanner{
		cfg:             cfg,
		logger:          logger,
		wg:              &sync.WaitGroup{},
		stopChan:        make(chan struct{}),
		scanChan:        make(chan int64),
		scannerStorage:  scannerStorage,
		thorchainBridge: thorchainBridge,
		chainScanner:    chainScanner,
		healthy:         false,
	}

	scanner.previousBlock, err = scanner.FetchLastHeight()
	return scanner, err
}

// IsHealthy return if the block scanner is healthy or not
func (b *ClaimScanner) IsHealthy() bool {
	return b.healthy
}

// GetMessages return the channel
func (b *ClaimScanner) GetMessages() <-chan int64 {
	return b.scanChan
}

// Start block scanner
func (b *ClaimScanner) Start(globalTxsQueue chan types.TxIn) {
	b.logger.Info().Msg("start claim scanner")
	b.globalTxsQueue = globalTxsQueue
	currentPos, err := b.scannerStorage.GetScanPos()
	if err != nil {
		b.logger.Err(err).Msgf("fail to get current claim scan pos, %s will start from %d", b.cfg.ChainID, b.previousBlock)
	} else if currentPos > b.previousBlock {
		b.previousBlock = currentPos
	}
	b.wg.Add(1)
	go b.scanClaims()
}

// Checks current mimir settings to determine if the current chain is paused
// either globally or specifically
func (b *ClaimScanner) isChainPaused() bool {
	var haltHeight, solvencyHaltHeight, nodeHaltHeight, thorHeight int64

	// Check if chain has been halted via mimir
	haltHeight, err := b.thorchainBridge.GetMimir(fmt.Sprintf("Halt%sChain", b.cfg.ChainID))
	if err != nil {
		b.logger.Err(err).Msgf("fail to get mimir setting %s", fmt.Sprintf("Halt%sChain", b.cfg.ChainID))
	}
	// Check if chain has been halted by auto solvency checks
	solvencyHaltHeight, err = b.thorchainBridge.GetMimir(fmt.Sprintf("SolvencyHalt%sChain", b.cfg.ChainID))
	if err != nil {
		b.logger.Err(err).Msgf("fail to get mimir %s", fmt.Sprintf("SolvencyHalt%sChain", b.cfg.ChainID))
	}
	// Check if all chains halted globally
	globalHaltHeight, err := b.thorchainBridge.GetMimir("HaltChainGlobal")
	if err != nil {
		b.logger.Err(err).Msg("fail to get mimir setting HaltChainGlobal")
	}
	if globalHaltHeight > haltHeight {
		haltHeight = globalHaltHeight
	}
	// Check if a node paused all chains
	nodeHaltHeight, err = b.thorchainBridge.GetMimir("NodePauseChainGlobal")
	if err != nil {
		b.logger.Err(err).Msg("fail to get mimir setting NodePauseChainGlobal")
	}
	thorHeight, err = b.thorchainBridge.GetBlockHeight()
	if err != nil {
		b.logger.Err(err).Msg("fail to get THORChain block height")
	}

	if nodeHaltHeight > 0 && thorHeight < nodeHaltHeight {
		haltHeight = 1
	}

	return (haltHeight > 0 && thorHeight > haltHeight) || (solvencyHaltHeight > 0 && thorHeight > solvencyHaltHeight)
}

// scanBlocks
func (b *ClaimScanner) scanClaims() {
	b.logger.Debug().Msg("start to scan claims")
	defer b.logger.Debug().Msg("stop scan claims")
	defer b.wg.Done()

	lastMimirCheck := time.Now().Add(-constants.BlackChainBlockTime)
	isChainPaused := false

	// start up to grab those blocks
	for {
		select {
		case <-b.stopChan:
			return
		default:
			preBlockHeight := atomic.LoadInt64(&b.previousBlock)
			currentBlock := preBlockHeight + 1
			// check if mimir has disabled this chain
			if time.Since(lastMimirCheck) >= constants.BlackChainBlockTime {
				isChainPaused = b.isChainPaused()
				lastMimirCheck = time.Now()
			}

			// Chain is paused, mark as unhealthy
			if isChainPaused {
				b.healthy = false
				time.Sleep(constants.BlackChainBlockTime)
				continue
			}

			chainHeight, err := b.chainScanner.GetHeight()
			if err != nil {
				b.logger.Err(err).Msg("fail to get chain block height")
				time.Sleep(b.cfg.BlockHeightDiscoverBackoff)
				continue
			}
			if chainHeight < currentBlock {
				time.Sleep(b.cfg.BlockHeightDiscoverBackoff)
				continue
			}
			b.logger.Info().Msgf("fetch claims,height:%d", currentBlock)
			txIns, err := b.chainScanner.FetchClaims(currentBlock)
			if err != nil {
				// don't log an error if the block doesn't exist yet
				if !errors.Is(err, btypes.ErrUnavailableBlock) {
					b.logger.Err(err).Int64("block height", currentBlock).Msg("fail to get RPCBlock")
					b.healthy = false
				}
				time.Sleep(b.cfg.BlockHeightDiscoverBackoff)
				continue
			}

			total := 0
			for _, item := range txIns {
				total += len(item.TxArray)
			}
			b.logger.Info().Int64("block height", currentBlock).Int("txs", total).Msg("scan block")
			atomic.AddInt64(&b.previousBlock, 1)
			// if current block height is less than 50 blocks behind the tip , then it should catch up soon, should be safe to mark block scanner as healthy
			// if the block scanner is too far away from tip , should not mark the block scanner as healthy , otherwise it might cause , reschedule and double send
			if chainHeight-currentBlock <= 50 {
				b.logger.Debug().Msgf("the gap is %d , set it to healthy", chainHeight-currentBlock)
				b.healthy = true
			} else {
				b.logger.Debug().Msgf("the gap is %d , healthy: %+v", chainHeight-currentBlock, b.healthy)
			}
			b.metrics.GetCounter(metrics.TotalBlockScanned).Inc()
			for _, item := range txIns {
				if len(item.TxArray) > 0 {
					select {
					case <-b.stopChan:
						return
					case b.globalTxsQueue <- item:
					}
				}
			}
			if err := b.scannerStorage.SetScanPos(b.previousBlock); err != nil {
				b.logger.Err(err).Msg("fail to save block scan pos")
				// alert!!
				continue
			}
		}
	}
}

// FetchLastHeight retrieves the last height to start scanning blocks from on startup
//  1. Check if we have a height specified in config AND
//     it's higher than the block scanner storage one, use that
//  2. Get the last observed height from THORChain if available
//  3. Use block scanner storage if > 0
//  4. Fetch last height from the chain itself
func (b *ClaimScanner) FetchLastHeight() (int64, error) {
	// get scanner storage height
	currentPos, _ := b.scannerStorage.GetScanPos() // ignore error

	// 1. if we've configured a starting height, use that
	if b.cfg.StartBlockHeight > 0 && b.cfg.StartBlockHeight > currentPos {
		return b.cfg.StartBlockHeight, nil
	}
	// 2. attempt to find the height from thorchain
	// wait for thorchain to be caught up first
	if err := b.thorchainBridge.WaitToCatchUp(); err != nil {
		return 0, err
	}
	if b.thorchainBridge != nil {
		var height int64
		if b.cfg.ChainID.Equals(common.BLKChain) {
			height, _ = b.thorchainBridge.GetBlockHeight()
		} else {
			height, _ = b.thorchainBridge.GetLastObservedInHeight(b.cfg.ChainID)
		}
		if height > 0 {
			return height, nil
		}
	}

	// 3. If we've already started scanning, begin where we left off
	if currentPos > 0 {
		return currentPos, nil
	}

	// 4. Start from the latest height on the chain itself
	return b.chainScanner.GetHeight()
}

func (b *ClaimScanner) Stop() {
	b.logger.Debug().Msg("receive stop request")
	defer b.logger.Debug().Msg("claim block scanner stopped")
	close(b.stopChan)
	b.wg.Wait()
}
