#!/usr/bin/env bash

set -euo pipefail

# Delete any existing protobuf generated files.
find . -name "*.pb.go" -delete
find . -name "*.pb.gw.go" -delete

go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@v1.16.0
go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install github.com/regen-network/cosmos-proto/protoc-gen-gocosmos
# shellcheck disable=SC2038
find proto/ -path -prune -o -name '*.proto' -printf '%h\n' | sort | uniq |
  while read -r DIR; do
    find "$DIR" -maxdepth 1 -name '*.proto' |
      xargs protoc \
        -I "proto" \
        -I "third_party/proto" \
        --gocosmos_out=plugins=interfacetype+grpc,Mgoogle/protobuf/any.proto=github.com/cosmos/cosmos-sdk/codec/types:.
  done

protoc -I . -I "proto" -I "third_party/proto" --grpc-gateway_out . \
  --grpc-gateway_opt logtostderr=true \
  --grpc-gateway_opt paths=source_relative \
  --grpc-gateway_opt generate_unbound_methods=true \
  proto/blackchain/v1/x/blackchain/types/query.proto

# Move proto files to the right places.
mv proto/blackchain/v1/x/blackchain/types/query.pb.gw.go gitlab.com/blackprotocol/blacknode/x/blackchain/types/query.pb.gw.go
cp -r gitlab.com/blackprotocol/blacknode/* ./

rm -rf gitlab.com
