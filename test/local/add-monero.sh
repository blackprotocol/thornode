#!/bin/bash
# this script will seed the network with some monero fund

set -x
set -o pipefail
# TARGET_HOST should be the host has  thornode
TARGET_HOST=127.0.0.1
if [ -z "$1" ]; then
  echo "Missing thornode host, assume it is localhost"
else
  TARGET_HOST="$1"
fi

for i in $(seq 1 1 1)
do
  SIGNER_PASSWD=password
  SIGNER_NAME="test$i"
  BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show "$SIGNER_NAME" --keyring-backend=file --output json | jq -r '.address')
  # when the last command's error code is not zero , which usually means the name doesn't exist
  if [ $? -gt 0 ]; then
    printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode keys add "$SIGNER_NAME" --keyring-backend=file
    BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --output json | jq -r '.address')
  fi
  PUBKEY=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --pubkey | blacknode pubkey)
  BNB_ADDRESS=$(NET=testnet pubkey2address -p $PUBKEY | grep tbnb | awk '{ print $NF }')
  echo $BNB_ADDRESS
  POOL_ADDRESS=$(curl -s $TARGET_HOST:1317/blackchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="BNB") .address')

  curl -v -s -X POST -d "[{
    \"from\": \"tbnb1lltanv67yztkpt5czw4ajsmg94dlqnnhrq7zqm\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"RUNE-67C\", \"amount\": 1000000000000000}
    ],
    \"memo\": \"switch:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

  sleep 10
  TO_XMR_ADDR=$(curl -s $TARGET_HOST:1317/blackchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="XMR") .address')
  # add XMR.XMR
  MONERO_FROM_ADDRESS=$(monero-helper monero get_address --account_index=0 --address_index=0|jq -r '.address')
  # send XMR
  RESULT=$(monero-helper monero send --recipient="$TO_XMR_ADDR" --amount 1200000000000000 --message "add:XMR.XMR:$BLK_ADDRESS")
  TX_HASH=$(echo "$RESULT" | jq -r '.tx_hash')
  SIGNATURE=$(echo "$RESULT" | jq -r '.signature')
  # add claim
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain claim "XMR" "$MONERO_FROM_ADDRESS" "$TO_XMR_ADDR" "$SIGNATURE" "$TX_HASH" "add:XMR.XMR:$BLK_ADDRESS" --chain-id blackchain --node tcp://"$TARGET_HOST":26657 --from "$SIGNER_NAME" --keyring-backend=file --yes --gas 280000
  # send RUNE
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain deposit 2000000000000 XBX "add:XMR.XMR:$MONERO_FROM_ADDRESS" --chain-id blackchain --node tcp://$TARGET_HOST:26657 --from "$SIGNER_NAME" --keyring-backend=file --yes --gas 280000
  sleep 5

done
