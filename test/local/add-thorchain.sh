#!/bin/bash
# this script will seed the network with some monero fund

set -x
set -o pipefail

# TARGET_HOST should be the host has  thornode
TARGET_HOST=127.0.0.1
if [ -z "$1" ]; then
  echo "Missing thornode host, assume it is localhost"
else
  TARGET_HOST="$1"
fi
TEST_PORT=26659
SIGNER_NAME="cat"
SIGNER_PASSWD=password
BLK_SIGNER_NAME=test
SEED_PHRASE="cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat crawl"
BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show "$BLK_SIGNER_NAME" --keyring-backend=file --output json | jq -r '.address')
if [ $? -gt 0 ]; then
    printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode keys add "$BLK_SIGNER_NAME" --keyring-backend=file
    BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $BLK_SIGNER_NAME --keyring-backend=file --output json | jq -r '.address')
  fi
  PUBKEY=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $BLK_SIGNER_NAME --keyring-backend=file --pubkey | blacknode pubkey)
  BNB_ADDRESS=$(NET=testnet pubkey2address -p $PUBKEY | grep tbnb | awk '{ print $NF }')
  echo $BNB_ADDRESS
  POOL_ADDRESS=$(curl -s $TARGET_HOST:1317/blackchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="BNB") .address')

  curl -v -s -X POST -d "[{
    \"from\": \"tbnb1lltanv67yztkpt5czw4ajsmg94dlqnnhrq7zqm\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"RUNE-67C\", \"amount\": 1000000000000000}
    ],
    \"memo\": \"switch:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

  sleep 10

THOR_ADDRESS=$(printf "$SIGNER_PASSWD\n" | thornode keys show "$SIGNER_NAME" --keyring-backend=file --output json | jq -r '.address')
if [ $? -gt 0 ]; then
  printf "$SEED_PHRASE\n$SIGNER_PASSWD\n" | blacknode keys add "$SIGNER_NAME" --recover --keyring-backend=file
  BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --output json | jq -r '.address')
fi
POOL_ADDRESS=$(curl -s $TARGET_HOST:1317/blackchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="THOR") .address')

# send fund to asgard
printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | thornode tx thorchain send --chain-id thorchain $POOL_ADDRESS 20000000000rune --note "add:THOR.RUNE:$BLK_ADDRESS"  --from $SIGNER_NAME --keyring-backend=file --node tcp://"$TARGET_HOST:$TEST_PORT" --yes --gas 280000

# send XBX on black chain
printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain deposit 2000000000000 XBX "add:THOR.RUNE:$THOR_ADDRESS" --chain-id blackchain --node tcp://$TARGET_HOST:26657 --from "$BLK_SIGNER_NAME" --keyring-backend=file --yes --gas 280000