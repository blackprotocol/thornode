#!/bin/bash
# this script will seed the network with some fund
# switch some RUNE, and created a few pools on BNB chain
# this same script can be used to create some liquidity providers
set -x
set -o pipefail
# TARGET_HOST should be the host has both blacknode & mock binance running on different ports
TARGET_HOST=127.0.0.1
if [ -z "$1" ]; then
  echo "Missing mock binance host, assume it is localhost"
else
  TARGET_HOST="$1"
fi

for i in $(seq 1 1 10)
do
  SIGNER_PASSWD=password
  SIGNER_NAME="test$i"
  BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show "$SIGNER_NAME" --keyring-backend=file --output json | jq -r '.address')
  # when the last command's error code is not zero , which usually means the name doesn't exist
  if [ $? -gt 0 ]; then
    printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode keys add "$SIGNER_NAME" --keyring-backend=file
    BLK_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --output json | jq -r '.address')
  fi
  PUBKEY=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --pubkey | blacknode pubkey)
  BNB_ADDRESS=$(NET=testnet pubkey2address -p $PUBKEY | grep tbnb | awk '{ print $NF }')
  echo $BNB_ADDRESS
  POOL_ADDRESS=$(curl -s $TARGET_HOST:1317/blackchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="BNB") .address')

  curl -v -s -X POST -d "[{
    \"from\": \"tbnb1lltanv67yztkpt5czw4ajsmg94dlqnnhrq7zqm\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"RUNE-67C\", \"amount\": 1000000000000000}
    ],
    \"memo\": \"switch:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

  sleep 10


  # add BNB.BNB
  # send RUNE
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain deposit 200000000000000 XBX add:BNB.BNB:$BNB_ADDRESS --chain-id blackchain --node tcp://$TARGET_HOST:26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 380000
  sleep 5

  # add BNB-LOK-3C0
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain deposit 50000000000 XBX add:BNB.LOK-3C0:$BNB_ADDRESS --chain-id blackchain --node tcp://$TARGET_HOST:26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 380000
  sleep 5

  # add BTCB-101
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx blackchain deposit 150000000000 XBX add:BNB.BTCB-101:$BNB_ADDRESS --chain-id blackchain --node tcp://$TARGET_HOST:26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 380000
  sleep 5

  # send in BNB
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"BNB\", \"amount\": 100000000000}
    ],
    \"memo\": \"add:BNB.BNB:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

  # send in LOK
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"LOK-3C0\", \"amount\": 40000000000}
    ],
    \"memo\": \"add:BNB.LOK-3C0:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

  # send in BTCB-101
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"BTCB-101\", \"amount\": 40000000000}
    ],
    \"memo\": \"add:BNB.BTCB-101:$BLK_ADDRESS\"
  }]" $TARGET_HOST:26660/broadcast/easy

done
