package constants

type DummyConstants struct {
	int64values  map[ConstantName]int64
	boolValues   map[ConstantName]bool
	stringValues map[ConstantName]string
}

// NewDummyConstants create a new instance of DummyConstants for test purpose
func NewDummyConstants(int64Values map[ConstantName]int64, boolValues map[ConstantName]bool, stringValues map[ConstantName]string) *DummyConstants {
	return &DummyConstants{
		int64values:  int64Values,
		boolValues:   boolValues,
		stringValues: stringValues,
	}
}

func (dc *DummyConstants) GetInt64Value(name ConstantName) int64 {
	v, ok := dc.int64values[name]
	if !ok {
		return 0
	}
	return v
}

func (dc *DummyConstants) GetBoolValue(name ConstantName) bool {
	v, ok := dc.boolValues[name]
	if !ok {
		return false
	}
	return v
}

func (dc *DummyConstants) GetStringValue(name ConstantName) string {
	v, ok := dc.stringValues[name]
	if !ok {
		return ""
	}
	return v
}

func (dc *DummyConstants) String() string {
	return ""
}

func (dc *DummyConstants) GetInt64Values() map[string]int64 {
	newMap := make(map[string]int64)
	// analyze-ignore(map-iteration)
	for k, v := range dc.int64values {
		newMap[k.String()] = v
	}
	return newMap
}

func (dc *DummyConstants) GetBoolValues() map[string]bool {
	newMap := make(map[string]bool)
	// analyze-ignore(map-iteration)
	for k, v := range dc.boolValues {
		newMap[k.String()] = v
	}
	return newMap
}

func (dc *DummyConstants) GetStringValues() map[string]string {
	newMap := make(map[string]string)
	// analyze-ignore(map-iteration)
	for k, v := range dc.stringValues {
		newMap[k.String()] = v
	}
	return newMap
}
