package main

import (
	"flag"
	"fmt"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

func main() {
	raw := flag.String("p", "", "thor bech32 pubkey")
	flag.Parse()

	if len(*raw) == 0 {
		panic("no pubkey provided")
	}

	// Read in the configuration file for the sdk
	nw := common.GetCurrentChainNetwork()
	switch nw {
	case common.TestNet:
		fmt.Println("blackchain testnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tblk", "tblkpub")
		config.SetBech32PrefixForValidator("tblkv", "tblkvpub")
		config.SetBech32PrefixForConsensusNode("tblkc", "tblkcpub")
		config.Seal()
	case common.MainNet:
		fmt.Println("blackchain mainnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("blk", "blkpub")
		config.SetBech32PrefixForValidator("blkv", "blkvpub")
		config.SetBech32PrefixForConsensusNode("blkc", "blkcpub")
		config.Seal()
	}

	pk, err := common.NewPubKey(*raw)
	if err != nil {
		panic(err)
	}

	chains := common.Chains{
		common.THORChain,
		common.BNBChain,
	}

	for _, chain := range chains {
		addr, err := pk.GetAddress(chain)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s Address: %s\n", chain.String(), addr)
	}
}
