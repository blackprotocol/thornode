package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	mrpc "gitlab.com/blackprotocol/monero-rpc"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

func main() {
	app := &cli.App{
		Name:  "monero-helper",
		Usage: "This is a cli tool to interact with monero wallet",
		Flags: []cli.Flag{},
		Commands: cli.Commands{
			getMoneroCommands(),
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err)
	}
}

var (
	moneroRpcHost    string
	moneroDaemonHost string
)

// getMoneroCommands return all commands related to monero
func getMoneroCommands() *cli.Command {
	return &cli.Command{
		Name:     "monero",
		Usage:    "sub commands to interact with monero",
		Category: "Monero",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "rpc",
				DefaultText: "rpc host and port ",
				FilePath:    "",
				Usage:       "",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  true,
				Value:       "http://127.0.0.1:18082/json_rpc",
				Destination: &moneroRpcHost,
				EnvVars:     []string{"MONERO_RPC"},
			},
			&cli.StringFlag{
				Name:        "daemon-rpc",
				DefaultText: "rpc host and port ",
				FilePath:    "",
				Usage:       "",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  true,
				Value:       "http://127.0.0.1:18081/json_rpc",
				Destination: &moneroDaemonHost,
				EnvVars:     []string{"MONERO_DAEMON_RPC"},
			},
		},
		Action: func(context *cli.Context) error {
			c := getMoneroRpcClient()
			v, err := c.GetVersion()
			if err != nil {
				return err
			}
			fmt.Println("version:", v.Version)
			return nil
		},
		Subcommands: []*cli.Command{
			getCreateWalletCommand(),
			getOpenWalletCommand(),
			getGetAddressCommand(),
			getGetAccountsCommand(),
			getCloseWalletCommand(),
			getRestoreWalletCommand(),
			getSendCommand(),
			getBalanceCommand(),
			getCheckTxProofCommand(),
			getIsMultisigCommand(),
		},
	}
}

func getMoneroRpcClient() wallet.Client {
	if len(moneroRpcHost) == 0 {
		panic("specify monero rpc host and port using --rpc")
	}
	c := &http.Transport{
		IdleConnTimeout:       time.Second * 300,
		ResponseHeaderTimeout: time.Second * 300,
		MaxIdleConns:          10,
	}
	return wallet.New(mrpc.Config{
		Address:       moneroRpcHost,
		CustomHeaders: nil,
		Transport:     c,
	})
}

func getCreateWalletCommand() *cli.Command {
	return &cli.Command{
		Name:        "createwallet",
		Usage:       "create a wallet",
		Description: "",
		Category:    "wallet",
		Action:      createWallet,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "filename",
				Category:    "wallet",
				DefaultText: "wallet file name",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  false,
				Value:       "",
			},
			&cli.StringFlag{
				Name:        "password",
				Category:    "wallet",
				DefaultText: "wallet password",
				Required:    false,
				HasBeenSet:  false,
				Value:       "",
			},
			&cli.StringFlag{
				Name:        "language",
				Category:    "wallet",
				DefaultText: "wallet language",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  true,
				Value:       "English",
			},
		},
	}
}

func createWallet(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	fileName := ctx.String("filename")
	password := ctx.String("password")
	language := ctx.String("language")
	return c.CreateWallet(&wallet.RequestCreateWallet{
		Filename: fileName,
		Password: password,
		Language: language,
	})
}

func getOpenWalletCommand() *cli.Command {
	return &cli.Command{
		Name:        "openwallet",
		Usage:       "open a wallet",
		Description: "",
		Category:    "wallet",
		Action:      openWallet,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "filename",
				Category:    "wallet",
				DefaultText: "wallet file name",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  false,
				Value:       "",
			},
			&cli.StringFlag{
				Name:        "password",
				Category:    "wallet",
				DefaultText: "wallet password",
				Required:    false,
				HasBeenSet:  false,
				Value:       "",
			},
		},
	}
}

func openWallet(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	fileName := ctx.String("filename")
	password := ctx.String("password")
	return c.OpenWallet(&wallet.RequestOpenWallet{
		Filename: fileName,
		Password: password,
	})
}

func getGetAddressCommand() *cli.Command {
	return &cli.Command{
		Name:        "get_address",
		Usage:       "get address",
		Description: "",
		Category:    "wallet",
		Action:      getMoneroAddresses,
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:        "account_index",
				DefaultText: "account_index",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  false,
				Value:       0,
			},
			&cli.IntFlag{
				Name:        "address_index",
				DefaultText: "address_index",
				Value:       0,
			},
			&cli.BoolFlag{
				Name:       "short",
				Category:   "output",
				Usage:      "only print out address",
				Required:   false,
				Hidden:     false,
				HasBeenSet: false,
				Value:      false,
			},
		},
	}
}

func getMoneroAddresses(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	accountIndex := ctx.Int("account_index")
	// addressIndex := ctx.Int("address_index")
	resp, err := c.GetAddress(&wallet.RequestGetAddress{
		AccountIndex: uint64(accountIndex),
		AddressIndex: nil,
	})
	if err != nil {
		return err
	}
	printJson(resp)
	return nil
}

func getGetAccountsCommand() *cli.Command {
	return &cli.Command{
		Name:        "get_accounts",
		Usage:       "get accounts",
		Description: "",
		Category:    "wallet",
		Action:      getAccounts,
	}
}

func printJson(input any) {
	result, err := json.MarshalIndent(input, "", "   ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(result))
}

func getAccounts(_ *cli.Context) error {
	c := getMoneroRpcClient()
	resp, err := c.GetAccounts(&wallet.RequestGetAccounts{})
	if err != nil {
		return err
	}
	printJson(resp)
	return nil
}

func getCloseWalletCommand() *cli.Command {
	return &cli.Command{
		Name:        "close_wallet",
		Usage:       "close_wallet",
		Description: "",
		Category:    "wallet",
		Action:      closeWallet,
	}
}

func closeWallet(_ *cli.Context) error {
	c := getMoneroRpcClient()
	return c.CloseWallet()
}

func getRestoreWalletCommand() *cli.Command {
	return &cli.Command{
		Name:        "restore_wallet",
		Usage:       "close_wallet",
		Description: "",
		Category:    "wallet",
		Action:      restoreWallet,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "filename",
				Category:   "wallet",
				Usage:      "wallet file name",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
				Value:      "default.wo",
			},
			&cli.StringFlag{
				Name:       "password",
				Category:   "wallet",
				Usage:      "wallet password",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
				Value:      "password",
			},
			&cli.StringFlag{
				Name:       "seed",
				Category:   "wallet",
				Usage:      "seed phase",
				Required:   true,
				Hidden:     false,
				HasBeenSet: false,
			},
			&cli.StringFlag{
				Name:       "language",
				Category:   "wallet",
				Usage:      "wallet language",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
				Value:      "English",
			},
		},
	}
}

func restoreWallet(ctx *cli.Context) error {
	language := ctx.String("language")
	seed := ctx.String("seed")
	password := ctx.String("password")
	filename := ctx.String("filename")
	c := getMoneroRpcClient()
	r := wallet.RequestRestoreDeterministicWallet{
		FileName: filename,
		Password: password,
		Seed:     seed,
		Language: language,
	}
	resp, err := c.RestoreDeterministicWallet(&r)
	if err != nil {
		return err
	}
	fmt.Println(StringifyObject(resp))
	return nil
}

func getSendCommand() *cli.Command {
	return &cli.Command{
		Name:        "send",
		Usage:       "send fund to recipients",
		Description: "",
		Category:    "wallet",
		Action:      sendMonero,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "recipient",
				Category:   "wallet",
				Usage:      "recipient address",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
			&cli.Uint64Flag{
				Name:       "amount",
				Category:   "wallet",
				Usage:      "amount to send to recipient",
				Required:   true,
				Hidden:     false,
				HasBeenSet: false,
				Value:      0,
			},
			&cli.StringFlag{
				Name:        "filename",
				Category:    "wallet",
				DefaultText: "wallet file name",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  true,
				Value:       "default.wo",
			},
			&cli.StringFlag{
				Name:        "password",
				Category:    "wallet",
				DefaultText: "wallet password",
				Required:    false,
				HasBeenSet:  false,
				Value:       "",
			},
			&cli.StringFlag{
				Name:       "message",
				Category:   "wallet",
				Usage:      "message in proof",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
		},
	}
}

func sendMonero(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	resp, err := c.Transfer(&wallet.RequestTransfer{
		Destinations: []*wallet.Destination{
			{
				Address: ctx.String("recipient"),
				Amount:  ctx.Uint64("amount"),
			},
		},

		Priority:      3,
		Mixing:        0,
		RingSize:      11,
		UnlockTime:    0,
		GetTxKey:      false,
		GetTxHex:      false,
		GetTxMetadata: false,
	})
	if err != nil {
		return fmt.Errorf("fail to send fund,err: %w", err)
	}

	// get a tx proof for the transaction
	respProof, err := c.GetTxProof(&wallet.RequestGetTxProof{
		TxID:    resp.TxHash,
		Address: ctx.String("recipient"),
		Message: ctx.String("message"),
	})
	if err != nil {
		return fmt.Errorf("fail to get tx proof,err: %w", err)
	}
	printJson(struct {
		TxHash    string `json:"tx_hash"`
		Amount    uint64 `json:"amount"`
		Fee       uint64 `json:"fee"`
		Signature string `json:"signature"`
	}{
		TxHash:    resp.TxHash,
		Amount:    resp.Amount,
		Fee:       resp.Fee,
		Signature: respProof.Signature,
	})
	return nil
}

func getBalanceCommand() *cli.Command {
	return &cli.Command{
		Name:        "balance",
		Usage:       "get current wallet's balance",
		Description: "",
		Category:    "wallet",
		Action:      getMoneroBalance,
		Flags: []cli.Flag{
			&cli.Uint64Flag{
				Name:       "account_index",
				Category:   "wallet",
				Required:   false,
				Hidden:     false,
				HasBeenSet: true,
				Value:      0,
			},
		},
	}
}

func getMoneroBalance(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	resp, err := c.GetBalance(&wallet.RequestGetBalance{
		AccountIndex: ctx.Uint64("account_index"),
	})
	if err != nil {
		return fmt.Errorf("fail to get wallet balance,err: %w", err)
	}
	fmt.Println(StringifyObject(resp))
	return nil
}

// StringifyObject convert the input to a json string
func StringifyObject(input any) string {
	result, err := json.MarshalIndent(input, "", "   ")
	if err != nil {
		fmt.Println("fail to json marshal object,err:", err)
	}
	return string(result)
}

func getCheckTxProofCommand() *cli.Command {
	return &cli.Command{
		Name:        "check_proof",
		Usage:       "Check transaction Proof",
		Description: "",
		Category:    "wallet",
		Action:      checkTxProof,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "recipient",
				Category:   "wallet",
				Usage:      "recipient address",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
			&cli.StringFlag{
				Name:       "tx_id",
				Category:   "wallet",
				Usage:      "transaction id",
				Required:   true,
				Hidden:     false,
				HasBeenSet: false,
			},
			&cli.StringFlag{
				Name:       "message",
				Category:   "wallet",
				Usage:      "message in proof",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
			&cli.StringFlag{
				Name:       "signature",
				Category:   "wallet",
				Usage:      "signature",
				Required:   true,
				Hidden:     false,
				HasBeenSet: false,
			},
		},
	}
}

func checkTxProof(ctx *cli.Context) error {
	c := getMoneroRpcClient()
	resp, err := c.CheckTxProof(&wallet.RequestCheckTxProof{
		TxID:      ctx.String("tx_id"),
		Address:   ctx.String("recipient"),
		Message:   ctx.String("message"),
		Signature: ctx.String("signature"),
	})
	if err != nil {
		return fmt.Errorf("fail to check tx proof,err: %w", err)
	}
	fmt.Println(StringifyObject(resp))
	return nil
}

func getIsMultisigCommand() *cli.Command {
	return &cli.Command{
		Name:        "is_multisig",
		Usage:       "check whether a wallet is multisig",
		Description: "",
		Category:    "wallet",
		Action:      getIsMultisigAction,
		Flags:       []cli.Flag{},
	}
}

func getIsMultisigAction(_ *cli.Context) error {
	c := getMoneroRpcClient()
	resp, err := c.IsMultisig()
	if err != nil {
		return fmt.Errorf("fail to get is multisig,err:%w", err)
	}
	fmt.Println(StringifyObject(resp))
	return nil
}
