//go:build stagenet
// +build stagenet

package cmd

const (
	Bech32PrefixAccAddr  = "sblk"
	Bech32PrefixAccPub   = "sblkpub"
	Bech32PrefixValAddr  = "sblkv"
	Bech32PrefixValPub   = "sblkvpub"
	Bech32PrefixConsAddr = "sblkc"
	Bech32PrefixConsPub  = "sblkcpub"
)
