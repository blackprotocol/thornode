//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package cmd

const (
	Bech32PrefixAccAddr  = "blk"
	Bech32PrefixAccPub   = "blkpub"
	Bech32PrefixValAddr  = "blkv"
	Bech32PrefixValPub   = "blkvpub"
	Bech32PrefixConsAddr = "blkc"
	Bech32PrefixConsPub  = "blkcpub"
)
