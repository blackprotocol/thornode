//go:build testnet || mocknet
// +build testnet mocknet

package cmd

const (
	Bech32PrefixAccAddr  = "tblk"
	Bech32PrefixAccPub   = "tblkpub"
	Bech32PrefixValAddr  = "tblkv"
	Bech32PrefixValPub   = "tblkvpub"
	Bech32PrefixConsAddr = "tblkc"
	Bech32PrefixConsPub  = "tblkcpub"
)
