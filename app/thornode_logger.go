package app

import (
	tmlog "github.com/tendermint/tendermint/libs/log"
	"gitlab.com/blackprotocol/blacknode/log"
)

var _ tmlog.Logger = (*log.TendermintLogWrapper)(nil)
