//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package blackchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"blk1xghvhe4p50aqh5zq2t2vls938as0dkr2zscsar",
	"blk19pkncem64gajdwrd5kasspyj0t75hhkpeqr94r",
}
