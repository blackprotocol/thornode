package types

import (
	. "gopkg.in/check.v1"

	"gitlab.com/blackprotocol/blacknode/common"
)

type THORNameSuite struct{}

var _ = Suite(&THORNameSuite{})

func (THORNameSuite) TestTHORName(c *C) {
	// happy path
	n := NewTHORName("iamthewalrus", 0, []THORNameAlias{{Chain: common.BLKChain, Address: GetRandomBlackAddress()}})
	c.Check(n.Valid(), IsNil)

	// unhappy path
	n1 := NewTHORName("", 0, []THORNameAlias{{Chain: common.BNBChain, Address: GetRandomBlackAddress()}})
	c.Check(n1.Valid(), NotNil)
	n2 := NewTHORName("hello", 0, []THORNameAlias{{Chain: common.EmptyChain, Address: GetRandomBlackAddress()}})
	c.Check(n2.Valid(), NotNil)
	n3 := NewTHORName("hello", 0, []THORNameAlias{{Chain: common.BLKChain, Address: common.Address("")}})
	c.Check(n3.Valid(), NotNil)
}
