package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"

	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

func init() {
	RegisterCodec(amino)
}

// RegisterCodec register the msg types for amino
func RegisterCodec(cdc *codec.LegacyAmino) {
	cdc.RegisterConcrete(&MsgSwap{}, "blackchain/Swap", nil)
	cdc.RegisterConcrete(&MsgTssPool{}, "blackchain/TssPool", nil)
	cdc.RegisterConcrete(&MsgTssKeysignFail{}, "blackchain/TssKeysignFail", nil)
	cdc.RegisterConcrete(&MsgAddLiquidity{}, "blackchain/AddLiquidity", nil)
	cdc.RegisterConcrete(&MsgWithdrawLiquidity{}, "blackchain/WidthdrawLiquidity", nil)
	cdc.RegisterConcrete(&MsgObservedTxIn{}, "blackchain/ObservedTxIn", nil)
	cdc.RegisterConcrete(&MsgObservedTxOut{}, "blackchain/ObservedTxOut", nil)
	cdc.RegisterConcrete(&MsgDonate{}, "blackchain/MsgDonate", nil)
	cdc.RegisterConcrete(&MsgBond{}, "blackchain/MsgBond", nil)
	cdc.RegisterConcrete(&MsgUnBond{}, "blackchain/MsgUnBond", nil)
	cdc.RegisterConcrete(&MsgLeave{}, "blackchain/MsgLeave", nil)
	cdc.RegisterConcrete(&MsgNoOp{}, "blackchain/MsgNoOp", nil)
	cdc.RegisterConcrete(&MsgOutboundTx{}, "blackchain/MsgOutboundTx", nil)
	cdc.RegisterConcrete(&MsgSetVersion{}, "blackchain/MsgSetVersion", nil)
	cdc.RegisterConcrete(&MsgSetNodeKeys{}, "blackchain/MsgSetNodeKeys", nil)
	cdc.RegisterConcrete(&MsgSetIPAddress{}, "blackchain/MsgSetIPAddress", nil)
	cdc.RegisterConcrete(&MsgReserveContributor{}, "blackchain/MsgReserveContributor", nil)
	cdc.RegisterConcrete(&MsgErrataTx{}, "blackchain/MsgErrataTx", nil)
	cdc.RegisterConcrete(&MsgBan{}, "blackchain/MsgBan", nil)
	cdc.RegisterConcrete(&MsgSwitch{}, "blackchain/MsgSwitch", nil)
	cdc.RegisterConcrete(&MsgMimir{}, "blackchain/MsgMimir", nil)
	cdc.RegisterConcrete(&MsgDeposit{}, "blackchain/MsgDeposit", nil)
	cdc.RegisterConcrete(&MsgNetworkFee{}, "blackchain/MsgNetworkFee", nil)
	cdc.RegisterConcrete(&MsgMigrate{}, "blackchain/MsgMigrate", nil)
	cdc.RegisterConcrete(&MsgRagnarok{}, "blackchain/MsgRagnarok", nil)
	cdc.RegisterConcrete(&MsgRefundTx{}, "blackchain/MsgRefundTx", nil)
	cdc.RegisterConcrete(&MsgSend{}, "blackchain/MsgSend", nil)
	cdc.RegisterConcrete(&MsgNodePauseChain{}, "blackchain/MsgNodePauseChain", nil)
	cdc.RegisterConcrete(&MsgSolvency{}, "blackchain/MsgSolvency", nil)
	cdc.RegisterConcrete(&MsgManageTHORName{}, "blackchain/MsgManageTHORName", nil)
}

// RegisterInterfaces register the types
func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSwap{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssPool{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssKeysignFail{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgAddLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgWithdrawLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxIn{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxOut{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDonate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgUnBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgLeave{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNoOp{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgOutboundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetVersion{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetNodeKeys{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetIPAddress{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgReserveContributor{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgErrataTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBan{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSwitch{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMimir{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDeposit{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNetworkFee{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMigrate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRagnarok{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRefundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSend{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNodePauseChain{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgManageTHORName{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSolvency{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgClaim{})
}
