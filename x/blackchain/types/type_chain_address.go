package types

import (
	"fmt"
)

// String implement fmt.Stringer
func (m *ChainAddress) String() string {
	return fmt.Sprintf("%s-%s", m.Chain, m.Address)
}
