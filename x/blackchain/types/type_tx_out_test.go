package types

import (
	. "gopkg.in/check.v1"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

type TxOutTestSuite struct{}

var _ = Suite(&TxOutTestSuite{})

func (TxOutTestSuite) TestTxOutItem(c *C) {
	txOutItem := TxOutItem{
		Chain:       common.LTCChain,
		ToAddress:   GetRandomLTCAddress(),
		VaultPubKey: GetRandomPubKey(),
		Coin: common.Coin{
			Asset:  common.LTCAsset,
			Amount: cosmos.NewUint(100),
		},
		Memo: "something memo",
		MaxGas: common.Gas{
			common.NewCoin(common.LTCAsset, bnbSingleTxFee),
		},
		GasRate: 1,
		InHash:  GetRandomTxHash(),
	}
	hash, err := txOutItem.TxHash()
	c.Check(err, IsNil)
	c.Check(len(hash) > 0, Equals, true)
	inputs := []struct {
		name        string
		chain       common.Chain
		toAddr      common.Address
		vaultPubKey common.PubKey
		coin        common.Coin
		maxGas      common.Gas
		inHash      common.TxID
		memo        string
	}{
		{
			name:        "empty chain should return an error",
			chain:       common.EmptyChain,
			toAddr:      txOutItem.ToAddress,
			vaultPubKey: txOutItem.VaultPubKey,
			coin:        txOutItem.Coin,
			maxGas:      txOutItem.MaxGas,
			inHash:      txOutItem.InHash,
		},
		{
			name:        "empty in hash should return an error",
			chain:       txOutItem.Chain,
			toAddr:      txOutItem.ToAddress,
			vaultPubKey: txOutItem.VaultPubKey,
			coin:        txOutItem.Coin,
			maxGas:      txOutItem.MaxGas,
			inHash:      "",
		},
		{
			name:        "empty to address should return an error",
			chain:       txOutItem.Chain,
			toAddr:      common.NoAddress,
			vaultPubKey: txOutItem.VaultPubKey,
			coin:        txOutItem.Coin,
			maxGas:      txOutItem.MaxGas,
			inHash:      txOutItem.InHash,
		},
		{
			name:        "empty vault pub key should return an error",
			chain:       txOutItem.Chain,
			toAddr:      txOutItem.ToAddress,
			vaultPubKey: "",
			coin:        txOutItem.Coin,
			maxGas:      txOutItem.MaxGas,
			inHash:      txOutItem.InHash,
		},
		{
			name:        "empty coin should return an error",
			chain:       txOutItem.Chain,
			toAddr:      txOutItem.ToAddress,
			vaultPubKey: txOutItem.VaultPubKey,
			coin:        common.NoCoin,
			maxGas:      txOutItem.MaxGas,
			inHash:      txOutItem.InHash,
		},
		{
			name:        "invalid MaxGas should return an error",
			chain:       txOutItem.Chain,
			toAddr:      txOutItem.ToAddress,
			vaultPubKey: txOutItem.VaultPubKey,
			coin:        txOutItem.Coin,
			maxGas: common.Gas{
				common.NoCoin,
			},
			inHash: txOutItem.InHash,
		},
	}
	for _, tc := range inputs {
		item := TxOutItem{
			Chain:       tc.chain,
			ToAddress:   tc.toAddr,
			VaultPubKey: tc.vaultPubKey,
			Coin:        tc.coin,
			Memo:        "something memo",
			MaxGas:      tc.maxGas,
			InHash:      tc.inHash,
			GasRate:     1,
		}
		c.Check(item.Valid(), NotNil, Commentf(tc.name))
		if item.MaxGas.Valid() == nil {
			c.Check(txOutItem.Equals(item), Equals, false, Commentf(tc.name))
		}
	}
	txOutItem1 := txOutItem
	c.Check(txOutItem1.Equals(txOutItem), Equals, true)
	txOutItem1.Memo = "123456"
	c.Check(txOutItem.Equals(txOutItem1), Equals, false)
}
