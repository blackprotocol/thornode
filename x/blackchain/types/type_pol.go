package types

import (
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

// NewProtocolOwnedLiquidity create a new instance ProtocolOwnedLiquidity it is empty though
func NewProtocolOwnedLiquidity() ProtocolOwnedLiquidity {
	return ProtocolOwnedLiquidity{
		Deposited: cosmos.ZeroUint(),
		Withdrawn: cosmos.ZeroUint(),
	}
}

func (pol ProtocolOwnedLiquidity) CurrentDeposit() cosmos.Int {
	deposited := cosmos.NewIntFromBigInt(pol.Deposited.BigInt())
	withdrawn := cosmos.NewIntFromBigInt(pol.Withdrawn.BigInt())
	return deposited.Sub(withdrawn)
}

// PnL - Profit and Loss
func (pol ProtocolOwnedLiquidity) PnL(value cosmos.Uint) cosmos.Int {
	deposited := cosmos.NewIntFromBigInt(pol.Deposited.BigInt())
	withdrawn := cosmos.NewIntFromBigInt(pol.Withdrawn.BigInt())
	v := cosmos.NewIntFromBigInt(value.BigInt())
	return withdrawn.Sub(deposited).Add(v)
}
