package blackchain

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

// NewQuerier is the module level router for state queries
func NewQuerier(_ *Mgrs, _ cosmos.KeybaseStore) cosmos.Querier {
	return func(ctx cosmos.Context, path []string, req abci.RequestQuery) (res []byte, err error) {
		return nil, nil
	}
}

func getVaultChainAddress(ctx cosmos.Context, vault Vault) []types.ChainAddress {
	var result []types.ChainAddress
	allChains := append(vault.GetChains(), common.BLKChain)
	for _, c := range allChains.Distinct() {
		addr, err := vault.PubKey.GetAddress(c)
		if err != nil {
			ctx.Logger().Error("fail to get address for %s:%w", c.String(), err)
			continue
		}
		if addr == common.NoAddress {
			// get the address from vault
			for _, item := range vault.Addresses {
				if item.Chain == c {
					addr = item.Address
					break
				}
			}
		}
		result = append(result, types.ChainAddress{
			Chain:   c,
			Address: addr,
		})
	}
	return result
}

func getNodePreflightResult(ctx cosmos.Context, mgr *Mgrs, nodeAcc NodeAccount) (QueryNodeAccountPreflightCheck, error) {
	constAccessor := mgr.GetConstants()
	preflightResult := QueryNodeAccountPreflightCheck{}
	status, err := mgr.ValidatorMgr().NodeAccountPreflightCheck(ctx, nodeAcc, constAccessor)
	preflightResult.Status = status
	if err != nil {
		preflightResult.Description = err.Error()
		preflightResult.Code = 1
	} else {
		preflightResult.Description = "OK"
		preflightResult.Code = 0
	}
	return preflightResult, nil
}

// Estimates current rewards for the NodeAccount taking into account bond-weighted rewards and slash points
func getNodeCurrentRewards(ctx cosmos.Context, mgr *Mgrs, nodeAcc NodeAccount, lastChurnHeight int64, totalBondReward, totalEffectiveBond, bondHardCap cosmos.Uint) (cosmos.Uint, error) {
	slashPts, err := mgr.Keeper().GetNodeAccountSlashPoints(ctx, nodeAcc.NodeAddress)
	if err != nil {
		return cosmos.ZeroUint(), fmt.Errorf("fail to get node slash points: %w", err)
	}

	// Find number of blocks since the last churn (the last bond reward payout)
	totalActiveBlocks := ctx.BlockHeight() - lastChurnHeight

	// find number of blocks they were well behaved (ie active - slash points)
	earnedBlocks := totalActiveBlocks - slashPts
	if earnedBlocks < 0 {
		earnedBlocks = 0
	}

	naEffectiveBond := nodeAcc.Bond
	if naEffectiveBond.GT(bondHardCap) {
		naEffectiveBond = bondHardCap
	}

	// reward = totalBondReward * (naEffectiveBond / totalEffectiveBond) * (unslashed blocks since last churn / blocks since last churn)
	reward := common.GetUncappedShare(naEffectiveBond, totalEffectiveBond, totalBondReward)
	reward = common.GetUncappedShare(cosmos.NewUint(uint64(earnedBlocks)), cosmos.NewUint(uint64(totalActiveBlocks)), reward)
	return reward, nil
}

// Calculates total "effective bond" - the total bond when taking into account the
// Bond-weighted hard-cap
func getTotalEffectiveBond(ctx cosmos.Context, mgr *Mgrs, bondHardCap cosmos.Uint) (cosmos.Uint, error) {
	activeNodes, err := mgr.Keeper().ListValidatorsByStatus(ctx, NodeActive)
	if err != nil {
		return cosmos.ZeroUint(), fmt.Errorf("fail to get active nodes: %w", err)
	}

	totalEffectiveBond := cosmos.ZeroUint()
	for _, item := range activeNodes {
		b := item.Bond
		if item.Bond.GT(bondHardCap) {
			b = bondHardCap
		}

		totalEffectiveBond = totalEffectiveBond.Add(b)
	}

	return totalEffectiveBond, nil
}

// GRPCQuerier process GRPC query
type GRPCQuerier struct {
	mgr *Mgrs
	kbs keyring.Signer
}

// NewGRPCQuerier create a new instance of GRPC Querier
func NewGRPCQuerier(mgr *Mgrs, kbs keyring.Signer) *GRPCQuerier {
	return &GRPCQuerier{
		mgr: mgr,
		kbs: kbs,
	}
}

// InboundAddresses process GRPC query inbound address request
func (q *GRPCQuerier) InboundAddresses(c context.Context, _ *types.QueryInboundAddressesRequest) (*types.QueryInboundAddressesResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	active, err := q.mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
	if err != nil {
		ctx.Logger().Error("fail to get active vaults", "error", err)
		return nil, fmt.Errorf("fail to get active vaults: %w", err)
	}

	constAccessor := mgr.GetConstants()
	signingTransactionPeriod := constAccessor.GetInt64Value(constants.SigningTransactionPeriod)

	// select vault that is most secure
	vault := mgr.Keeper().GetMostSecure(ctx, active, signingTransactionPeriod)
	chains := vault.GetChains()
	if len(chains) == 0 {
		chains = common.Chains{common.BLKAsset().Chain}
	}

	isGlobalTradingPaused := isGlobalTradingHalted(ctx, mgr)
	resp := &types.QueryInboundAddressesResponse{
		InboundAddresses: nil,
	}
	for _, chain := range chains {
		// tx send to blackchain doesn't need an address , thus here skip it
		if chain == common.BLKChain {
			continue
		}

		isChainTradingPaused := isChainTradingHalted(ctx, mgr, chain)
		isChainLpPaused := isLPPausedV1(ctx, chain, mgr)
		vaultAddress, err := vault.GetChainAddress(chain)
		if err != nil {
			ctx.Logger().Error("fail to get address for chain", "error", err)
			return nil, fmt.Errorf("fail to get address for chain: %w", err)
		}

		cc := vault.GetContract(chain)
		gasRate := mgr.GasMgr().GetGasRate(ctx, chain)
		networkFeeInfo, err := mgr.GasMgr().GetNetworkFee(ctx, chain)
		if err != nil {
			ctx.Logger().Error("fail to get network fee info", "error", err)
			return nil, fmt.Errorf("fail to get network fee info: %w", err)
		}

		// because blacknode is using 1e8, while GWei in ETH is in 1e9, thus the minimum blacknode can represent is 10Gwei
		// here convert the gas rate to Gwei , so api user don't need to convert it , make it easier for people to understand
		if chain.IsEVM() {
			gasRate = gasRate.MulUint64(10)
		}

		// Retrieve the value the network charges for outbound fees for this chain
		// Note: If calculating the outbound fee of a non-gas asset outbound, the returned value must be converted to RUNE,
		// then to the non-gas asset using the pool depths. That will be the value deducted from the non-gas asset outbound amount
		outboundFee := mgr.GasMgr().GetFee(ctx, chain, chain.GetGasAsset())
		resp.InboundAddresses = append(resp.InboundAddresses, &types.InboundAddress{
			Chain:                chain.String(),
			Pubkey:               vault.PubKey.String(),
			Address:              vaultAddress.String(),
			Router:               cc.Router.String(),
			Halted:               isGlobalTradingPaused || isChainTradingPaused,
			GlobalTradingPaused:  isGlobalTradingPaused,
			ChainTradingPaused:   isChainTradingPaused,
			ChainLpActionsPaused: isChainLpPaused,
			GasRate:              gasRate.String(),
			GasRateUnits:         chain.GetGasUnits(),
			OutboundTxSize:       strconv.FormatUint(networkFeeInfo.TransactionSize, 10),
			OutboundFee:          outboundFee.String(),
			DustThreshold:        chain.DustThreshold().String(),
		})
	}
	return resp, nil
}

// Pools return all poosl
func (q *GRPCQuerier) Pools(c context.Context, _ *types.QueryPoolsRequest) (*types.QueryPoolsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	resp := &types.QueryPoolsResponse{}
	iterator := mgr.Keeper().GetPoolIterator(ctx)
	for ; iterator.Valid(); iterator.Next() {
		var pool Pool
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &pool); err != nil {
			return nil, fmt.Errorf("fail to unmarshal pool: %w", err)
		}
		// ignore pool if no liquidity provider units
		if pool.LPUnits.IsZero() {
			continue
		}

		// Ignore synth asset pool (savers). Info will be on the L1 pool
		if pool.Asset.IsVaultAsset() {
			continue
		}

		// Get Savers Vault
		saversAsset := pool.Asset.GetSyntheticAsset()
		saversPool, err := mgr.Keeper().GetPool(ctx, saversAsset)
		if err != nil {
			return nil, fmt.Errorf("fail to unmarshal savers vault: %w", err)
		}

		saversDepth := saversPool.BalanceAsset
		saversUnits := saversPool.LPUnits

		synthSupply := mgr.Keeper().GetTotalSupply(ctx, pool.Asset.GetSyntheticAsset())
		pool.CalcUnits(mgr.GetVersion(), synthSupply)

		synthMintPausedErr := isSynthMintPaused(ctx, mgr, pool.Asset, cosmos.ZeroUint())
		decimals := pool.Decimals
		if decimals == 0 {
			decimals = 8
		}
		resp.Pools = append(resp.Pools, &types.QueryPool{
			BalanceRune:         pool.BalanceRune,
			BalanceAsset:        pool.BalanceAsset,
			Asset:               pool.Asset,
			LpUnits:             pool.LPUnits,
			PoolUnits:           pool.GetPoolUnits(),
			Status:              pool.Status,
			Decimals:            decimals,
			SynthUnits:          pool.SynthUnits,
			SynthSupply:         synthSupply,
			SaversDepth:         saversDepth.String(),
			SaversUnits:         saversUnits.String(),
			SynthMintPaused:     synthMintPausedErr != nil,
			PendingInboundRune:  pool.PendingInboundRune,
			PendingInboundAsset: pool.PendingInboundAsset,
		})

	}
	return resp, nil
}

// Pool return a single pool
func (q *GRPCQuerier) Pool(c context.Context, in *types.QueryPoolRequest) (*types.QueryPoolResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	asset, err := common.NewAsset(in.Asset)
	if err != nil {
		ctx.Logger().Error("fail to parse asset", "error", err)
		return nil, fmt.Errorf("could not parse asset: %w", err)
	}

	pool, err := mgr.Keeper().GetPool(ctx, asset)
	if err != nil {
		ctx.Logger().Error("fail to get pool", "error", err)
		return nil, fmt.Errorf("could not get pool: %w", err)
	}
	if pool.IsEmpty() {
		return nil, fmt.Errorf("pool: %s doesn't exist", in.Asset)
	}

	// Get Savers Vault for this L1 pool if it's a gas asset
	saversAsset := pool.Asset.GetSyntheticAsset()
	saversPool, err := mgr.Keeper().GetPool(ctx, saversAsset)
	if err != nil {
		return nil, fmt.Errorf("fail to unmarshal savers vault: %w", err)
	}

	saversDepth := saversPool.BalanceAsset
	saversUnits := saversPool.LPUnits

	synthSupply := mgr.Keeper().GetTotalSupply(ctx, pool.Asset.GetSyntheticAsset())
	pool.CalcUnits(mgr.GetVersion(), synthSupply)

	synthMintPausedErr := isSynthMintPaused(ctx, mgr, saversAsset, cosmos.ZeroUint())
	decimals := pool.Decimals
	if decimals == 0 {
		decimals = 8
	}
	resp := &types.QueryPoolResponse{
		Pool: &types.QueryPool{
			BalanceRune:         pool.BalanceRune,
			BalanceAsset:        pool.BalanceAsset,
			Asset:               pool.Asset,
			LpUnits:             pool.LPUnits,
			PoolUnits:           pool.GetPoolUnits(),
			Status:              pool.Status,
			Decimals:            decimals,
			SynthUnits:          pool.SynthUnits,
			SynthSupply:         synthSupply,
			SaversDepth:         saversDepth.String(),
			SaversUnits:         saversUnits.String(),
			SynthMintPaused:     synthMintPausedErr != nil,
			PendingInboundRune:  pool.PendingInboundRune,
			PendingInboundAsset: pool.PendingInboundAsset,
		},
	}
	return resp, nil
}

func (q *GRPCQuerier) LiquidityProviders(c context.Context, in *types.QueryLiquidityProvidersRequest) (*types.QueryLiquidityProvidersResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	asset, err := common.NewAsset(in.Asset)
	if err != nil {
		ctx.Logger().Error("fail to get parse asset", "error", err)
		return nil, fmt.Errorf("fail to parse asset: %w", err)
	}
	if asset.IsVaultAsset() {
		return nil, fmt.Errorf("invalid request: requested pool is a SaversPool")
	}

	var lps []*types.LiquidityProvider
	iterator := mgr.Keeper().GetLiquidityProviderIterator(ctx, asset)
	defer CloseIter(ctx, iterator)
	for ; iterator.Valid(); iterator.Next() {
		var lp LiquidityProvider
		mgr.Keeper().Cdc().MustUnmarshal(iterator.Value(), &lp)
		lps = append(lps, &lp)
	}
	return &types.QueryLiquidityProvidersResponse{LiquidityProviders: lps}, nil
}

// LiquidityProvider query liquidity provider
func (q *GRPCQuerier) LiquidityProvider(c context.Context, in *types.QueryLiquidityProviderRequest) (*types.QueryLiquidityProviderResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	asset, err := common.NewAsset(in.Asset)
	if err != nil {
		ctx.Logger().Error("fail to get parse asset", "error", err)
		return nil, fmt.Errorf("fail to parse asset: %w", err)
	}

	if asset.IsVaultAsset() {
		return nil, fmt.Errorf("invalid request: requested pool is a SaversPool")
	}

	addr, err := common.NewAddress(in.Address)
	if err != nil {
		ctx.Logger().Error("fail to get parse address", "error", err)
		return nil, fmt.Errorf("fail to parse address: %w", err)
	}
	lp, err := mgr.Keeper().GetLiquidityProvider(ctx, asset, addr)
	if err != nil {
		ctx.Logger().Error("fail to get liquidity provider", "error", err)
		return nil, fmt.Errorf("fail to liquidity provider: %w", err)
	}

	return &types.QueryLiquidityProviderResponse{LiquidityProvider: &lp}, nil
}

// Saver query saver
func (q *GRPCQuerier) Saver(c context.Context, in *types.QuerySaverRequest) (*types.QuerySaverResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	inAsset := strings.Replace(in.Asset, ".", "/", 1)
	asset, err := common.NewAsset(inAsset)
	if err != nil {
		ctx.Logger().Error("fail to get parse asset", "error", err)
		return nil, fmt.Errorf("fail to parse asset: %w", err)
	}

	if !asset.IsVaultAsset() {
		return nil, fmt.Errorf("invalid request: requested pool is not a SaversPool")
	}

	addr, err := common.NewAddress(in.Address)
	if err != nil {
		ctx.Logger().Error("fail to get parse address", "error", err)
		return nil, fmt.Errorf("fail to parse address: %w", err)
	}
	lp, err := mgr.Keeper().GetLiquidityProvider(ctx, asset, addr)
	if err != nil {
		ctx.Logger().Error("fail to get liquidity provider", "error", err)
		return nil, fmt.Errorf("fail to liquidity provider: %w", err)
	}

	return &types.QuerySaverResponse{Saver: &lp}, nil
}

func (q *GRPCQuerier) Savers(c context.Context, in *types.QuerySaversRequest) (*types.QuerySaversResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	inputAsset := strings.Replace(in.Asset, ".", "/", 1)
	mgr := q.mgr
	asset, err := common.NewAsset(inputAsset)
	if err != nil {
		ctx.Logger().Error("fail to get parse asset", "error", err)
		return nil, fmt.Errorf("fail to parse asset: %w", err)
	}
	if !asset.IsVaultAsset() {
		return nil, fmt.Errorf("invalid request: requested pool is not a SaversPool")
	}

	var savers []*LiquidityProvider
	iterator := mgr.Keeper().GetLiquidityProviderIterator(ctx, asset)
	defer CloseIter(ctx, iterator)
	for ; iterator.Valid(); iterator.Next() {
		var lp LiquidityProvider
		mgr.Keeper().Cdc().MustUnmarshal(iterator.Value(), &lp)
		savers = append(savers, &lp)
	}
	return &types.QuerySaversResponse{Savers: savers}, nil
}

// TxVoters query tx voter
func (q *GRPCQuerier) TxVoters(c context.Context, in *types.QueryTxVoterRequest) (*types.QueryTxVoterResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.TxId) == 0 {
		return nil, fmt.Errorf("tx id is empty")
	}
	txID, err := common.NewTxID(in.TxId)
	if err != nil {
		return nil, fmt.Errorf("(%s) is not valid tx id,err: %w", in.TxId, err)
	}
	voter, err := mgr.Keeper().GetObservedTxInVoter(ctx, txID)
	if err != nil {
		ctx.Logger().Error("fail to get observed tx voter", "error", err)
		return nil, fmt.Errorf("fail to get observed tx voter: %w", err)
	}
	// when tx in voter doesn't exist , double check tx out voter
	if len(voter.Txs) == 0 {
		voter, err = mgr.Keeper().GetObservedTxOutVoter(ctx, txID)
		if err != nil {
			return nil, fmt.Errorf("fail to get observed tx out voter: %w", err)
		}
		if len(voter.Txs) == 0 {
			return nil, fmt.Errorf("tx: %s doesn't exist", in.TxId)
		}
	}

	return &types.QueryTxVoterResponse{TxVoter: &voter}, nil
}

// Tx query tx based on the given txid
func (q *GRPCQuerier) Tx(c context.Context, in *types.QueryTxRequest) (*types.QueryTxResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.TxId) == 0 {
		return nil, fmt.Errorf("tx id is empty")
	}
	txID, err := common.NewTxID(in.TxId)
	if err != nil {
		return nil, fmt.Errorf("(%s) is not valid tx id,err: %w", in.TxId, err)
	}
	voter, err := mgr.Keeper().GetObservedTxInVoter(ctx, txID)
	if err != nil {
		ctx.Logger().Error("fail to get observed tx voter", "error", err)
		return nil, fmt.Errorf("fail to get observed tx voter: %w", err)
	}
	if len(voter.Txs) == 0 {
		voter, err = mgr.Keeper().GetObservedTxOutVoter(ctx, txID)
		if err != nil {
			return nil, fmt.Errorf("fail to get observed tx out voter: %w", err)
		}
		if len(voter.Txs) == 0 {
			return nil, fmt.Errorf("tx: %s doesn't exist", in.TxId)
		}
	}

	nodeAccounts, err := mgr.Keeper().ListActiveValidators(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get node accounts: %w", err)
	}
	keysignMetric, err := mgr.Keeper().GetTssKeysignMetric(ctx, txID)
	if err != nil {
		ctx.Logger().Error("fail to get keysign metrics", "error", err)
	}
	observedTx := voter.GetTx(nodeAccounts)
	result := &types.QueryTxResponse{ObservedTx: &observedTx, KeysignMetric: keysignMetric}
	return result, nil
}

func (q *GRPCQuerier) Keysigns(c context.Context, in *types.QueryKeysignsRequest) (*types.QueryKeysignsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var err error
	if in.Height <= 0 {
		return nil, fmt.Errorf("block height(%d) is invalid", in.Height)
	}
	if in.Height > ctx.BlockHeight() {
		return nil, fmt.Errorf("block height not available yet")
	}

	txs, err := mgr.Keeper().GetTxOut(ctx, in.Height)
	if err != nil {
		ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
		return nil, fmt.Errorf("fail to get tx out array from key value store: %w", err)
	}

	buf, err := json.Marshal(txs)
	if err != nil {
		ctx.Logger().Error("fail to marshal keysign block to json", "error", err)
		return nil, fmt.Errorf("fail to marshal keysign block to json: %w", err)
	}
	sig, _, err := q.kbs.Sign("blackchain", buf)
	if err != nil {
		ctx.Logger().Error("fail to sign keysign", "error", err)
		return nil, fmt.Errorf("fail to sign keysign: %w", err)
	}
	return &types.QueryKeysignsResponse{
		Keysign:   txs,
		Signature: base64.StdEncoding.EncodeToString(sig),
	}, nil
}

func (q *GRPCQuerier) KeysignsByPubKey(c context.Context, in *types.QueryKeysignsByPubKeyRequest) (*types.QueryKeysignsByPubKeyResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var err error
	if in.Height <= 0 {
		return nil, fmt.Errorf("block height(%d) is invalid", in.Height)
	}
	if in.Height > ctx.BlockHeight() {
		return nil, fmt.Errorf("block height not available yet")
	}

	pk := common.EmptyPubKey
	if len(in.PubKey) > 0 {
		pk, err = common.NewPubKey(in.PubKey)
		if err != nil {
			ctx.Logger().Error("fail to parse pubkey", "error", err)
			return nil, fmt.Errorf("fail to parse pubkey: %w", err)
		}
	}

	txs, err := mgr.Keeper().GetTxOut(ctx, in.Height)
	if err != nil {
		ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
		return nil, fmt.Errorf("fail to get tx out array from key value store: %w", err)
	}

	if !pk.IsEmpty() {
		newTxs := &TxOut{
			Height: txs.Height,
		}
		for _, tx := range txs.TxArray {
			if pk.Equals(tx.VaultPubKey) {
				newTxs.TxArray = append(newTxs.TxArray, tx)
			}
		}
		txs = newTxs
	}

	buf, err := json.Marshal(txs)
	if err != nil {
		ctx.Logger().Error("fail to marshal keysign block to json", "error", err)
		return nil, fmt.Errorf("fail to marshal keysign block to json: %w", err)
	}
	sig, _, err := q.kbs.Sign("blackchain", buf)
	if err != nil {
		ctx.Logger().Error("fail to sign keysign", "error", err)
		return nil, fmt.Errorf("fail to sign keysign: %w", err)
	}
	return &types.QueryKeysignsByPubKeyResponse{
		Keysign:   txs,
		Signature: base64.StdEncoding.EncodeToString(sig),
	}, nil
}

func (q *GRPCQuerier) Keygens(c context.Context, in *types.QueryKeygensRequest) (*types.QueryKeygensResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if in.Height <= 0 {
		return nil, fmt.Errorf("block height(%d) is invalid", in.Height)
	}
	var err error

	if in.Height > ctx.BlockHeight() {
		return nil, fmt.Errorf("block height not available yet")
	}

	keygenBlock, err := mgr.Keeper().GetKeygenBlock(ctx, in.Height)
	if err != nil {
		ctx.Logger().Error("fail to get keygen block", "error", err)
		return nil, fmt.Errorf("fail to get keygen block: %w", err)
	}

	buf, err := json.Marshal(keygenBlock)
	if err != nil {
		ctx.Logger().Error("fail to marshal keygen block to json", "error", err)
		return nil, fmt.Errorf("fail to marshal keygen block to json: %w", err)
	}
	sig, _, err := q.kbs.Sign("blackchain", buf)
	if err != nil {
		ctx.Logger().Error("fail to sign keygen", "error", err)
		return nil, fmt.Errorf("fail to sign keygen: %w", err)
	}
	return &types.QueryKeygensResponse{
		KeygenBlock: &keygenBlock,
		Signature:   base64.StdEncoding.EncodeToString(sig),
	}, nil
}

func (q *GRPCQuerier) KeygensByPubKey(c context.Context, in *types.QueryKeygenByPubKeyRequest) (*types.QueryKeygenByPubKeyResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if in.Height <= 0 {
		return nil, fmt.Errorf("block height(%d) is invalid", in.Height)
	}
	var err error

	if in.Height > ctx.BlockHeight() {
		return nil, fmt.Errorf("block height not available yet")
	}

	keygenBlock, err := mgr.Keeper().GetKeygenBlock(ctx, in.Height)
	if err != nil {
		ctx.Logger().Error("fail to get keygen block", "error", err)
		return nil, fmt.Errorf("fail to get keygen block: %w", err)
	}

	if len(in.PubKey) > 1 {
		pk, err := common.NewPubKey(in.PubKey)
		if err != nil {
			ctx.Logger().Error("fail to parse pubkey", "error", err)
			return nil, fmt.Errorf("fail to parse pubkey: %w", err)
		}
		// only return those keygen contains the request pub key
		newKeygenBlock := NewKeygenBlock(keygenBlock.Height)
		for _, keygen := range keygenBlock.Keygens {
			if keygen.GetMembers().Contains(pk) {
				newKeygenBlock.Keygens = append(newKeygenBlock.Keygens, keygen)
			}
		}
		keygenBlock = newKeygenBlock
	}

	buf, err := json.Marshal(keygenBlock)
	if err != nil {
		ctx.Logger().Error("fail to marshal keygen block to json", "error", err)
		return nil, fmt.Errorf("fail to marshal keygen block to json: %w", err)
	}
	sig, _, err := q.kbs.Sign("blackchain", buf)
	if err != nil {
		ctx.Logger().Error("fail to sign keygen", "error", err)
		return nil, fmt.Errorf("fail to sign keygen: %w", err)
	}
	return &types.QueryKeygenByPubKeyResponse{
		KeygenBlock: &keygenBlock,
		Signature:   base64.StdEncoding.EncodeToString(sig),
	}, nil
}

func (q *GRPCQuerier) Queue(c context.Context, _ *types.QueryQueueRequest) (*types.QueryQueueResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	constAccessor := mgr.GetConstants()
	signingTransactionPeriod := constAccessor.GetInt64Value(constants.SigningTransactionPeriod)
	startHeight := ctx.BlockHeight() - signingTransactionPeriod
	query := &types.QueryQueueResponse{
		ScheduledOutboundValue: cosmos.ZeroUint(),
	}

	iterator := mgr.Keeper().GetSwapQueueIterator(ctx)
	defer CloseIter(ctx, iterator)
	for ; iterator.Valid(); iterator.Next() {
		var msg MsgSwap
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &msg); err != nil {
			continue
		}
		query.Swap++
	}

	iter2 := mgr.Keeper().GetOrderBookItemIterator(ctx)
	defer CloseIter(ctx, iter2)
	for ; iter2.Valid(); iter2.Next() {
		var msg MsgSwap
		if err := mgr.Keeper().Cdc().Unmarshal(iterator.Value(), &msg); err != nil {
			ctx.Logger().Error("failed to load MsgSwap", "error", err)
			continue
		}
		query.Swap++
	}

	for height := startHeight; height <= ctx.BlockHeight(); height++ {
		txs, err := mgr.Keeper().GetTxOut(ctx, height)
		if err != nil {
			ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
			return nil, fmt.Errorf("fail to get tx out array from key value store: %w", err)
		}
		for _, tx := range txs.TxArray {
			if tx.OutHash.IsEmpty() {
				memo, _ := ParseMemoWithTHORNames(ctx, mgr.Keeper(), tx.Memo)
				if memo.IsInternal() {
					query.Internal++
				} else if memo.IsOutbound() {
					query.Outbound++
				}
			}
		}
	}

	// sum outbound value
	maxTxOutOffset, err := mgr.Keeper().GetMimir(ctx, constants.MaxTxOutOffset.String())
	if maxTxOutOffset < 0 || err != nil {
		maxTxOutOffset = constAccessor.GetInt64Value(constants.MaxTxOutOffset)
	}
	txOutDelayMax, err := mgr.Keeper().GetMimir(ctx, constants.TxOutDelayMax.String())
	if txOutDelayMax <= 0 || err != nil {
		txOutDelayMax = constAccessor.GetInt64Value(constants.TxOutDelayMax)
	}

	for height := ctx.BlockHeight() + 1; height <= ctx.BlockHeight()+txOutDelayMax; height++ {
		value, err := mgr.Keeper().GetTxOutValue(ctx, height)
		if err != nil {
			ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
			continue
		}
		if height > ctx.BlockHeight()+maxTxOutOffset && value.IsZero() {
			// we've hit our max offset, and an empty block, we can assume the
			// rest will be empty as well
			break
		}
		query.ScheduledOutboundValue = query.ScheduledOutboundValue.Add(value)
	}

	return query, nil
}

func (q *GRPCQuerier) Outbound(c context.Context, _ *types.QueryOutboundRequest) (*types.QueryOutboundResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	constAccessor := mgr.GetConstants()
	signingTransactionPeriod := constAccessor.GetInt64Value(constants.SigningTransactionPeriod)
	startHeight := ctx.BlockHeight() - signingTransactionPeriod
	if startHeight < 0 {
		startHeight = 0
	}
	result := &types.QueryOutboundResponse{}
	for height := startHeight; height <= ctx.BlockHeight(); height++ {
		txs, err := mgr.Keeper().GetTxOut(ctx, height)
		if err != nil {
			ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
			return nil, fmt.Errorf("fail to get tx out array from key value store: %w", err)
		}
		for _, tx := range txs.TxArray {
			if tx.OutHash.IsEmpty() {
				txClosure := tx
				result.TxOutItems = append(result.TxOutItems, &txClosure)
			}
		}
	}
	return result, nil
}

func (q *GRPCQuerier) ScheduledOutbound(c context.Context, _ *types.QueryScheduledOutboundRequest) (*types.QueryScheduledOutboundResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	result := &types.QueryScheduledOutboundResponse{}
	constAccessor := mgr.GetConstants()
	maxTxOutOffset, err := mgr.Keeper().GetMimir(ctx, constants.MaxTxOutOffset.String())
	if maxTxOutOffset < 0 || err != nil {
		maxTxOutOffset = constAccessor.GetInt64Value(constants.MaxTxOutOffset)
	}
	for height := ctx.BlockHeight() + 1; height <= ctx.BlockHeight()+17280; height++ {
		txOut, err := mgr.Keeper().GetTxOut(ctx, height)
		if err != nil {
			ctx.Logger().Error("fail to get tx out array from key value store", "error", err)
			continue
		}
		if height > ctx.BlockHeight()+maxTxOutOffset && len(txOut.TxArray) == 0 {
			// we've hit our max offset, and an empty block, we can assume the
			// rest will be empty as well
			break
		}
		for _, toi := range txOut.TxArray {
			result.TxOutItems = append(result.TxOutItems, NewQueryTxOutItem(toi, height))
		}
	}

	return result, nil
}

func (q *GRPCQuerier) Heights(c context.Context, _ *types.QueryHeightsRequest) (*types.QueryHeightsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var chains common.Chains
	asgards, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
	if err != nil {
		return nil, fmt.Errorf("fail to get active asgard: %w", err)
	}
	for _, vault := range asgards {
		chains = vault.GetChains().Distinct()
		break
	}
	result := &types.QueryHeightsResponse{}

	for _, c := range chains {
		if c == common.BLKChain {
			continue
		}
		chainHeight, err := mgr.Keeper().GetLastChainHeight(ctx, c)
		if err != nil {
			return nil, fmt.Errorf("fail to get last chain height: %w", err)
		}

		signed, err := mgr.Keeper().GetLastSignedHeight(ctx)
		if err != nil {
			return nil, fmt.Errorf("fail to get last sign height: %w", err)
		}

		result.BlockHeights = append(result.BlockHeights, &types.QueryLastBlockHeight{
			Chain:          c,
			LastObservedIn: chainHeight,
			LastSignedOut:  signed,
			Blackchain:     ctx.BlockHeight(),
		})
	}

	return result, nil
}

func (q *GRPCQuerier) ChainHeights(c context.Context, in *types.QueryChainHeightsRequest) (*types.QueryChainHeightsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.Chain) == 0 {
		return nil, fmt.Errorf("chain is empty")
	}
	chain, err := common.NewChain(in.Chain)
	if err != nil {
		return nil, fmt.Errorf("%s is not a valid chain,err: %w", in.Chain, err)
	}

	if chain.IsBlackChain() {
		return nil, nil
	}
	result := &types.QueryChainHeightsResponse{}
	chainHeight, err := mgr.Keeper().GetLastChainHeight(ctx, chain)
	if err != nil {
		return nil, fmt.Errorf("fail to get last chain height: %w", err)
	}

	signed, err := mgr.Keeper().GetLastSignedHeight(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get last sign height: %w", err)
	}
	result.BlockHeight = &types.QueryLastBlockHeight{
		Chain:          chain,
		LastObservedIn: chainHeight,
		LastSignedOut:  signed,
		Blackchain:     ctx.BlockHeight(),
	}
	return result, nil
}

func (q *GRPCQuerier) Nodes(c context.Context, _ *types.QueryNodesRequest) (*types.QueryNodesResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	nodeAccounts, err := mgr.Keeper().ListValidatorsWithBond(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get node accounts: %w", err)
	}

	active, err := mgr.Keeper().ListActiveValidators(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get all active node account: %w", err)
	}

	network, err := mgr.Keeper().GetNetwork(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get network: %w", err)
	}

	vaults, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
	if err != nil {
		return nil, fmt.Errorf("fail to get active vaults: %w", err)
	}
	if len(vaults) == 0 {
		return nil, fmt.Errorf("no active vaults")
	}

	bondHardCap := getHardBondCap(active)
	totalEffectiveBond, err := getTotalEffectiveBond(ctx, mgr, bondHardCap)
	if err != nil {
		return nil, fmt.Errorf("fail to get total effective bond: %w", err)
	}

	lastChurnHeight := vaults[0].BlockHeight
	result := &types.QueryNodesResponse{}

	for _, na := range nodeAccounts {
		if na.RequestedToLeave && na.Bond.LTE(cosmos.NewUint(common.One)) {
			// ignore the node , it left and also has very little bond
			continue
		}
		slashPts, err := mgr.Keeper().GetNodeAccountSlashPoints(ctx, na.NodeAddress)
		if err != nil {
			return nil, fmt.Errorf("fail to get node slash points: %w", err)
		}
		n := NewQueryNodeAccount(na)

		n.SlashPoints = slashPts
		if na.Status == NodeActive {
			reward, err := getNodeCurrentRewards(ctx, mgr, na, lastChurnHeight, network.BondReward, totalEffectiveBond, bondHardCap)
			if err != nil {
				return nil, fmt.Errorf("fail to get current node rewards: %w", err)
			}
			n.CurrentAward = reward
		}

		jail, err := mgr.Keeper().GetNodeAccountJail(ctx, na.NodeAddress)
		if err != nil {
			return nil, fmt.Errorf("fail to get node jail: %w", err)
		}
		n.Jail = &jail
		chainHeights, err := mgr.Keeper().GetLastObserveHeight(ctx, na.NodeAddress)
		if err != nil {
			return nil, fmt.Errorf("fail to get last observe chain height: %w", err)
		}

		// analyze-ignore(map-iteration)
		for c, h := range chainHeights {
			n.ObserveChains = append(n.ObserveChains, &types.QueryChainHeight{
				Chain:  c,
				Height: h,
			})
		}

		preflightCheckResult, err := getNodePreflightResult(ctx, mgr, na)
		if err != nil {
			ctx.Logger().Error("fail to get node preflight result", "error", err)
		} else {
			n.PreflightStatus = &preflightCheckResult
		}

		bps, err := mgr.Keeper().GetBondProviders(ctx, na.NodeAddress)
		if err != nil {
			ctx.Logger().Error("fail to get bond providers", "error", err)
		}
		n.BondProviders = &bps
		n.BondProviders.Adjust(n.Bond)
		result.Nodes = append(result.Nodes, n)
	}

	return result, nil
}

func (q *GRPCQuerier) Node(c context.Context, in *types.QueryNodeRequest) (*types.QueryNodeResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.Address) == 0 {
		return nil, errors.New("node address not provided")
	}
	nodeAddress := in.Address
	addr, err := cosmos.AccAddressFromBech32(nodeAddress)
	if err != nil {
		return nil, cosmos.ErrUnknownRequest("invalid account address")
	}

	nodeAcc, err := mgr.Keeper().GetNodeAccount(ctx, addr)
	if err != nil {
		return nil, fmt.Errorf("fail to get node accounts: %w", err)
	}

	slashPts, err := mgr.Keeper().GetNodeAccountSlashPoints(ctx, addr)
	if err != nil {
		return nil, fmt.Errorf("fail to get node slash points: %w", err)
	}
	jail, err := mgr.Keeper().GetNodeAccountJail(ctx, nodeAcc.NodeAddress)
	if err != nil {
		return nil, fmt.Errorf("fail to get node jail: %w", err)
	}

	bp, err := mgr.Keeper().GetBondProviders(ctx, nodeAcc.NodeAddress)
	if err != nil {
		return nil, fmt.Errorf("fail to get bond providers: %w", err)
	}
	bp.Adjust(nodeAcc.Bond)

	active, err := mgr.Keeper().ListActiveValidators(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get all active node account: %w", err)
	}
	result := &types.QueryNodeResponse{
		Node: NewQueryNodeAccount(nodeAcc),
	}

	result.Node.SlashPoints = slashPts
	result.Node.Jail = &jail
	result.Node.BondProviders = &bp

	// CurrentAward is an estimation of reward for node in active status
	// Node in other status should not have current reward
	if nodeAcc.Status == NodeActive && !nodeAcc.Bond.IsZero() {
		network, err := mgr.Keeper().GetNetwork(ctx)
		if err != nil {
			return nil, fmt.Errorf("fail to get network: %w", err)
		}
		vaults, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
		if err != nil {
			return nil, fmt.Errorf("fail to get active vaults: %w", err)
		}
		if len(vaults) == 0 {
			return nil, fmt.Errorf("no active vaults")
		}

		bondHardCap := getHardBondCap(active)
		totalEffectiveBond, err := getTotalEffectiveBond(ctx, mgr, bondHardCap)
		if err != nil {
			return nil, fmt.Errorf("fail to get total effective bond: %w", err)
		}

		// Note that unlike actual BondReward distribution in manager_validator_current.go ,
		// this estimate treats lastChurnHeight as the block_height of the first (oldest) Asgard vault,
		// rather than the active_block_height of the youngest active node.
		// As an example, note from the below URLs that these are 5293728 and 5293733 respectively in block 5336942.
		// https://thornode.ninerealms.com/thorchain/vaults/asgard?height=5336942
		// https://thornode.ninerealms.com/thorchain/nodes?height=5336942
		// (Nodes .cxmy and .uy3a .)
		lastChurnHeight := vaults[0].BlockHeight

		reward, err := getNodeCurrentRewards(ctx, mgr, nodeAcc, lastChurnHeight, network.BondReward, totalEffectiveBond, bondHardCap)
		if err != nil {
			return nil, fmt.Errorf("fail to get current node rewards: %w", err)
		}

		result.Node.CurrentAward = reward
	}

	chainHeights, err := mgr.Keeper().GetLastObserveHeight(ctx, addr)
	if err != nil {
		return nil, fmt.Errorf("fail to get last observe chain height: %w", err)
	}

	// analyze-ignore(map-iteration)
	for c, h := range chainHeights {
		result.Node.ObserveChains = append(result.Node.ObserveChains, &types.QueryChainHeight{
			Chain:  c,
			Height: h,
		})
	}

	preflightCheckResult, err := getNodePreflightResult(ctx, mgr, nodeAcc)
	if err != nil {
		ctx.Logger().Error("fail to get node preflight result", "error", err)
	} else {
		result.Node.PreflightStatus = &preflightCheckResult
	}

	return result, nil
}

func (q *GRPCQuerier) Network(c context.Context, _ *types.QueryNetworkRequest) (*types.QueryNetworkResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	data, err := mgr.Keeper().GetNetwork(ctx)
	if err != nil {
		ctx.Logger().Error("fail to get network", "error", err)
		return nil, fmt.Errorf("fail to get network: %w", err)
	}
	result := &types.QueryNetworkResponse{
		BondReward:     data.BondReward.String(),
		TotalBondUnits: data.TotalBondUnits.String(),
		TotalReserve:   mgr.Keeper().GetXBXBalanceOfModule(ctx, ReserveName).String(),
	}

	return result, nil
}

func (q *GRPCQuerier) POL(c context.Context, _ *types.QueryPOLRequest) (*types.QueryPOLResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	data, err := mgr.Keeper().GetPOL(ctx)
	if err != nil {
		ctx.Logger().Error("fail to get POL", "error", err)
		return nil, fmt.Errorf("fail to get POL: %w", err)
	}
	polValue, err := polPoolValue(ctx, mgr)
	if err != nil {
		ctx.Logger().Error("fail to fetch POL value", "error", err)
		return nil, fmt.Errorf("fail to fetch POL value: %w", err)
	}
	pnl := data.PnL(polValue)
	result := &types.QueryPOLResponse{
		Deposited:      data.Deposited.String(),
		Withdrawn:      data.Withdrawn.String(),
		Value:          polValue.String(),
		Pnl:            pnl.String(),
		CurrentDeposit: data.CurrentDeposit().String(),
	}
	return result, nil
}

func (q *GRPCQuerier) ModuleBalance(c context.Context, in *types.QueryModuleBalanceRequest) (*types.QueryModuleBalanceResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	moduleName := in.Name
	if len(moduleName) == 0 {
		moduleName = AsgardName
	}

	modAddr := mgr.Keeper().GetModuleAccAddress(moduleName)
	bal := mgr.Keeper().GetBalance(ctx, modAddr)
	balance := &types.QueryModuleBalanceResponse{
		Name:    moduleName,
		Address: modAddr.String(),
		Coins:   bal,
	}
	return balance, nil
}

func (q *GRPCQuerier) Asgards(c context.Context, _ *types.QueryAsgardsRequest) (*types.QueryAsgardsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	vaults, err := mgr.Keeper().GetAsgardVaults(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get asgard vaults: %w", err)
	}

	result := &types.QueryAsgardsResponse{}
	for _, vault := range vaults {
		if vault.Status == InactiveVault {
			continue
		}
		if !vault.IsAsgard() {
			continue
		}
		if vault.HasFunds() || vault.Status == ActiveVault {
			result.Vaults = append(result.Vaults, &types.QueryVault{
				BlockHeight:           vault.BlockHeight,
				PubKey:                vault.PubKey,
				Coins:                 vault.Coins,
				Type:                  vault.Type,
				Status:                vault.Status,
				StatusSince:           vault.StatusSince,
				Membership:            vault.Membership,
				Chains:                vault.Chains,
				InboundTxCount:        vault.InboundTxCount,
				OutboundTxCount:       vault.OutboundTxCount,
				PendingTxBlockHeights: vault.PendingTxBlockHeights,
				Routers:               vault.Routers,
				Addresses:             getVaultChainAddress(ctx, vault),
			})
		}
	}

	return result, nil
}

func (q *GRPCQuerier) Vault(c context.Context, in *types.QueryVaultRequest) (*types.QueryVaultResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.PubKey) == 0 {
		return nil, errors.New("pub key is empty")
	}
	pubkey, err := common.NewPubKey(in.PubKey)
	if err != nil {
		return nil, fmt.Errorf("%s is invalid pubkey", in.PubKey)
	}
	v, err := mgr.Keeper().GetVault(ctx, pubkey)
	if err != nil {
		return nil, fmt.Errorf("fail to get vault with pubkey(%s),err:%w", pubkey, err)
	}
	if v.IsEmpty() {
		return nil, errors.New("vault not found")
	}
	result := &types.QueryVaultResponse{
		Vault: &types.QueryVault{
			BlockHeight:           v.BlockHeight,
			PubKey:                v.PubKey,
			Coins:                 v.Coins,
			Type:                  v.Type,
			Status:                v.Status,
			StatusSince:           v.StatusSince,
			Membership:            v.Membership,
			Chains:                v.Chains,
			InboundTxCount:        v.InboundTxCount,
			OutboundTxCount:       v.OutboundTxCount,
			PendingTxBlockHeights: v.PendingTxBlockHeights,
			Routers:               v.Routers,
			Addresses:             getVaultChainAddress(ctx, v),
		},
	}
	return result, nil
}

func (q *GRPCQuerier) VaultPubkeys(c context.Context, _ *types.QueryVaultPubKeysRequest) (*types.QueryVaultPubKeysResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	result := &types.QueryVaultPubKeysResponse{}

	iter := mgr.Keeper().GetVaultIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		var vault Vault
		if err := mgr.Keeper().Cdc().Unmarshal(iter.Value(), &vault); err != nil {
			ctx.Logger().Error("fail to unmarshal vault", "error", err)
			return nil, fmt.Errorf("fail to unmarshal vault: %w", err)
		}
		if vault.IsAsgard() {
			if vault.Status == ActiveVault || vault.Status == RetiringVault {
				result.Asgards = append(result.Asgards, &types.QueryVaultPubKeyContract{
					PubKey:         vault.PubKey.String(),
					Routers:        vault.Routers,
					OtherAddresses: vault.Addresses,
				})
			}
		}
	}
	return result, nil
}

func (q *GRPCQuerier) Constants(_ context.Context, _ *types.QueryConstantsRequest) (*types.QueryConstantsResponse, error) {
	mgr := q.mgr
	constAccessor := mgr.GetConstants()

	result := &types.QueryConstantsResponse{
		Int_64Values: constAccessor.GetInt64Values(),
		BoolValues:   constAccessor.GetBoolValues(),
		StringValues: constAccessor.GetStringValues(),
	}
	return result, nil
}

func (q *GRPCQuerier) Mimirs(c context.Context, _ *types.QueryMimirsRequest) (*types.QueryMimirsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	values := make(map[string]int64)
	// collect keys
	iter := mgr.Keeper().GetMimirIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		k := strings.TrimPrefix(string(iter.Key()), "mimir//")
		values[k] = 0
	}

	// analyze-ignore(map-iteration)
	for k := range values {
		v, err := mgr.Keeper().GetMimir(ctx, k)
		if err != nil {
			return nil, fmt.Errorf("fail to get mimir, err: %w", err)
		}
		// v from GetMimir is of type int64.
		if v == -1 {
			// This key has node votes but no node consensus or Admin-set value,
			// so do not display its unset status in the Mimir endpoint.
			delete(values, k)
			continue
		}
		values[k] = v
	}
	return &types.QueryMimirsResponse{
		Mimirs: values,
	}, nil
}

func (q *GRPCQuerier) MimirWithKey(c context.Context, in *types.QueryMimirWithKeyRequest) (*types.QueryMimirWithKeyResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if in.Name == "" {
		return nil, fmt.Errorf("no mimir key")
	}

	v, err := mgr.Keeper().GetMimir(ctx, in.Name)
	if err != nil {
		return nil, fmt.Errorf("fail to get mimir with key:%s, err : %w", in.Name, err)
	}

	return &types.QueryMimirWithKeyResponse{Value: v}, nil
}

func (q *GRPCQuerier) AdminMimirs(c context.Context, _ *types.QueryAdminMimirRequest) (*types.QueryAdminMimirResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	values := make(map[string]int64)
	iter := mgr.Keeper().GetMimirIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		value := types.ProtoInt64{}
		if err := mgr.Keeper().Cdc().Unmarshal(iter.Value(), &value); err != nil {
			ctx.Logger().Error("fail to unmarshal mimir value", "error", err)
			return nil, fmt.Errorf("fail to unmarshal mimir value: %w", err)
		}
		k := strings.TrimPrefix(string(iter.Key()), "mimir//")
		values[k] = value.GetValue()
	}
	return &types.QueryAdminMimirResponse{Mimirs: values}, nil
}

func (q *GRPCQuerier) NodesMimir(c context.Context, _ *types.QueryNodeMimirRequest) (*types.QueryNodeMimirResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	activeNodes, err := mgr.Keeper().ListActiveValidators(ctx)
	if err != nil {
		ctx.Logger().Error("fail to fetch active node accounts", "error", err)
		return nil, fmt.Errorf("fail to fetch active node accounts: %w", err)
	}
	active := activeNodes.GetNodeAddresses()

	values := make(map[string]int64)
	iter := mgr.Keeper().GetNodeMimirIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		mimirs := NodeMimirs{}
		if err := mgr.Keeper().Cdc().Unmarshal(iter.Value(), &mimirs); err != nil {
			ctx.Logger().Error("fail to unmarshal node mimir value", "error", err)
			return nil, fmt.Errorf("fail to unmarshal node mimir value: %w", err)
		}
		k := strings.TrimPrefix(string(iter.Key()), "nodemimir//")
		if v, ok := mimirs.HasSuperMajority(k, active); ok {
			values[k] = v
		}
	}
	return &types.QueryNodeMimirResponse{Mimirs: values}, nil
}

func (q *GRPCQuerier) NodeMimirWithAddress(c context.Context, in *types.QueryNodeMimirWithAddressRequest) (*types.QueryNodeMimirWithAddressResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	acc, err := cosmos.AccAddressFromBech32(in.Address)
	if err != nil {
		ctx.Logger().Error("fail to parse address", "error", err)
		return nil, fmt.Errorf("fail to parse address: %w", err)
	}

	values := make(map[string]int64)
	iter := mgr.Keeper().GetNodeMimirIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		mimirs := NodeMimirs{}
		if err := mgr.Keeper().Cdc().Unmarshal(iter.Value(), &mimirs); err != nil {
			ctx.Logger().Error("fail to unmarshal node mimir value", "error", err)
			return nil, fmt.Errorf("fail to unmarshal node mimir value: %w", err)
		}

		k := strings.TrimPrefix(string(iter.Key()), "nodemimir//")
		if v, ok := mimirs.Get(k, acc); ok {
			values[k] = v
		}
	}

	return &types.QueryNodeMimirWithAddressResponse{Mimirs: values}, nil
}

func (q *GRPCQuerier) AllNodeMimirs(c context.Context, _ *types.QueryAllNodesMimirsRequest) (*types.QueryAllNodesMimirsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	result := &types.QueryAllNodesMimirsResponse{}
	iter := mgr.Keeper().GetNodeMimirIterator(ctx)
	defer CloseIter(ctx, iter)
	for ; iter.Valid(); iter.Next() {
		m := NodeMimirs{}
		if err := mgr.Keeper().Cdc().Unmarshal(iter.Value(), &m); err != nil {
			ctx.Logger().Error("fail to unmarshal node mimir value", "error", err)
			return nil, fmt.Errorf("fail to unmarshal node mimir value: %w", err)
		}
		result.Mimirs = append(result.Mimirs, m.Mimirs...)
	}
	return result, nil
}

func (q *GRPCQuerier) Ban(c context.Context, in *types.QueryBanRequest) (*types.QueryBanResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.Address) == 0 {
		return nil, errors.New("node address not available")
	}
	addr, err := cosmos.AccAddressFromBech32(in.Address)
	if err != nil {
		ctx.Logger().Error("invalid node address", "error", err)
		return nil, fmt.Errorf("invalid node address: %w", err)
	}

	ban, err := mgr.Keeper().GetBanVoter(ctx, addr)
	if err != nil {
		ctx.Logger().Error("fail to get ban voter", "error", err)
		return nil, fmt.Errorf("fail to get ban voter: %w", err)
	}

	return &types.QueryBanResponse{BanVoter: &ban}, nil
}

func (q *GRPCQuerier) Ragnarok(c context.Context, _ *types.QueryRagnarokRequest) (*types.QueryRagnarokResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	ragnarokInProgress := mgr.Keeper().RagnarokInProgress(ctx)
	return &types.QueryRagnarokResponse{RagnarokInProgress: ragnarokInProgress}, nil
}

func (q *GRPCQuerier) KeygenMetric(c context.Context, in *types.QueryKeygenMetricRequest) (*types.QueryKeygenMetricResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var pubKeys common.PubKeys
	if len(in.PubKey) > 0 {
		pkey, err := common.NewPubKey(in.PubKey)
		if err != nil {
			return nil, fmt.Errorf("fail to parse pubkey(%s) err:%w", in.PubKey, err)
		}
		pubKeys = append(pubKeys, pkey)
	}
	var result []*types.TssKeygenMetric
	for _, pkey := range pubKeys {
		m, err := mgr.Keeper().GetTssKeygenMetric(ctx, pkey)
		if err != nil {
			return nil, fmt.Errorf("fail to get tss keygen metric for pubkey(%s):%w", pkey, err)
		}
		result = append(result, m)
	}
	return &types.QueryKeygenMetricResponse{Metrics: result}, nil
}

func (q *GRPCQuerier) TssMetric(c context.Context, _ *types.QueryTssMetricRequest) (*types.QueryTssMetricResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var pubKeys common.PubKeys
	// get all active asgard
	vaults, err := mgr.Keeper().GetAsgardVaultsByStatus(ctx, ActiveVault)
	if err != nil {
		return nil, fmt.Errorf("fail to get active asgards:%w", err)
	}
	for _, v := range vaults {
		pubKeys = append(pubKeys, v.PubKey)
	}
	var keygenMetrics []*types.TssKeygenMetric
	for _, pkey := range pubKeys {
		m, err := mgr.Keeper().GetTssKeygenMetric(ctx, pkey)
		if err != nil {
			return nil, fmt.Errorf("fail to get tss keygen metric for pubkey(%s):%w", pkey, err)
		}
		if len(m.NodeTssTimes) == 0 {
			continue
		}
		keygenMetrics = append(keygenMetrics, m)
	}
	keysignMetric, err := mgr.Keeper().GetLatestTssKeysignMetric(ctx)
	if err != nil {
		return nil, fmt.Errorf("fail to get keysign metric:%w", err)
	}

	result := &types.QueryTssMetricResponse{
		Keygen:  keygenMetrics,
		Keysign: keysignMetric,
	}
	return result, nil
}

func (q *GRPCQuerier) Thorname(c context.Context, in *types.QueryThornameRequest) (*types.QueryThornameResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	if len(in.Name) == 0 {
		return nil, fmt.Errorf("name is empty")
	}
	name, err := mgr.Keeper().GetTHORName(ctx, in.Name)
	if err != nil {
		return nil, ErrInternal(err, "fail to fetch THORName")
	}

	// convert to openapi types
	var aliases []*types.QueryThornameAlias
	for _, alias := range name.Aliases {
		aliases = append(aliases, &types.QueryThornameAlias{
			Chain:   alias.Chain,
			Address: alias.Address,
		})
	}

	resp := &types.QueryThornameResponse{
		Name:              name.Name,
		ExpireBlockHeight: name.ExpireBlockHeight,
		Owner:             name.Owner.String(),
		PreferredAsset:    name.PreferredAsset.String(),
		Aliases:           aliases,
	}
	return resp, nil
}

func (q *GRPCQuerier) Version(c context.Context, _ *types.QueryVersionRequest) (*types.QueryVersionResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	v, hasV := mgr.Keeper().GetVersionWithCtx(ctx)
	if !hasV {
		// re-compute version if not stored
		v = mgr.Keeper().GetLowestActiveVersion(ctx)
	}
	ver := &types.QueryVersionResponse{
		Current: v.String(),
		Next:    mgr.Keeper().GetMinJoinVersion(ctx).String(),
		Querier: constants.SWVersion.String(),
	}
	return ver, nil
}

// Claims return all current valid claims
func (q *GRPCQuerier) Claims(c context.Context, req *types.QueryClaimsRequest) (*types.QueryClaimsResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	constAccessor := mgr.GetConstants()
	signingTransactionPeriod := constAccessor.GetInt64Value(constants.SigningTransactionPeriod)
	startHeight := ctx.BlockHeight() - signingTransactionPeriod
	if startHeight < 0 {
		startHeight = 0
	}
	result := &types.QueryClaimsResponse{}
	for height := startHeight; height <= ctx.BlockHeight(); height++ {
		txs, err := mgr.Keeper().GetBlockClaims(ctx, height)
		if err != nil {
			ctx.Logger().Error("fail to get BlockClaims from key value store", "error", err)
			return nil, fmt.Errorf("fail to get BlockClaims from key value store: %w", err)
		}

		for _, tx := range txs.Claims {
			txClosure := tx
			if req.IncludeDone || !hasClaimObserved(ctx, mgr, tx) {
				result.Claims = append(result.Claims, &txClosure)
			}
		}
	}
	return result, nil
}

func hasClaimObserved(ctx cosmos.Context, mgr *Mgrs, claim types.ClaimItem) bool {
	txIns, err := mgr.K.GetObservedTxInVoter(ctx, claim.TxID)
	if err != nil {
		ctx.Logger().Error("fail to get observed tx in voter", "error", err)
	}
	if !txIns.Tx.IsEmpty() {
		return true
	}
	observedTxOuts, err := mgr.K.GetObservedTxOutVoter(ctx, claim.TxID)
	if err != nil {
		ctx.Logger().Error("fail to get observed tx out voter", "error", err)
	}
	if !observedTxOuts.Tx.IsEmpty() {
		return true
	}
	return false
}

func (q *GRPCQuerier) ClaimsByHeight(c context.Context, in *types.QueryClaimsByHeightRequest) (*types.QueryClaimsByHeightResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	mgr := q.mgr
	var err error
	if in.Height <= 0 {
		return nil, fmt.Errorf("block height(%d) is invalid", in.Height)
	}
	if in.Height > ctx.BlockHeight() {
		return nil, fmt.Errorf("block height not available yet")
	}

	blockClaims, err := mgr.Keeper().GetBlockClaims(ctx, in.Height)
	if err != nil {
		ctx.Logger().Error("fail to get block claim from key value store", "error", err)
		return nil, fmt.Errorf("fail to get block claim from key value store: %w", err)
	}

	buf, err := json.Marshal(blockClaims)
	if err != nil {
		ctx.Logger().Error("fail to marshal BlockClaim to json", "error", err)
		return nil, fmt.Errorf("fail to marshal BlockClaim to json: %w", err)
	}
	sig, _, err := q.kbs.Sign("blackchain", buf)
	if err != nil {
		ctx.Logger().Error("fail to sign BlockClaims", "error", err)
		return nil, fmt.Errorf("fail to sign BlockClaims: %w", err)
	}
	return &types.QueryClaimsByHeightResponse{
		Blockclaims: blockClaims,
		Signature:   base64.StdEncoding.EncodeToString(sig),
	}, nil
}

func CloseIter(ctx sdk.Context, iter sdk.Iterator) {
	if err := iter.Close(); err != nil {
		ctx.Logger().Error("fail to close iter", "err", err)
	}
}
