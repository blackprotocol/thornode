package blackchain

import (
	"fmt"

	"cosmossdk.io/errors"
	"github.com/hashicorp/go-multierror"
)

// BlackChain error code start at 99
const (
	// CodeBadVersion error code for bad version
	CodeInternalError     uint32 = 99
	CodeTxFail            uint32 = 100
	CodeBadVersion        uint32 = 101
	CodeInvalidMessage    uint32 = 102
	CodeInvalidMemo       uint32 = 105
	CodeInvalidPoolStatus uint32 = 107

	CodeSwapFail                 uint32 = 108
	CodeSwapFailNotEnoughFee     uint32 = 110
	CodeSwapFailInvalidAmount    uint32 = 113
	CodeSwapFailInvalidBalance   uint32 = 114
	CodeSwapFailNotEnoughBalance uint32 = 115

	CodeAddLiquidityFailValidation   uint32 = 120
	CodeFailGetLiquidityProvider     uint32 = 122
	CodeAddLiquidityMismatchAddr     uint32 = 123
	CodeAddLiquidityRUNEOverLimit    uint32 = 125
	CodeAddLiquidityRUNEMoreThanBond uint32 = 126

	CodeWithdrawFailValidation uint32 = 130
	CodeFailAddOutboundTx      uint32 = 131
	CodeFailSaveEvent          uint32 = 132
	CodeNoLiquidityUnitLeft    uint32 = 135
	CodeWithdrawWithin24Hours  uint32 = 136
	CodeWithdrawFail           uint32 = 137
)

var (
	errNotAuthorized  = fmt.Errorf("not authorized")
	errInvalidVersion = fmt.Errorf("bad version")
	// errBadVersion                   = errors.Register(DefaultCodespace, CodeBadVersion, errInvalidVersion.Error())
	errInvalidMessage               = errors.Register(DefaultCodespace, CodeInvalidMessage, "invalid message")
	errInvalidMemo                  = errors.Register(DefaultCodespace, CodeInvalidMemo, "invalid memo")
	errFailSaveEvent                = errors.Register(DefaultCodespace, CodeFailSaveEvent, "fail to save add events")
	errAddLiquidityFailValidation   = errors.Register(DefaultCodespace, CodeAddLiquidityFailValidation, "fail to validate add liquidity")
	errAddLiquidityRUNEOverLimit    = errors.Register(DefaultCodespace, CodeAddLiquidityRUNEOverLimit, "add liquidity rune is over limit")
	errAddLiquidityRUNEMoreThanBond = errors.Register(DefaultCodespace, CodeAddLiquidityRUNEMoreThanBond, "add liquidity rune is more than bond")
	errInvalidPoolStatus            = errors.Register(DefaultCodespace, CodeInvalidPoolStatus, "invalid pool status")
	errFailAddOutboundTx            = errors.Register(DefaultCodespace, CodeFailAddOutboundTx, "prepare outbound tx not successful")
	errWithdrawFailValidation       = errors.Register(DefaultCodespace, CodeWithdrawFailValidation, "fail to validate withdraw")
	errFailGetLiquidityProvider     = errors.Register(DefaultCodespace, CodeFailGetLiquidityProvider, "fail to get liquidity provider")
	errAddLiquidityMismatchAddr     = errors.Register(DefaultCodespace, CodeAddLiquidityMismatchAddr, "memo paired address must be non-empty and together with origin address match the liquidity provider record")
	errSwapFailNotEnoughFee         = errors.Register(DefaultCodespace, CodeSwapFailNotEnoughFee, "fail swap, not enough fee")
	errSwapFailInvalidAmount        = errors.Register(DefaultCodespace, CodeSwapFailInvalidAmount, "fail swap, invalid amount")
	errSwapFailInvalidBalance       = errors.Register(DefaultCodespace, CodeSwapFailInvalidBalance, "fail swap, invalid balance")
	errSwapFailNotEnoughBalance     = errors.Register(DefaultCodespace, CodeSwapFailNotEnoughBalance, "fail swap, not enough balance")
	errNoLiquidityUnitLeft          = errors.Register(DefaultCodespace, CodeNoLiquidityUnitLeft, "nothing to withdraw")
	errWithdrawWithin24Hours        = errors.Register(DefaultCodespace, CodeWithdrawWithin24Hours, "you cannot withdraw for 24 hours after providing liquidity for this blockchain")
	errWithdrawFail                 = errors.Register(DefaultCodespace, CodeWithdrawFail, "fail to withdraw")
	errInternal                     = errors.Register(DefaultCodespace, CodeInternalError, "internal error")
)

// ErrInternal return an error  of errInternal with additional message
func ErrInternal(err error, msg string) error {
	return errors.Wrap(multierror.Append(errInternal, err), msg)
}
