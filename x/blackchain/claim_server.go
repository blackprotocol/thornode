package blackchain

import (
	"context"
	"errors"
	"fmt"

	"github.com/blang/semver"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/keeper"
	"gitlab.com/blackprotocol/blacknode/x/blackchain/types"
)

type claimServer struct {
	k   keeper.Keeper
	mgr *Mgrs
}

// NewClaimServerImpl create a new instance of msg server
func NewClaimServerImpl(keeper keeper.Keeper, mgr *Mgrs) types.MsgServer {
	return &claimServer{
		k:   keeper,
		mgr: mgr,
	}
}

// Claim process claim request
func (cs claimServer) Claim(c context.Context, mc *types.MsgClaim) (*types.MsgClaimResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	if mc == nil {
		return nil, errors.New("invalid claim message")
	}
	ctx.Logger().Info("receive MsgClaim", "from", mc.From,
		"to", mc.To,
		"tx_id", mc.TxID,
		"signature", mc.Signature,
		"msg", mc.Message)

	if err := mc.ValidateBasic(); err != nil {
		return nil, cosmos.ErrUnknownRequest("invalid claim message")
	}
	v := cs.mgr.GetVersion()
	switch {
	case v.GTE(semver.MustParse("0.1.0")):
		return cs.ClaimV1(ctx, mc)
	default:
		return nil, fmt.Errorf("invalid version(%s)", v.String())
	}
}

func (cs claimServer) isToAndFromAsgard(ctx cosmos.Context, mc *types.MsgClaim) (bool, error) {
	vaults, err := cs.k.GetAsgardVaults(ctx)
	if err != nil {
		return false, fmt.Errorf("fail to get asgard vaults,err: %w", err)
	}
	for _, v := range vaults {
		for _, item := range v.Addresses {
			if item.Chain.Equals(mc.Chain) && (item.Address.String() == mc.To || item.Address.String() == mc.From) {
				return true, nil
			}
		}
	}
	return false, nil
}

func isClaimFromValidator(ctx cosmos.Context, k keeper.Keeper, signers []cosmos.AccAddress) bool {
	if len(signers) == 0 {
		return false
	}
	// typically don't expect multiple signer
	for _, signer := range signers {
		nodeAccount, err := k.GetNodeAccount(ctx, signer)
		if err != nil {
			ctx.Logger().Error("unauthorized account", "address", signer.String(), "error", err)
			return false
		}

		if nodeAccount.IsEmpty() {
			return false
		}

		if nodeAccount.Status == NodeDisabled || nodeAccount.Status == NodeWhiteListed {
			return false
		}

	}
	return true
}

func (cs claimServer) ClaimV1(ctx cosmos.Context, mc *types.MsgClaim) (*types.MsgClaimResponse, error) {
	// allow active nodes to send claim message for fee , for their outbound observation
	isFromValidator := isClaimFromValidator(ctx, cs.k, mc.GetSigners())
	if !isFromValidator {
		// when chain is halt , no one can claim anything
		haltHeight, err := cs.mgr.Keeper().GetMimir(ctx, "HaltBlackChain")
		if err != nil {
			return nil, fmt.Errorf("failed to get mimir setting: %w", err)
		}
		if haltHeight > 0 && ctx.BlockHeight() > haltHeight {
			return nil, fmt.Errorf("mimir has halted chain")
		}
		// charge transaction fee
		nativeTxFee, err := cs.mgr.Keeper().GetMimir(ctx, constants.NativeTransactionFee.String())
		if err != nil || nativeTxFee < 0 {
			nativeTxFee = cs.mgr.GetConstants().GetInt64Value(constants.NativeTransactionFee)
		}
		gas := common.NewCoin(common.XBXNative, cosmos.NewUint(uint64(nativeTxFee)))
		gasFee, err := gas.Native()
		if err != nil {
			return nil, fmt.Errorf("fail to get gas fee: %w", err)
		}

		totalCoins := cosmos.NewCoins(gasFee)
		if !cs.mgr.Keeper().HasCoins(ctx, mc.GetSigners()[0], totalCoins) {
			return nil, cosmos.ErrInsufficientCoins(err, "insufficient funds")
		}

		// send gas to reserve
		sdkErr := cs.mgr.Keeper().SendFromAccountToModule(ctx, mc.GetSigners()[0], ReserveName, common.NewCoins(gas))
		if sdkErr != nil {
			return nil, fmt.Errorf("unable to send gas to reserve: %w", sdkErr)
		}
	}
	asgardRelated, err := cs.isToAndFromAsgard(ctx, mc)
	if err != nil {
		return nil, fmt.Errorf("fail to check whether claim is to asgard,err: %w", err)
	}
	if !asgardRelated {
		return nil, fmt.Errorf("from(%s) and to(%s) are not asgard , ignore", mc.To, mc.From)
	}
	// make the claim tx has not been observed before
	observedTxIn, err := cs.k.GetObservedTxInVoter(ctx, mc.TxID)
	if err != nil {
		return nil, fmt.Errorf("fail to get observed tx in voter,err: %w", err)
	}
	if !observedTxIn.Tx.IsEmpty() {
		return nil, fmt.Errorf("%s has been observed", mc.TxID)
	}
	observedTxOut, err := cs.k.GetObservedTxOutVoter(ctx, mc.TxID)
	if err != nil {
		return nil, fmt.Errorf("fail to get observed tx in voter,err: %w", err)
	}
	if !observedTxOut.Tx.IsEmpty() {
		return nil, fmt.Errorf("%s has been observed", mc.TxID)
	}
	// make sure the same claim doesn't exist in the last 300 blocks
	signingTranPeriod := cs.mgr.GetConstants().GetInt64Value(constants.SigningTransactionPeriod)
	startHeight := ctx.BlockHeight() - signingTranPeriod
	if startHeight < 0 {
		startHeight = 0
	}

	for i := startHeight; i <= ctx.BlockHeight(); i++ {
		blockClaim, err := cs.k.GetBlockClaims(ctx, i)
		if err != nil {
			ctx.Logger().Error("fail to get BlockClaim", "err", err, "height", i)
		}
		for _, item := range blockClaim.Claims {
			if item.TxID.Equals(mc.TxID) {
				if !isFromValidator {
					return nil, fmt.Errorf("claim already exist on block height %d", i)
				}
				// when sending outbound transactions , multiple bifrost will post the claim back to black node
				// for an outbound , the network only need one claim
				return &types.MsgClaimResponse{}, nil
			}
		}
	}
	// save the claim to key value store
	if err := cs.k.AppendClaimItem(ctx, ctx.BlockHeight(), types.ClaimItem{
		From:      mc.From,
		To:        mc.To,
		Signature: mc.Signature,
		TxID:      mc.TxID,
		Message:   mc.Message,
		Signer:    mc.Signer,
		Chain:     mc.Chain,
	}); err != nil {
		return nil, fmt.Errorf("fail to get BlockClaims,err: %w", err)
	}
	return &types.MsgClaimResponse{}, nil
}

var _ types.MsgServer = claimServer{}
