package blackchain

import (
	"fmt"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/constants"
)

// SwitchHandler is to handle Switch message
// MsgSwitch is used to switch from bep2 RUNE to native RUNE
type SwitchHandler struct {
	mgr Manager
}

// NewSwitchHandler create new instance of SwitchHandler
func NewSwitchHandler(mgr Manager) SwitchHandler {
	return SwitchHandler{
		mgr: mgr,
	}
}

// Run it the main entry point to execute Switch logic
func (h SwitchHandler) Run(ctx cosmos.Context, m cosmos.Msg) (*cosmos.Result, error) {
	msg, ok := m.(*MsgSwitch)
	if !ok {
		return nil, errInvalidMessage
	}
	if err := h.validate(ctx, *msg); err != nil {
		ctx.Logger().Error("msg switch failed validation", "error", err)
		return nil, err
	}
	result, err := h.handle(ctx, *msg)
	if err != nil {
		ctx.Logger().Error("failed to process msg switch", "error", err)
		return nil, err
	}
	return result, err
}

func (h SwitchHandler) validate(ctx cosmos.Context, msg MsgSwitch) error {
	return h.validateV1(ctx, msg)
}

func (h SwitchHandler) validateV1(ctx cosmos.Context, msg MsgSwitch) error {
	if err := msg.ValidateBasic(); err != nil {
		return err
	}

	killSwitchStart := fetchConfigInt64(ctx, h.mgr, constants.KillSwitchStart)
	killSwitchDuration := fetchConfigInt64(ctx, h.mgr, constants.KillSwitchDuration)

	if killSwitchStart > 0 && ctx.BlockHeight() > killSwitchStart+killSwitchDuration {
		return fmt.Errorf("switch is deprecated")
	}

	// if we are getting a non-native asset, ensure its signed by an active
	// node account
	if !msg.Tx.Coins[0].IsNative() {
		if !isSignedByActiveNodeAccounts(ctx, h.mgr.Keeper(), msg.GetSigners()) {
			return cosmos.ErrUnauthorized(errNotAuthorized.Error())
		}
	}

	return nil
}

func (h SwitchHandler) handle(ctx cosmos.Context, msg MsgSwitch) (*cosmos.Result, error) {
	ctx.Logger().Info("handleMsgSwitch request", "destination address", msg.Destination.String())
	return h.handleV1(ctx, msg)
}

func (h SwitchHandler) handleV1(ctx cosmos.Context, msg MsgSwitch) (*cosmos.Result, error) {
	haltHeight, err := h.mgr.Keeper().GetMimir(ctx, "HaltBlackChain")
	if err != nil {
		return nil, fmt.Errorf("failed to get mimir setting: %w", err)
	}
	if haltHeight > 0 && ctx.BlockHeight() > haltHeight {
		return nil, fmt.Errorf("mimir has halted BlackChain transactions")
	}

	if !msg.Tx.Coins[0].IsNative() && msg.Tx.Coins[0].Asset.IsRune() {
		return h.toNativeV1(ctx, msg)
	}

	return nil, fmt.Errorf("only non-native rune can be 'switched' to native rune")
}

func (h SwitchHandler) toNativeV1(ctx cosmos.Context, msg MsgSwitch) (*cosmos.Result, error) {
	coin := common.NewCoin(common.BLKAsset(), h.calcCoinV1(ctx, msg.Tx.Coins[0].Amount))
	// sanity check
	if coin.Amount.GT(msg.Tx.Coins[0].Amount) {
		return nil, fmt.Errorf("improper switch calculation: %d/%d", coin.Amount.Uint64(), msg.Tx.Coins[0].Amount.Uint64())
	}

	addr, err := cosmos.AccAddressFromBech32(msg.Destination.String())
	if err != nil {
		return nil, ErrInternal(err, "fail to parse thor address")
	}
	if err := h.mgr.Keeper().MintAndSendToAccount(ctx, addr, coin); err != nil {
		return nil, ErrInternal(err, "fail to mint native rune coins")
	}

	// update network data
	network, err := h.mgr.Keeper().GetNetwork(ctx)
	if err != nil {
		// do not cause the transaction to fail
		ctx.Logger().Error("failed to get network", "error", err)
	}

	if err := h.mgr.Keeper().SetNetwork(ctx, network); err != nil {
		ctx.Logger().Error("failed to set network", "error", err)
	}

	switchEvent := NewEventSwitch(msg.Tx.FromAddress, addr, msg.Tx.Coins[0], msg.Tx.ID, coin.Amount)
	if err := h.mgr.EventMgr().EmitEvent(ctx, switchEvent); err != nil {
		ctx.Logger().Error("fail to emit switch event", "error", err)
	}

	return &cosmos.Result{}, nil
}

func (h SwitchHandler) calcCoin(ctx cosmos.Context, in cosmos.Uint) cosmos.Uint {
	return h.calcCoinV1(ctx, in)
}

func (h SwitchHandler) calcCoinV1(ctx cosmos.Context, in cosmos.Uint) cosmos.Uint {
	killSwitchStart := fetchConfigInt64(ctx, h.mgr, constants.KillSwitchStart)
	if killSwitchStart > 0 && ctx.BlockHeight() >= killSwitchStart {
		killSwitchDuration := fetchConfigInt64(ctx, h.mgr, constants.KillSwitchDuration)
		remainBlocks := (killSwitchStart + killSwitchDuration) - ctx.BlockHeight()
		if remainBlocks <= 0 {
			return cosmos.ZeroUint()
		}
		return common.GetSafeShare(cosmos.NewUint(uint64(remainBlocks)), cosmos.NewUint(uint64(killSwitchDuration)), in)
	}
	return in
}
