//go:build testnet
// +build testnet

package blackchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"tblk1xghvhe4p50aqh5zq2t2vls938as0dkr2v935aj",
	"tblk19pkncem64gajdwrd5kasspyj0t75hhkph42p4j",
}
