package common

import (
	. "gopkg.in/check.v1"
)

type AssetSuite struct{}

var _ = Suite(&AssetSuite{})

func (s AssetSuite) TestAsset(c *C) {
	asset, err := NewAsset("thor.rune")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(RuneNative), Equals, true)
	c.Check(asset.IsRune(), Equals, true)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.Synth, Equals, false)
	c.Check(asset.String(), Equals, "THOR.RUNE")

	asset, err = NewAsset("thor/rune")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(RuneNative), Equals, false)
	c.Check(asset.IsRune(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.Synth, Equals, true)
	c.Check(asset.String(), Equals, "THOR/RUNE")

	c.Check(asset.Chain.Equals(THORChain), Equals, true)
	c.Check(asset.Symbol.Equals(Symbol("RUNE")), Equals, true)
	c.Check(asset.Ticker.Equals(Ticker("RUNE")), Equals, true)

	// parse without chain
	asset, err = NewAsset("rune")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(RuneNative), Equals, false)

	asset, err = NewAsset("XBX")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(XBXNative), Equals, true)

	// BCH test
	asset, err = NewAsset("bch.bch")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(BCHChain), Equals, true)
	c.Check(asset.Equals(BCHAsset), Equals, true)
	c.Check(asset.IsRune(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "BCH.BCH")

	// LTC test
	asset, err = NewAsset("ltc.ltc")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(LTCChain), Equals, true)
	c.Check(asset.Equals(LTCAsset), Equals, true)
	c.Check(asset.IsRune(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "LTC.LTC")

	// btc/btc
	asset, err = NewAsset("btc/btc")
	c.Check(err, IsNil)
	c.Check(asset.Chain.Equals(BTCChain), Equals, true)
	c.Check(asset.Equals(BTCAsset), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "BTC/BTC")

	asset, err = NewAsset("XMR.XMR")
	c.Check(err, IsNil)
	c.Check(asset.Chain.Equals(XMRChain), Equals, true)
	c.Check(asset.Equals(XMRAsset), Equals, true)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.IsSyntheticAsset(), Equals, false)
}
