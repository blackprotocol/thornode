package tokenlist

import (
	"encoding/json"

	"github.com/blang/semver"

	"gitlab.com/blackprotocol/blacknode/common/tokenlist/avaxtokens"
)

var avaxTokenListV1 EVMTokenList

func init() {
	if err := json.Unmarshal(avaxtokens.AVAXTokenListRawV1, &avaxTokenListV1); err != nil {
		panic(err)
	}
}

func GetAVAXTokenList(version semver.Version) EVMTokenList {
	return avaxTokenListV1
}
