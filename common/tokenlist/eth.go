package tokenlist

import (
	"encoding/json"

	"github.com/blang/semver"

	"gitlab.com/blackprotocol/blacknode/common/tokenlist/ethtokens"
)

var ethTokenListV1 EVMTokenList

func init() {
	if err := json.Unmarshal(ethtokens.ETHTokenListRawV1, &ethTokenListV1); err != nil {
		panic(err)
	}
}

func GetETHTokenList(version semver.Version) EVMTokenList {
	return ethTokenListV1
}
