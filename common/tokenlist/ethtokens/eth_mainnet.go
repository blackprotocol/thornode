//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package ethtokens

import (
	_ "embed"
)

//go:embed eth_mainnet_V1.json
var ETHTokenListRawV1 []byte
