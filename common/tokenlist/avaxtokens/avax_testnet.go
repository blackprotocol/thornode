//go:build testnet || mocknet
// +build testnet mocknet

package avaxtokens

import (
	_ "embed"
)

//go:embed avax_testnet_V1.json
var AVAXTokenListRawV1 []byte
