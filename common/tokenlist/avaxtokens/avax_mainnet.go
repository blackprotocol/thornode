//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package avaxtokens

import (
	_ "embed"
)

//go:embed avax_mainnet_V1.json
var AVAXTokenListRawV1 []byte
