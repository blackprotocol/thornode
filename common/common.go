package common

import (
	"encoding/json"

	"github.com/blang/semver"
)

var LatestVersion semver.Version = semver.MustParse("999.0.0")

func Jsonfy(input any) string {
	buf, err := json.Marshal(input)
	if err != nil {
		panic(err)
	}
	return string(buf)
}
