#!/bin/bash

set -ex

XMR_HOST="${XMR_HOST:=http://monero:18081}"

# Start the wallet RPC server
monero-wallet-rpc --daemon-address "${XMR_HOST}" --rpc-bind-ip 0.0.0.0 --rpc-bind-port 18082 --confirm-external-bind --disable-rpc-login --wallet-dir /wallet --allow-mismatched-daemon-version
