#!/bin/sh

set -o pipefail

. "$(dirname "$0")/core.sh"

if [ "$NET" = "mocknet" ] || [ "$NET" = "testnet" ]; then
  echo "Loading unsafe init for mocknet and testnet..."
  . "$(dirname "$0")/core-unsafe.sh"
fi

NODES="${NODES:=1}"
SEED="${SEED:=blacknode}" # the hostname of the master node
ETH_HOST="${ETH_HOST:=http://ethereum:8545}"
AVAX_HOST="${AVAX_HOST:=http://avalanche:9650}"
THOR_BLOCK_TIME="${THOR_BLOCK_TIME:=5s}"
CHAIN_ID=${CHAIN_ID:=blackchain}

# this is required as it need to run blacknode init, otherwise tendermint related command doesn't work
if [ "$SEED" = "$(hostname)" ]; then
  if [ ! -f ~/.blacknode/config/priv_validator_key.json ]; then
    init_chain
    # remove the original generate genesis file, as below will init chain again
    rm -rf ~/.blacknode/config/genesis.json
  fi
fi

create_thor_user "$SIGNER_NAME" "$SIGNER_PASSWD" "$SIGNER_SEED_PHRASE"

VALIDATOR=$(blacknode tendermint show-validator | blacknode pubkey --bech cons)
NODE_ADDRESS=$(echo "$SIGNER_PASSWD" | blacknode keys show blackchain -a --keyring-backend file)
NODE_PUB_KEY=$(echo "$SIGNER_PASSWD" | blacknode keys show blackchain -p --keyring-backend file | blacknode pubkey)
VERSION=$(fetch_version)

if [ "$SEED" = "$(hostname)" ]; then
  echo "Setting blacknode as genesis"
  if [ ! -f ~/.blacknode/config/genesis.json ]; then
    # add ourselves to the genesis state
    NODE_IP_ADDRESS=${EXTERNAL_IP:=$(curl -s http://whatismyip.akamai.com)}

    init_chain "$NODE_ADDRESS"
    add_node_account "$NODE_ADDRESS" "$VALIDATOR" "$NODE_PUB_KEY" "$VERSION" "$NODE_ADDRESS" "$NODE_PUB_KEY_ED25519" "$NODE_IP_ADDRESS"

    # disable default bank transfer, and opt to use our own custom one
    disable_bank_send

    # for mocknet, add initial balances
    echo "Using NET $NET"
    if [ "$NET" = "mocknet" ]; then
      echo "Setting up accounts"

      # integration test accounts
      add_account tblk1z63f3mzwv3g75az80xwmhrawdqcjpaek9l7gc4 XBX 5000000000000
      add_account tblk1wz78qmrkplrdhy37tw0tnvn0tkm5pqd63af0c7 XBX 25000000000100
      add_account tblk18f55frcvknxvcpx2vvpfedvw4l8eutuh9v8e76 XBX 25000000000100
      add_account tblk1xwusttz86hqfuk5z7amcgqsg7vp6g8zhr3u6s2 XBX 5090000000000

      # local cluster accounts (2M RUNE)
      add_account tblk1uuds8pd92qnnq0udw0rpg0szpgcslc9p50he4j XBX 200000000000000 # cat
      add_account tblk1zf3gsk7edzwl9syyefvfhle37cjtql35y27zcm XBX 200000000000000 # dog
      add_account tblk13wrmhnh2qe98rjse30pl7u6jxszjjwl462vp5r XBX 200000000000000 # fox
      add_account tblk1qk8c8sfrmfm0tkncs0zxeutc8v5mx3pjplkneu XBX 200000000000000 # pig

      reserve 22000000000000000

      # override block time for faster smoke tests
      block_time "$THOR_BLOCK_TIME"
    fi
    if [ "$NET" = "testnet" ]; then
      # mint 1m RUNE to reserve for testnet
      reserve 100000000000000

      # add testnet account and balances
      testnet_add_accounts
    fi

    echo "Genesis content"
    cat ~/.blacknode/config/genesis.json
    blacknode validate-genesis --trace
  fi
fi

# setup peer connection, typically only used for some mocknet configurations
if [ "$SEED" != "$(hostname)" ]; then
  if [ ! -f ~/.blacknode/config/genesis.json ]; then
    echo "Setting blacknode as peer not genesis"

    init_chain "$NODE_ADDRESS"
    NODE_ID=$(fetch_node_id "$SEED")
    echo "NODE ID: $NODE_ID"
    export THOR_TENDERMINT_P2P_PERSISTENT_PEERS="$NODE_ID@$SEED:$PORT_P2P"

    cat ~/.blacknode/config/genesis.json
  fi
fi

# render tendermint and cosmos configuration files
blacknode render-config

export SIGNER_NAME
export SIGNER_PASSWD
exec "$@"
