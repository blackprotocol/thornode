#!/bin/sh

set -o pipefail

export SIGNER_NAME="${SIGNER_NAME:=thorchain}"
export SIGNER_PASSWD="${SIGNER_PASSWD:=password}"

. "$(dirname "$0")/core.sh"

if [ ! -f ~/.blacknode/config/genesis.json ]; then
  init_chain
  rm -rf ~/.blacknode/config/genesis.json # set in blacknode render-config
fi

# render tendermint and cosmos configuration files
blacknode render-config

exec blacknode start
